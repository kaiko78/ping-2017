﻿using App_pressing_Loreau.Helper;
using App_pressing_Loreau.View;
using App_pressing_Loreau.ViewModel;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using App_pressing_Loreau.Model;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using System.Configuration;
using System.Collections;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;



namespace App_pressing_Loreau.View
{
    /// <summary>
    /// Logique d'interaction pour Emballage.xaml
    /// </summary>
    public partial class Emballage : UserControl
    {


        public Emballage()
        {
            InitializeComponent();
            DataContext = new EmballageVM();
            Loaded += TestWindow_Loaded;



        }

        private void TestWindow_Loaded(object sender, RoutedEventArgs e)
        {
            txt_CommandeAttente.Focus();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (txt_CommandeAttente.Text != "")
                {
                    EmballageVM.ConfigClass.idcom = txt_CommandeAttente.Text;
                    //MessageBox.Show("CA MARCHE");

                    char[] arr = new char[] { '0', ' ' }; // Trim these characters.
                    String bb = txt_CommandeAttente.Text;
                    String aa = "Bonjour, votre pressing vous informe que la commande " + bb + " est disponible. Merci de vous présenter avec votre ticket ou ce SMS";
                    int idcommande = Int32.Parse(EmballageVM.ConfigClass.idcom);
                    int idmax = CommandeDAO.selectMaxCommande();

                    if (idcommande > idmax)
                    {
                        MessageBox.Show("Cette commande n'existe pas.");
                    }
                    else
                    {
                        String tel = ClientDAO.selectMobile(idcommande);
                        String text = ClientDAO.selectMobile(idcommande);
                        String smsenvoie = CommandeDAO.selectCommandeDateSms(idcommande);

                        String daterendu = CommandeDAO.selectCommandeDateRendu(idcommande);

                        if(daterendu=="")
                        {
                            if (smsenvoie == "")
                            {
                                //si ca commence par 0 ca ne va pas il faut que ca soit au format internationnal soit +3360000000
                                //donc on remplace 0 par +33
                                if (tel != "")
                                {
                                    if (tel.Substring(0, 1) == "0")
                                    {
                                        text = text.TrimStart(arr);
                                        //Console.WriteLine(text);
                                        tel = "+33" + text;
                                        // Console.WriteLine(tel);
                                        // Console.ReadLine();
                                        Model.Sms.sendSms(tel, aa);
                                    }
                                    else
                                    { // Find the closing tag.

                                        Model.Sms.sendSms(tel, aa);
                                    }
                                }
                                else
                                {

                                }

                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                        

                        
                        
                    }


                    txt_CommandeAttente.Text = "ugviygviygviy";
                }
                else
                {
                    MessageBox.Show("Veuillez entrer l'id de la commande.");

                }


            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void txt_CommandeAttente_TextChanged_1(object sender, TextChangedEventArgs e)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(txt_CommandeAttente.Text, "[^0-9]"))
            {
                MessageBox.Show("Veuillez entrer que des nombres.");
                txt_CommandeAttente.Text = txt_CommandeAttente.Text.Remove(txt_CommandeAttente.Text.Length - 1);
            }

            if (txt_CommandeAttente.Text.Length > 9)
            {
                MessageBox.Show("Cette commande n'existe pas.");
                txt_CommandeAttente.Text = String.Empty;
            }

        }




    }
}
