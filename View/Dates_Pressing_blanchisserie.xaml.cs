﻿using App_pressing_Loreau.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App_pressing_Loreau.Model;
using App_pressing_Loreau.Helper;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using App_pressing_Loreau.Data.DAO;


namespace App_pressing_Loreau.View
{
    /// <summary>
    /// Logique d'interaction pour Dates_Pressing_blanchisserie.xaml
    /// </summary>
    public partial class Dates_Pressing_blanchisserie :  Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Dates_Pressing_blanchisserie()
        {
            InitializeComponent();
            DataContext = new Date_Pressing_BlanchisserieVM();
        }




        private void Date_Blanchisserie(object sender,
        SelectionChangedEventArgs e)
        {
           

        }
        private void Date_Pressing(object sender, SelectionChangedEventArgs e)
        {}
        

        private void bouton_date_click(object sender, RoutedEventArgs e)
        {
            if(Blanchisserie.SelectedDate==null || Pressing.SelectedDate==null)
            { }
            else {
                            var ajd = DateTime.Now;

                Date_Pressing_BlanchisserieVM.ConfigClass.blanblan = Blanchisserie.SelectedDate.Value.Date;
                Date_Pressing_BlanchisserieVM.ConfigClass.prepre = Pressing.SelectedDate.Value.Date;
                String press = Date_Pressing_BlanchisserieVM.ConfigClass.prepre.ToString("dd/MM/yyyy");
                String blanch = Date_Pressing_BlanchisserieVM.ConfigClass.blanblan.ToString("dd/MM/yyyy");
                MessageBox.Show("Pressing: " + press + " Blanchisserie: " + blanch);
            CommandeDAO.updatePopup(ajd, Date_Pressing_BlanchisserieVM.ConfigClass.prepre, Date_Pressing_BlanchisserieVM.ConfigClass.blanblan);

                Close();
				}
            

        }

        public static implicit operator DatePicker(Dates_Pressing_blanchisserie v)
        {
            throw new NotImplementedException();
        }

        private void Pressing_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
