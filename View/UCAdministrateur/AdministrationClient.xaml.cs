﻿using App_pressing_Loreau.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace App_pressing_Loreau.View
{
    /// <summary>
    /// Logique d'interaction pour AdministrationClient.xaml
    /// </summary>
    public partial class AdministrationClient : UserControl
    {
        public AdministrationClient()
        {
            InitializeComponent();
            DataContext = new AdministrationClientVM();
        }


        private void txb_administrationClient_choix_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btn_retour_Click(object sender, RoutedEventArgs e)
        {
            dp.Children.Clear();
            dp.Children.Add( new PageAdministrateur());
        }

        private void txb_adminClient_modifTypesolde_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txb_adminClient_modifTypesolde.Text, "[^0-9]"))
            {
                MessageBox.Show("Veuillez entrer que des nombres.");
                txb_adminClient_modifTypesolde.Text = txb_adminClient_modifTypesolde.Text.Remove(txb_adminClient_modifTypesolde.Text.Length - 1);
            }
        }

        private void txb_adminClient_modifTypeTelephone_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txb_adminClient_modifTypeTelephone.Text, "[^0-9]"))
            {
                MessageBox.Show("Veuillez entrer que des nombres.");
                txb_adminClient_modifTypeTelephone.Text = txb_adminClient_modifTypeTelephone.Text.Remove(txb_adminClient_modifTypeTelephone.Text.Length - 1);
            }
        }
    }
}
