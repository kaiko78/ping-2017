﻿using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Model;
using App_pressing_Loreau.Model.DTO;
using App_pressing_Loreau.View;
using App_pressing_Loreau.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace App_pressing_Loreau
{
    /// <summary>
    /// Logique d'interaction pour DetailCommande.xaml
    /// </summary>
    public partial class DetailCommande : UserControl
    {
        public DetailCommande()
        {
            InitializeComponent();
            DataContext = new DetailCommandeVM();

        }

        

        public class AutoClosingMessageBox
        {
            System.Threading.Timer _timeoutTimer;
            string _caption;
            AutoClosingMessageBox(string text, string caption, int timeout)
            {
                _caption = caption;
                _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                    null, timeout, System.Threading.Timeout.Infinite);
                using (_timeoutTimer)
                    MessageBox.Show(text, caption);
            }
            public static void Show(string text, string caption, int timeout)
            {
                new AutoClosingMessageBox(text, caption, timeout);
            }
            void OnTimerElapsed(object state)
            {
                IntPtr mbWnd = FindWindow("#32770", _caption); // lpClassName is #32770 for MessageBox
                if (mbWnd != IntPtr.Zero)
                    SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                _timeoutTimer.Dispose();
            }
            const int WM_CLOSE = 0x0010;
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
            static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
            [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
            static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        }

        private void btn_detailCommande_rendre_articles_selectionnes_Click(object sender, RoutedEventArgs e)
        {
            






            dp.Children.Clear();
            //dp.Children.Add(new Accueil());
            //dp.Children.Add(new Paiement());
            //dp.Children.Clear();
            ////Si la commande a déjà été payée je ne pase pas par la page de paiement
            if (ClasseGlobale._renduCommande.payee == false)
            {
                dp.Children.Add(new Paiement());
            }
            else
            {
                //Il faut nettoyer le convoyeur et update les articles

                List<Article> ListeSelectArt = ClasseGlobale._rendreArticlesSelectionnes;
                Commande comdRendu = ClasseGlobale._renduCommande;

                foreach (Article art in ListeSelectArt)
                {
                    //Mise à jour de la place convoyeur
                    //1 - dans la table convoyeur : on soustrait l'encombrement
                    //2 - dans la table article : id convoyeur devient nul

                    art.convoyeur.encombrement = (float)((decimal)art.convoyeur.encombrement - (decimal)art.type.encombrement);
                    //Si un article est à la même place, il faut modifier sa place convoyeur pour qu'elle corresponde au changement appliqué
                    //Permet la mise à jour correcte de la table convoyeur
                    foreach (Article art2 in ListeSelectArt)
                    {
                        //Si j'ai un autre article au même emplacement convoyeur
                        if (art2.convoyeur.id == art.convoyeur.id && art2.id != art.id)
                        {
                            //Je lui attribut le bon encombrement
                            art2.convoyeur.encombrement = art.convoyeur.encombrement;
                        }
                    }

                    PlaceConvoyeurDAO.updatePlaceConvoyeur(art.convoyeur);

                    //Article artAdd = new Article(art.id, art.photo, art.commentaire, true, art.TVA, art.TTC, art.type, null, comdRendu.id);
                    Article artAdd = new Article(art.id, true, null);
                    artAdd.date_rendu = DateTime.Now;
                    //artAdd.date_payee = DateTime.Now;
                    //MessageBox.Show("date rendu avant update "+comdRendu);
                    ArticleDAO.updateArticleMax(artAdd);
                }


                //Mise à jour de la commande
                comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);
                bool tousLesArticlesSontIlsRendus = true;
                //Si j'ai rendu tous les articles de la commande je dois update le champ de la commande
                foreach (Article art in comdRendu.listArticles)
                {
                    //Si l'un au moins des articles n'est pas rendu je ne change pas la date rendu de la commande
                    if (art.ifRendu == false)
                    {
                        tousLesArticlesSontIlsRendus = false;
                        break;
                    }
                }
                if (tousLesArticlesSontIlsRendus == true)
                {
                    comdRendu.date_rendu = DateTime.Now;
                    CommandeDAO.updateCommande(comdRendu);
                }


                comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);
                try
                {
                    if (tousLesArticlesSontIlsRendus == true)
                    {
                        //MessageBox.Show("ça marche");
                        AutoClosingMessageBox.Show("La commande est bien restituée.", "Message", 2000);
                    }
                    else
                    {
                        RecuPaiement rp = new RecuPaiement(comdRendu);
                        rp.printRecu(true);
                        rp.printRecu(false);
                    }
                    
                    
                    //if (comdRendu.listArticles != null)
                    //{
                    //    TicketVetement ticketVetement = new TicketVetement(comdRendu);
                    //    ticketVetement.printAllArticleCmd();
                    //}
                    //else
                    //    MessageBox.Show("La commande ne contient pas d'articles");

                }
                catch (Exception)
                {
                    MessageBox.Show("Impression refusée");
                }
                finally
                {
                    //initialise tout
                    ClasseGlobale.SET_ALL_NULL();
                }

            }

        }

        private void btn_rendutotal_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn.Command.Execute(btn.CommandParameter);


            dp.Children.Clear();
            //dp.Children.Add(new Accueil());
            //dp.Children.Add(new Paiement());
            //dp.Children.Clear();
            ////Si la commande a déjà été payée je ne pase pas par la page de paiement
            if (ClasseGlobale._renduCommande.payee == false)
            {
                dp.Children.Add(new Paiement());
            }
            else
            {
                //Il faut nettoyer le convoyeur et update les articles

                List<Article> ListeSelectArt = ClasseGlobale._rendreArticlesSelectionnes;
                Commande comdRendu = ClasseGlobale._renduCommande;

                foreach (Article art in ListeSelectArt)
                {
                    //Mise à jour de la place convoyeur
                    //1 - dans la table convoyeur : on soustrait l'encombrement
                    //2 - dans la table article : id convoyeur devient nul

                    art.convoyeur.encombrement = (float)((decimal)art.convoyeur.encombrement - (decimal)art.type.encombrement);
                    //Si un article est à la même place, il faut modifier sa place convoyeur pour qu'elle corresponde au changement appliqué
                    //Permet la mise à jour correcte de la table convoyeur
                    foreach (Article art2 in ListeSelectArt)
                    {
                        //Si j'ai un autre article au même emplacement convoyeur
                        if (art2.convoyeur.id == art.convoyeur.id && art2.id != art.id)
                        {
                            //Je lui attribut le bon encombrement
                            art2.convoyeur.encombrement = art.convoyeur.encombrement;
                        }
                    }

                    PlaceConvoyeurDAO.updatePlaceConvoyeur(art.convoyeur);

                    //Article artAdd = new Article(art.id, art.photo, art.commentaire, true, art.TVA, art.TTC, art.type, null, comdRendu.id);
                    Article artAdd = new Article(art.id, true, null);

                    artAdd.date_rendu = DateTime.Now;
                    //artAdd.date_payee = DateTime.Now;

                    ArticleDAO.updateArticleMax(artAdd);
                }


                //Mise à jour de la commande
                comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);
                bool tousLesArticlesSontIlsRendus = true;
                //Si j'ai rendu tous les articles de la commande je dois update le champ de la commande
                foreach (Article art in comdRendu.listArticles)
                {
                    //Si l'un au moins des articles n'est pas rendu je ne change pas la date rendu de la commande
                    if (art.ifRendu == false)
                    {
                        tousLesArticlesSontIlsRendus = false;
                        break;
                    }
                }
                if (tousLesArticlesSontIlsRendus == true)
                {
                    comdRendu.date_rendu = DateTime.Now;
                    CommandeDAO.updateCommande(comdRendu);
                }


                comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);
                try
                {

                    if (tousLesArticlesSontIlsRendus == true)
                    {
                        //MessageBox.Show("ça marche");
                        AutoClosingMessageBox.Show("La commande est bien restituée.", "Message", 2000);

                    }
                    else
                    {
                        RecuPaiement rp = new RecuPaiement(comdRendu);
                        rp.printRecu(true);
                        rp.printRecu(false);
                    }
                    
                    //if (comdRendu.listArticles != null)
                    //{
                    //    TicketVetement ticketVetement = new TicketVetement(comdRendu);
                    //    ticketVetement.printAllArticleCmd();
                    //}
                    //else
                    //    MessageBox.Show("La commande ne contient pas d'articles");

                }
                catch (Exception)
                {
                    MessageBox.Show("Impression refusée");
                }
                finally
                {
                    //initialise tout
                    ClasseGlobale.SET_ALL_NULL();
                }

            }
        }
    }
}
