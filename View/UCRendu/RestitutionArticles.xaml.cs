﻿using System.Windows.Controls;
using System.Windows;
using System.Collections.Generic;
using System;
using App_pressing_Loreau.ViewModel;
using App_pressing_Loreau.Model.DTO;
using App_pressing_Loreau.Data.DAO;
using System.Windows.Input;
using App_pressing_Loreau.Helper;



namespace App_pressing_Loreau.View
{

    /// <summary>
    /// 
    /// Logique d'interaction pour RestitutionClient.xaml
    /// </summary>
    public partial class RestitutionArticles : UserControl
    {


        public RestitutionArticles()
        {
            InitializeComponent();

            Loaded += TestWindow_Loaded;


        }

        private void TestWindow_Loaded(object sender, RoutedEventArgs e)
        {
            txt_CommandeBip.Focus();
        }

        private void btn_restitutionArticles_suivant_Click(object sender, RoutedEventArgs e)
        {
            if (ClasseGlobale._renduCommande != null)
            {


                if (DesArticlesSontIlsEncoreARendre())
                {
                    dp.Children.Clear();
                    dp.Children.Add(new DetailCommande());
                }
                else
                {
                    MessageBox.Show("Les articles de cette commande ont déjà tous été rendus");
                }

            }
            else
            {
                MessageBox.Show("Selectionner une commande");
            }

        }

        private bool DesArticlesSontIlsEncoreARendre()
        {

            foreach (Article art in ClasseGlobale._renduCommande.listArticles)
            {
                if (art.ifRendu == false)
                {
                    return true;
                }
            }
            return false;
        }

        private void txb_restitutionArticles_idFactures_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key >= Key.D0 && e.Key <= Key.D9) ; // it`s number
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ; // it`s number
            //else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
            //    e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
            //    e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
            //    e.Key == Key.System) ; // it`s a system key (add other key here if you want to allow)
            else
                e.Handled = true; // the key will sappressed
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Return)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(txt_CommandeBip.Text, "[^0-9]"))
                {
                    MessageBox.Show("Veuillez entrer que des nombres.");
                    txt_CommandeBip.Text = "";
                }
                else
                {
                    if (txt_CommandeBip.Text != "")
                    {
                        if (txt_CommandeBip.Text.Length > 9)
                        {
                            MessageBox.Show("Cette commande n'existe pas. ID trop long.");
                            txt_CommandeBip.Text = String.Empty;
                        }
                        else
                        {
                            RestitutionArticlesVM.ConfigClass.idcom = txt_CommandeBip.Text;
                            int idcommande = Int32.Parse(RestitutionArticlesVM.ConfigClass.idcom);
                            int idmax = CommandeDAO.selectMaxCommande();
                            if (idcommande > idmax)
                            {
                                MessageBox.Show("Cette commande n'existe pas.");
                            }
                            else
                            {
                                Commande com = CommandeDAO.selectCommandeByIdOnlyRendu(idcommande, false, true, true);
                                String daterendu = CommandeDAO.selectCommandeDateRendu(idcommande);
                                ClasseGlobale._renduCommande = com;
                                ClasseGlobale.Client = com.client;

                                if (daterendu == "")
                                {
                                    //MessageBox.Show("Pas rendu");
                                    if (ClasseGlobale._renduCommande != null)
                                    {



                                        dp.Children.Clear();
                                        dp.Children.Add(new DetailCommande());


                                    }
                                    else
                                    {
                                        MessageBox.Show("Selectionner une commande");
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Cette commande a déjà été rendue");
                                }
                                txt_CommandeBip.Text = "ugviygviygviy";
                            }
                        }








                    }
                    else
                    {
                        MessageBox.Show("Veuillez entrer l'id de la commande.");
                    }
                }



            }
            else { }
        }

        private void txt_CommandeBip_TextChanged(object sender, TextChangedEventArgs e)
        {
            /*
            if (System.Text.RegularExpressions.Regex.IsMatch(txt_CommandeBip.Text, "[^0-9]"))
            {
                MessageBox.Show("Veuillez entrer que des nombres.");
                txt_CommandeBip.Text = txt_CommandeBip.Text.Remove(txt_CommandeBip.Text.Length - 1);
            }
             * */

        }

    }
}