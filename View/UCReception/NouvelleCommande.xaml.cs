﻿using App_pressing_Loreau.Helper;
using App_pressing_Loreau.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using App_pressing_Loreau.Model;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace App_pressing_Loreau.View
{
    /// <summary>
    /// Logique d'interaction pour NouvelleCommande.xaml
    /// </summary>
    public partial class NouvelleCommande : UserControl
    {
        public NouvelleCommande()
        {
            InitializeComponent();
            DataContext = new NouvelleCommandeVM();
        }

        private void btn_nouvelleCommande_paiement_immediat_Click(object sender, RoutedEventArgs e)
        {
            float encombtotal = 0;
            float diff = 2500;
            int empdep = 0;
            float libre = 0;
            int stop = 0;
            PlaceConvoyeur place = new PlaceConvoyeur();
            PlaceConvoyeur placeautre = new PlaceConvoyeur();
            PlaceConvoyeur plapla = new PlaceConvoyeur();
            plapla=null;
            PlaceConvoyeur plaplaplus = new PlaceConvoyeur();
            plaplaplus = null;
            
            int placedep = 0;
            int bonneboucle = 0;
            int convsuite = 0;
            int plusdeplace = 0;

            //for (int i = 0; i<5; i++)
            //{
            //    placeautre = ClasseGlobale.PlacesLibres.getList()[i];
            //    MessageBox.Show("places libres :" + placeautre.emplacement+"et encombrement : "+placeautre.encombrement);
            //}
                try
                {


                    if (ClasseGlobale._contentDetailCommande != null)
                    {
                        if (ClasseGlobale._contentDetailCommande.Count != 0)
                        {
                            foreach (ArticlesVM artVm in ClasseGlobale._contentDetailCommande)
                            {
                                if (artVm.typeArticle.encombrement == 0 | artVm.typeArticle.encombrement > 2.5)
                                {

                                }
                                else
                                {
                                    encombtotal = encombtotal + artVm.typeArticle.encombrement;
                                }

                            }
                            //MessageBox.Show("total : " + encombtotal);

                            if (encombtotal <= 3 && encombtotal !=0)
                            {
                                //il ne faut qu'un emplacement
                                plapla = PlaceConvoyeurDAO.selectConvoyeurencombtotal1(encombtotal);
                                if (plapla.emplacement == 0)
                                {
                                    //MessageBox.Show("On ne peut pas tout mettre dans le même emplacement. Go alternative.");
                                    alternative();
                                }
                                else
                                {
                                    List<PlaceConvoyeur> lista = new List<PlaceConvoyeur>();
                                    foreach (ArticlesVM artVm in ClasseGlobale._contentDetailCommande)
                                    {
                                        artVm.PlaceConvoyeur = plapla;
                                        plapla.encombrement += artVm.typeArticle.encombrement;
                                        lista.Add(plapla);
                                        
                                    }
                                    ClasseGlobale.PlacesLibres.setList(lista);
                                    //foreach (PlaceConvoyeur placex in ClasseGlobale.PlacesLibres.getList())
                                    //{
                                    //    MessageBox.Show("placex : " + placex.emplacement);
                                    //}
                                    //MessageBox.Show("place utilisée : " + plapla.emplacement);
                                    
                                }
                            }
                            else
                            {
                                //requête en boucle ?
                                //MessageBox.Show("en cours..");


                                ClasseGlobale.PlacesLibres.setList(PlaceConvoyeurDAO.selectConvoyeurencombtotal2());
                                int finDeListe = ClasseGlobale.PlacesLibres.getList().Count();

                                if (finDeListe == 0)
                                {
                                    //MessageBox.Show("On passe par l'alternative.");
                                    alternative();
                                }
                                else
                                {
                                    while (bonneboucle == 0)
                                    {
                                        ClasseGlobale.PlacesLibres.setList(PlaceConvoyeurDAO.selectConvoyeurencombtotal2());
                                        finDeListe = ClasseGlobale.PlacesLibres.getList().Count();
                                        diff = encombtotal;
                                        foreach (ArticlesVM artu in ClasseGlobale._contentDetailCommande)
                                        {
                                            convsuite = 0;
                                            //MessageBox.Show(" ? : "+artu.PlaceConvoyeur.emplacement);
                                            if (artu.typeArticle.encombrement == 0 | artu.typeArticle.encombrement > 2.5)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                for (int j = placedep; j < finDeListe; j++)
                                                {
                                                    plapla = ClasseGlobale.PlacesLibres.getList()[j];

                                                    if (j == finDeListe - 1 && diff>(3-plapla.encombrement))
                                                    {
                                                        //MessageBox.Show("Ces articles ne seront pas placés à la suite dans le convoyeur.");
                                                        plusdeplace = 1;
                                                        break;
                                                    }

                                                    if (j != finDeListe - 1)
                                                    {
                                                        plaplaplus = ClasseGlobale.PlacesLibres.getList()[j + 1];
                                                    }


                                                    if (plaplaplus.emplacement - plapla.emplacement != 1 && diff > 3)
                                                    {
                                                        placedep += 1;
                                                        convsuite = 1;
                                                        break;

                                                    }

                                                    if (plapla.encombrement <= 3 - artu.typeArticle.encombrement)
                                                    {
                                                        //MessageBox.Show("encombrement de la place : " + plapla.encombrement + " et emplacement : " + plapla.emplacement);
                                                        ClasseGlobale.PlacesLibres[j].encombrement += artu.typeArticle.encombrement;
                                                        diff = diff - artu.typeArticle.encombrement;
                                                        //MessageBox.Show("diff : " + diff + "article entré : " + artu.typeArticle.nom);
                                                        placedep = j;
                                                        artu.PlaceConvoyeur = plapla;
                                                        break;
                                                    }



                                                }
                                                if (convsuite == 1)
                                                {
                                                    //MessageBox.Show("La place suivante n'a pas assez de place de libre. Actuelle : " + plapla.emplacement);
                                                    break;
                                                }
                                                if (diff <= 0)
                                                {
                                                    bonneboucle = 1;
                                                    break;
                                                }
                                                if (plusdeplace == 1)
                                                {
                                                    //MessageBox.Show("Fin du foreach");
                                                    break;
                                                }
                                            }

                                        }
                                        if (plusdeplace == 1)
                                        {
                                            //MessageBox.Show("Fin du while");
                                            break;
                                        }
                                    }
                                }

                                if (plusdeplace == 1)
                                {
                                    alternative();
                                }

                                if (bonneboucle == 1)
                                {
                                    //MessageBox.Show("YES !!!");
                                }
      
                            }


                            dp.Children.Clear();
                            dp.Children.Add(new Paiement());
                        }
                        else
                        {
                            MessageBox.Show("Veuillez ajouter des articles");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Aucune commande n'est en cours. \nVeuillez retourner à l'acccueil");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }
        }

        private void alternative()
        {
            PlaceConvoyeur place = new PlaceConvoyeur();
            place = null;
            int pastous = 0;
            ClasseGlobale.PlacesLibres.setList(PlaceConvoyeurDAO.selectConvoyeursEmpty());
            foreach (ArticlesVM artvm in ClasseGlobale._contentDetailCommande)
            {
                if (artvm.typeArticle.encombrement == 0 || artvm.typeArticle.encombrement > 2.5)
                {
                    //Cet article ne va pas dans le convoyeur
                }
                else
                {
                    //Je parcours la liste pour trouver une place convoyeur pouvant accueillir l'article
                    int finDeListe = ClasseGlobale.PlacesLibres.getList().Count();
                    float encombrement_occupe_pour_cette_place;
                    float encombrement_maximum = 3 - artvm.typeArticle.encombrement;
                    for (int i = 0; i < finDeListe; i++)
                    {
                        //si l'encombrement du convoyeur permet de recevoir l'article
                        encombrement_occupe_pour_cette_place = ClasseGlobale.PlacesLibres.getList()[i].encombrement;


                        if (encombrement_occupe_pour_cette_place <= encombrement_maximum)
                        {
                            //Je modifie l'encombrement de la place convoyeur
                            ClasseGlobale.PlacesLibres[i].encombrement += artvm.typeArticle.encombrement;
                            //Je récupère la place convoyeur concernée
                            place = ClasseGlobale.PlacesLibres.getList()[i];
                            artvm.PlaceConvoyeur = place;
                            //MessageBox.Show("place libre : "+ place.emplacement );
                            break;
                        }

                        if (i == finDeListe - 1)
                        {
                            pastous = 1;
                            
                        }
                    }
                }
            }

            if (pastous == 1)
            {
                MessageBox.Show("Tous les articles n'ont pas trouvé de place dans le convoyeur.");
            }
            //MessageBox.Show("Alternative OK.");
            

        }

        private void btn_nouvelleCommande_paiement_differe_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(ClasseGlobale._contentDetailCommande[0].SelectedPhoto);
            //AccueilVM acvm = new AccueilVM();
            //acvm.accueilVM();
        }

        private void btn_nouvelleCommande_paiement_differe_Click_1(object sender, RoutedEventArgs e)
        {
            float encombtotal = 0;
            float diff = 2500;
            int empdep = 0;
            float libre = 0;
            int stop = 0;
            PlaceConvoyeur place = new PlaceConvoyeur();
            PlaceConvoyeur placeautre = new PlaceConvoyeur();
            PlaceConvoyeur plapla = new PlaceConvoyeur();
            plapla = null;
            PlaceConvoyeur plaplaplus = new PlaceConvoyeur();
            plaplaplus = null;

            int placedep = 0;
            int bonneboucle = 0;
            int convsuite = 0;
            int plusdeplace = 0;

            //for (int i = 0; i<5; i++)
            //{
            //    placeautre = ClasseGlobale.PlacesLibres.getList()[i];
            //    MessageBox.Show("places libres :" + placeautre.emplacement+"et encombrement : "+placeautre.encombrement);
            //}
            try
            {


                if (ClasseGlobale._contentDetailCommande != null)
                {
                    if (ClasseGlobale._contentDetailCommande.Count != 0)
                    {
                        foreach (ArticlesVM artVm in ClasseGlobale._contentDetailCommande)
                        {
                            if (artVm.typeArticle.encombrement == 0 | artVm.typeArticle.encombrement > 2.5)
                            {

                            }
                            else
                            {
                                encombtotal = encombtotal + artVm.typeArticle.encombrement;
                            }

                        }
                        //MessageBox.Show("total : " + encombtotal);

                        if (encombtotal <= 3)
                        {
                            //il ne faut qu'un emplacement
                            plapla = PlaceConvoyeurDAO.selectConvoyeurencombtotal1(encombtotal);
                            if (plapla.emplacement == 0)
                            {
                                //MessageBox.Show("On ne peut pas tout mettre dans le même emplacement. Go alternative.");
                                alternative();
                            }
                            else
                            {
                                List<PlaceConvoyeur> lista = new List<PlaceConvoyeur>();
                                foreach (ArticlesVM artVm in ClasseGlobale._contentDetailCommande)
                                {
                                    artVm.PlaceConvoyeur = plapla;
                                    plapla.encombrement += artVm.typeArticle.encombrement;
                                    lista.Add(plapla);

                                }
                                ClasseGlobale.PlacesLibres.setList(lista);
                                foreach (PlaceConvoyeur placex in ClasseGlobale.PlacesLibres.getList())
                                {
                                    //MessageBox.Show("placex : " + placex.emplacement);
                                }
                                //MessageBox.Show("place utilisée : " + plapla.emplacement);

                            }
                        }
                        else
                        {
                            //requête en boucle ?
                            //MessageBox.Show("en cours..");


                            ClasseGlobale.PlacesLibres.setList(PlaceConvoyeurDAO.selectConvoyeurencombtotal2());
                            int finDeListe = ClasseGlobale.PlacesLibres.getList().Count();

                            if (finDeListe == 0)
                            {
                                //MessageBox.Show("On passe par l'alternative.");
                                alternative();
                            }
                            else
                            {
                                while (bonneboucle == 0)
                                {
                                    ClasseGlobale.PlacesLibres.setList(PlaceConvoyeurDAO.selectConvoyeurencombtotal2());
                                    finDeListe = ClasseGlobale.PlacesLibres.getList().Count();
                                    diff = encombtotal;
                                    foreach (ArticlesVM artu in ClasseGlobale._contentDetailCommande)
                                    {
                                        convsuite = 0;
                                        //MessageBox.Show(" ? : "+artu.PlaceConvoyeur.emplacement);
                                        if (artu.typeArticle.encombrement == 0 | artu.typeArticle.encombrement > 2.5)
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            for (int j = placedep; j < finDeListe; j++)
                                            {
                                                plapla = ClasseGlobale.PlacesLibres.getList()[j];

                                                if (j == finDeListe - 1 && diff > (3 - plapla.encombrement))
                                                {
                                                    //MessageBox.Show("Ces articles ne seront pas placés à la suite dans le convoyeur.");
                                                    plusdeplace = 1;
                                                    break;
                                                }

                                                if (j != finDeListe - 1)
                                                {
                                                    plaplaplus = ClasseGlobale.PlacesLibres.getList()[j + 1];
                                                }


                                                if (plaplaplus.emplacement - plapla.emplacement != 1 && diff > 3)
                                                {
                                                    placedep += 1;
                                                    convsuite = 1;
                                                    break;

                                                }

                                                if (plapla.encombrement <= 3 - artu.typeArticle.encombrement)
                                                {
                                                    //MessageBox.Show("encombrement de la place : " + plapla.encombrement + " et emplacement : " + plapla.emplacement);
                                                    ClasseGlobale.PlacesLibres[j].encombrement += artu.typeArticle.encombrement;
                                                    diff = diff - artu.typeArticle.encombrement;
                                                    //MessageBox.Show("diff : " + diff + "article entré : " + artu.typeArticle.nom);
                                                    placedep = j;
                                                    artu.PlaceConvoyeur = plapla;
                                                    break;
                                                }



                                            }
                                            if (convsuite == 1)
                                            {
                                                //MessageBox.Show("La place suivante n'a pas assez de place de libre. Actuelle : " + plapla.emplacement);
                                                break;
                                            }
                                            if (diff <= 0)
                                            {
                                                bonneboucle = 1;
                                                break;
                                            }
                                            if (plusdeplace == 1)
                                            {
                                                //MessageBox.Show("Fin du foreach");
                                                break;
                                            }
                                        }

                                    }
                                    if (plusdeplace == 1)
                                    {
                                        //MessageBox.Show("Fin du while");
                                        break;
                                    }
                                }
                            }

                            if (plusdeplace == 1)
                            {
                                alternative();
                            }

                            if (bonneboucle == 1)
                            {
                                //MessageBox.Show("YES !!!");
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Veuillez ajouter des articles");
                    }

                }
                else
                {
                    MessageBox.Show("Aucune commande n'est en cours. \nVeuillez retourner à l'acccueil");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
        }

    }
}
