﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using App_pressing_Loreau.ViewModel;


namespace App_pressing_Loreau.View
{
    /// <summary>
    /// Logique d'interaction pour Impression.xaml
    /// </summary>
    public partial class Impression : UserControl
    {
        public Impression()
        {
            InitializeComponent();
            DataContext = new ImpressionVM();

        }

        private void txt_Imprimerfacture_TextChanged_1(object sender, TextChangedEventArgs e)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(txt_Imprimerfacture.Text, "[^0-9]"))
            {
                MessageBox.Show("Veuillez entrer que des nombres.");
                txt_Imprimerfacture.Text = txt_Imprimerfacture.Text.Remove(txt_Imprimerfacture.Text.Length - 1);
            }


        }

    }
 
}
