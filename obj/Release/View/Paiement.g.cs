﻿#pragma checksum "..\..\..\View\Paiement.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "490931E81451839E8C086E618B0F5BF3"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.36366
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using App_pressing_Loreau.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace App_pressing_Loreau.View {
    
    
    /// <summary>
    /// Paiement
    /// </summary>
    public partial class Paiement : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel dp;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paiement_cartebleue;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paiement_especes;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paiement_cheques;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paiement_americanExpress;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paiement_solde;
        
        #line default
        #line hidden
        
        
        #line 163 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button valider;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_paiement_montant;
        
        #line default
        #line hidden
        
        
        #line 193 "..\..\..\View\Paiement.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_paiment_valider;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/App_pressing_Loreau;component/view/paiement.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\Paiement.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dp = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 2:
            this.btn_paiement_cartebleue = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.btn_paiement_especes = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.btn_paiement_cheques = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.btn_paiement_americanExpress = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.btn_paiement_solde = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.valider = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.label_paiement_montant = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.btn_paiment_valider = ((System.Windows.Controls.Button)(target));
            
            #line 194 "..\..\..\View\Paiement.xaml"
            this.btn_paiment_valider.Click += new System.Windows.RoutedEventHandler(this.btn_paiment_valider_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

