﻿#pragma checksum "..\..\..\..\View\UCAdministrateur\Statistiques.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "603680E706E223497FB5EE6C03DFCFF7"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.36366
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using App_pressing_Loreau.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace App_pressing_Loreau.View {
    
    
    /// <summary>
    /// Statistiques
    /// </summary>
    public partial class Statistiques : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel dp;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker choix_stat_1;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker choix_stat_2;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbb_stat_choixEmploye;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_statistique_choix;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_catotal;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbb_stat_choixDepart;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_statistiques_retour;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/App_pressing_Loreau;component/view/ucadministrateur/statistiques.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dp = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 2:
            this.choix_stat_1 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 3:
            this.choix_stat_2 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 4:
            this.cbb_stat_choixEmploye = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.btn_statistique_choix = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
            this.btn_statistique_choix.Click += new System.Windows.RoutedEventHandler(this.btn_statistique_choix_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.label_catotal = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.cbb_stat_choixDepart = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.btn_statistiques_retour = ((System.Windows.Controls.Button)(target));
            
            #line 151 "..\..\..\..\View\UCAdministrateur\Statistiques.xaml"
            this.btn_statistiques_retour.Click += new System.Windows.RoutedEventHandler(this.btn_statistiques_retour_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

