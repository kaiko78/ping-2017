﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows;
using App_pressing_Loreau.Data;


namespace App_pressing_Loreau.ViewModel
{
    /// <summary>
    /// Classe permettant d'enregistrer unn nouveau client et de collecter les informations le concernant
    /// </summary>
    class NouveauClientVM : ObservableObject, IPageViewModel
    {
        private string _txb_nouveauClient_date_naissance;

        #region Variables locales

        //public static int index { get; private set; }

        private Client _client;
        private bool dateDeNaissanceObligatoire;

        #endregion

        #region Constructeur
        public NouveauClientVM()
        {
            Client = new Client();
            dateDeNaissanceObligatoire = false;

        }
        #endregion

        public String Name
        {
            get { return " "; }
        }


        #region Properties / Commands

        public Client Client
        {
            get
            {
                return _client;
            }
            set
            {
                _client = value;
            }
        }
        public String Txb_nouveauClient_nom
        {
            get { return Client.nom; }
            set
            {
                if (value != Client.nom)
                {
                    Client.nom = value;
                    NotifyPropertychange("Txb_nouveauClient_nom");
                }
            }
        }

        public String Txb_nouveauClient_prenom
        {
            get { return Client.prenom; }
            set
            {
                if (value != Client.prenom)
                {
                    Client.prenom = value;
                    NotifyPropertychange("Txb_nouveauClient_prenom");
                }
            }
        }

        public String Txb_nouveauClient_date_naissance
        {
            get { return _txb_nouveauClient_date_naissance; }//ClasseGlobale.Client.dateNaissance.ToString().Split(' ')[0]
            set
            {
                if (value != _txb_nouveauClient_date_naissance)//ClasseGlobale.Client.dateNaissance.ToString()
                {
                    //ClasseGlobale.Client.dateNaissance = value;
                    _txb_nouveauClient_date_naissance = value;
                    //MessageBox.Show("Value changed : " + _txb_nouveauClient_date_naissance);
                    NotifyPropertychange("Txb_nouveauClient_date_naissance");
                }
            }

        }


        public float Txb_nouveauClient_solde
        {
            get { return Client.solde; }
            set
            {
                if (value != Client.solde)
                {
                    Client.solde = value;
                    NotifyPropertychange("Txb_nouveauClient_solde");
                }
            }
        }

        public String Txb_nouveauClient_numero
        {
            get
            {
                try
                {
                    return Client.adresse.numero;
                }
                catch (Exception e)
                {
                    return null;
                }

            }
            set
            {
                if (value != Client.adresse.numero)
                {
                    Client.adresse.numero = value;
                    NotifyPropertychange("Txb_nouveauClient_numero");
                }
            }
        }

        public String Txb_nouveauClient_rue_voie
        {
            get { return Client.adresse.rue; }
            set
            {
                if (value != Client.adresse.rue)
                {
                    Client.adresse.rue = value;
                    NotifyPropertychange("Txb_nouveauClient_rue_voie");
                }
            }
        }

        public String Txb_nouveauClient_bp
        {
            get { return Client.adresse.codePostal; }
            set
            {
                if (value != Client.adresse.codePostal)
                {
                    Client.adresse.codePostal = value;
                    NotifyPropertychange("Txb_nouveauClient_bp");
                }
            }
        }

        public String Txb_nouveauClient_ville
        {
            get { return Client.adresse.ville; }
            set
            {
                if (value != Client.adresse.ville)
                {
                    Client.adresse.ville = value;
                    NotifyPropertychange("Txb_nouveauClient_ville");

                }
            }
        }

        public bool Ckb_nouveauClient_sms
        {
            get { return Client.contactSms; }
            set
            {
                if (value != Client.contactSms)
                {
                    Client.contactSms = value;
                    NotifyPropertychange("Ckb_nouveauClient_sms");
                }
            }
        }
        public String Txb_nouveauClient_portable
        {
            get { return Client.telmob; }
            set
            {
                if (value != Client.telmob)
                {
                    Client.telmob = value;
                    NotifyPropertychange("Txb_nouveauClient_portable");
                }
            }
        }


        public bool Ckb_nouveauClient_mail
        {
            get { return Client.contactMail; }
            set
            {
                if (value != Client.contactMail)
                {
                    Client.contactMail = value;
                    NotifyPropertychange("Ckb_nouveauClient_mail");
                }
            }
        }

        public String Txb_nouveauClient_mail
        {
            get { return Client.email; }
            set
            {
                if (value != Client.email)
                {
                    Client.email = value;
                    NotifyPropertychange("Txb_nouveauClient_mail");
                }
            }
        }


        public ICommand BtnNouveauClientEnregistrer
        {
            get
            {
                
                return new RelayCommand(
                    
                    p => enregisterClient());
            }
        }



        #endregion


        #region Méthodes

        public void enregisterClient()
        {

            if(Txb_nouveauClient_portable!="" && Txb_nouveauClient_portable.Length!=10)
            {
                MessageBox.Show("Veuillez entrer un numéro de téléphone de 10 chiffres.");
            }
            else if (Txb_nouveauClient_nom == "")
            {                
                MessageBox.Show("Veuillez entrer un nom.");
            }
            else if (Txb_nouveauClient_prenom == "")
            {
                MessageBox.Show("Veuillez entrer un prenom.");
            }
            else {


                //Si le client n'à déjà été enregistré en bdd
                if (ClasseGlobale.Client.nom != Txb_nouveauClient_nom &&
                    ClasseGlobale.Client.prenom != Txb_nouveauClient_prenom)
                {
                    bool check = false;
                    if (Client != null)
                    {
                        if (Client.nom != "" && Client.prenom != "")
                        {
                            

                            //Vérification de l'existence du nom et du prénom en base de données
                            if (ClientDAO.verificationNomEtPrenom(Client.nom, Client.prenom))
                            {
                                
                                MessageBox.Show("Un client portant le même nom et prénom existe déjà.\n" +
                                    "Veuillez le différencier (les chiffres sont acceptés).");
                            }
                            else
                            {
                                if (check == false)//Si je n'ai pas rencontré de problème au niveau de la date de naissance
                                {
                                    //Formatage des champs => évite les problèmes de comparaison de chaines
                                    Client.prenom = Client.prenom.Substring(0, 1).ToUpper() + Client.prenom.Substring(1, Client.prenom.Length - 1).ToLower();
                                    Client.nom = Client.nom.ToUpper();

                                    if (ClientDAO.insertClient(Client) == 1)
                                    {
                                        Client client = ClientDAO.lastClient();

                                        if (client == null)
                                        {
                                            MessageBox.Show("Problème de récupération du dernier client en BDD");
                                        }
                                        else
                                        {
                                            ClasseGlobale.Client = client;
                                            MessageBox.Show("Nouveau client enregistré avec succès.\nCliquez sur Nouvelle commande pour accéder à l'écran suivant");
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Problème d'enregistrement du client dans la base de données");
                                    }
                                }
                            }
                        }
                       

                    }
                    else
                    {
                        MessageBox.Show("Le client n'a pas été initialisée, cette erreur logiciel n'est pas censée arriver. Cf code NouveauClientVM.cs l~341");
                    }
                }
                else
                {
                    MessageBox.Show("Le client a déjà été enregistré : \n" +
                        "soit, cliquez sur nouvelle commande\n" +
                        "soit, renseignez de nouveau les champs pour enregistrer un nouveau client");
                }
            }
            

        }

        #endregion

    }
}
