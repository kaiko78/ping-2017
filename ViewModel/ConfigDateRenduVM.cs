﻿using App_pressing_Loreau.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace App_pressing_Loreau.ViewModel
{
    class ConfigDateRenduVM : ObservableObject
    {
        public static DateTime DatePressing { get; set; }
        public static DateTime DateBlanchisserie { get; set; }

        private void DatePicker_SelectedDateChanged_pressing(object sender,
        SelectionChangedEventArgs e)
        {
            // ... Get DatePicker reference.
            var picker = sender as DatePicker;

            // ... Get nullable DateTime from SelectedDate.
            DateTime? date = picker.SelectedDate;
            if (date == null)
            {
                // ... A null object.
                date = DatePressing;
            }
           
        }
        private void DatePicker_SelectedDateChanged_blanchisserie(object sender,
        SelectionChangedEventArgs e)
        {
            // ... Get DatePicker reference.
            var picker = sender as DatePicker;

            // ... Get nullable DateTime from SelectedDate.
          
        }
       
        public ICommand Btn_pageDemarrage_valider
        {
            get;
            set;
        }
    }
}
