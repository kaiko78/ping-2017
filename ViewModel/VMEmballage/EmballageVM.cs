﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;

using App_pressing_Loreau.View;
using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows;
using Microsoft.Practices.Prism.Commands;



namespace App_pressing_Loreau.ViewModel
{
    class EmballageVM : ObservableObject, IPageViewModel
    {

        #region Attributs


        public List<CommandeConcernantRA_DATA> _contentCommandeConcernantAttente = null;
        public List<CommandeConcernantRA_DATA> _contentCommandeConcernantBip = null;

        private float _label_CommandeBip = 0;
        private float _label_CommandeAttente = 0;
        private string _txt_CommandeAttente;

        private int id_com;

        #endregion

        public EmballageVM()
        {
            ClasseGlobale.Client = null;

            ClasseGlobale._renduCommande = null;
            ClasseGlobale._contentDetailCommande = null;
            Attente();
           Bip();

        }


        public static class ConfigClass
        {
            public static string idcom { get; set; }

        }
        public List<CommandeConcernantRA_DATA> ContentCommandeConcernantAttente
        {
            get { return _contentCommandeConcernantAttente; }
            set
            {
                if (value != _contentCommandeConcernantAttente)
                {
                    _contentCommandeConcernantAttente = value;
                    NotifyPropertychange("ContentCommandeConcernantAttente");
                }
            }

        }

        public List<CommandeConcernantRA_DATA> ContentCommandeConcernantBip
        {
            get { return _contentCommandeConcernantBip; }
            set
            {
                if (value != _contentCommandeConcernantBip)
                {
                    _contentCommandeConcernantBip = value;
                    NotifyPropertychange("ContentCommandeConcernantBip");
                }
            }

        }

        public float Label_CommandeBip
        {
            get { return _label_CommandeBip; }
            set
            {
                if (value != _label_CommandeBip)
                {
                    _label_CommandeBip = value;
                    NotifyPropertychange("Label_CommandeBip");
                }
            }
        }

        public float Label_CommandeAttente
        {
            get { return _label_CommandeAttente; }
            set
            {
                if (value != _label_CommandeAttente)
                {
                    _label_CommandeAttente = value;
                    NotifyPropertychange("Label_CommandeAttente");
                }
            }
        }

        public String Txt_CommandeAttente
        {
            
            get { return _txt_CommandeAttente; }
            set
            {
                
                if (value != _txt_CommandeAttente)
                {
                    _txt_CommandeAttente = value;
                    NotifyPropertychange("Txt_CommandeAttente");
                    if (_txt_CommandeAttente == "ugviygviygviy")
                    {
                        
                        id_com = Int32.Parse(ConfigClass.idcom);
                        //MessageBox.Show("TEST" + id_com);
                        SmsAttente(id_com);
                        _txt_CommandeAttente = "";
                        Label_CommandeAttente = 0;
                        Bip();
                        Attente();

                    }
                    
                }
            }
        }


        


        public string Name
        {
            get { return ""; }
        }





        private void SmsAttente(int idcommande)
        {
            Commande com = CommandeDAO.selectCommandeByIdAttente(idcommande);
            CommandeDAO.updateCommandeAttente(idcommande);
            /*Choper l'id de la commande OK
             * Choper la commande OK
             * Update cette commande en changeant le timestamp OK
             * (Envoyer le sms)
             * Passer la commande dans la fenêtre de gauche donc faire une list commande comme dans Attente
             * Refaire une requête Attente OK
             * 
             * 
             * 
             * */
        }


        //Fonction qui permet de lister les commandes en attente sur la droite dans la partie Emballage
        private void Attente()
        {
            List<Commande> listeCommandeAttente = (List<Commande>)CommandeDAO.selectCommandeAttente();


            ContentCommandeConcernantAttente = new List<CommandeConcernantRA_DATA>();

            if(listeCommandeAttente.Count > 0)
            {
                Label_CommandeAttente = listeCommandeAttente.Count;
                foreach (Commande com1 in listeCommandeAttente)
                {

                    String etat = null;
                    if (com1.payee == true)
                    {
                        etat = "Commande payée";
                    }
                    else
                    {
                        etat = "Commande non payée";
                    }
                    ContentCommandeConcernantAttente.Add(new CommandeConcernantRA_DATA()
                    {
                        Label_restitutionArticles_Reference = com1.id,
                        Label_restitutionArticles_DateCommande = com1.date.ToString(),
                        commande = com1,
                        Label_restitutionArticles_Etat = etat,
                        Label_restitutionArticles_nomDuClient = com1.nom_client + "  " + com1.prenom_client
                    });

                }
            }
            else
            {
               // MessageBox.Show("Aucune commande en attente.");
            }
             
        }

        /*Commande affichant les commandes bipées de la journée sur la gauche
         * 
         */
        private void Bip()
        {



            List<Commande> listeCommandeBip = (List<Commande>)CommandeDAO.selectCommandeSms();


            ContentCommandeConcernantBip = new List<CommandeConcernantRA_DATA>();

            if (listeCommandeBip.Count > 0)
            {
                Label_CommandeBip = listeCommandeBip.Count;
                foreach (Commande com1 in listeCommandeBip)
                {

                    String etat = null;
                    if (com1.payee == true)
                    {
                        etat = "Commande payée";
                    }
                    else
                    {
                        etat = "Commande non payée";
                    }
                    ContentCommandeConcernantBip.Add(new CommandeConcernantRA_DATA()
                    {
                        Label_restitutionArticles_Reference = com1.id,
                        Label_restitutionArticles_DateCommande = com1.date.ToString(),
                        commande = com1,
                        Label_restitutionArticles_Etat = etat,
                        Label_restitutionArticles_nomDuClient = com1.nom_client + "  " + com1.prenom_client
                    });

                }
            }
            else
            {
               // MessageBox.Show("Aucune commande en attente.");
            }
        }

    }
}
