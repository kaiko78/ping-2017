﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Practices.Prism.Commands;

namespace App_pressing_Loreau.ViewModel
{
    class SmsVM : ObservableObject, IPageViewModel
    {

        #region Variables

private List<SmsClientData> _resultatRecherche_sms;


        private String _label_sms_choix;
 private DelegateCommand<SmsClientData> _resultatRechercheClient;


        fields2 fields2Recherche = AutoComplete2.getfields22();
        #endregion
      

        public string Name
        {
            get { return ""; }
        }
         public SmsVM()
        {
            ClasseGlobale.Client = null;
            
            ClasseGlobale._renduCommande = null;
            ClasseGlobale._contentDetailCommande = null;

        }
		 #region Properties
        // Affichage du client choisi
        public String Label_sms_choix
        {
            get { return _label_sms_choix; }
            set
            {
                if (value != _label_sms_choix)
                {
                    _label_sms_choix = value;
                    NotifyPropertychange("Label_sms_choix");
                }
            }
        }
		 //Action de la commande  button recherche 
        public ICommand Btn_sms_recherche
        {
            get { return new RelayCommand(p => rechercheBDD()); }
        }
//la liste de la recherche
        public List<SmsClientData> ResultatRecherche_sms
        {
            get { return _resultatRecherche_sms; }
            set
            {
                if (value != _resultatRecherche_sms)
                {
                    _resultatRecherche_sms = value;
                    NotifyPropertychange("ResultatRecherche_sms");
                }
            }
        }
        // L action sur le resultat de la recherche
        public DelegateCommand<SmsClientData> ResultatRechercheClient
        {
            get
            {
                return this._resultatRechercheClient ?? (this._resultatRechercheClient = new DelegateCommand<SmsClientData>(
                                                                       this.ExecuteAddClient,
                                                                       (arg) => true));
            }
        }


        //Champs recherche
        public string Txb_sms_nom
        {
            get { return fields2Recherche.nom; }
            set
            {

                fields2Recherche.nom = value;
                NotifyPropertychange("Txb_sms_nom");

            }
        }
		 public string Txb_sms_prenom
        {
            get { return fields2Recherche.prenom; }
            set
            {

                fields2Recherche.prenom = value;
                NotifyPropertychange("Txb_sms_prenom");

            }
        }
        public string Txb_sms_telephone
        {
            get { return fields2Recherche.telephone; }
            set
            {

                fields2Recherche.telephone = value;
                NotifyPropertychange("Txb_sms_telephone");

            }
        }
		public float Txb_sms_solde
        {
            get { return fields2Recherche.solde; }
            set
            {
                if (value != fields2Recherche.solde)
                {
                    fields2Recherche.solde = value;
                    NotifyPropertychange("Txb_sms_solde");
                }
            }
        }

        #endregion
        #region methodes

        //Ajout d un client a la proprieté global Client
        private void ExecuteAddClient(SmsClientData obj)
        {

            if (ClasseGlobale.Client != obj.clt)
            {
                ClasseGlobale.Client = obj.clt;

            }

            if (ClasseGlobale.Client != null)
            {
                Label_sms_choix = "Choix = " + ClasseGlobale.Client.nom + " " + ClasseGlobale.Client.prenom + " " + ClasseGlobale.Client.telmob;
              
            }
        }
        private void Initfields2()
        {
            Txb_sms_nom = null;
            Txb_sms_prenom = null;
            Txb_sms_telephone = null;
            Txb_sms_solde = 0;
        }
		 public void rechercheBDD()
        {
            //On recherche dans la bdd en fonction des champs que l'utilisateur a entrés
            fields2 fields2 = AutoComplete2.getfields22();

            if (fields2 != null)
            {

                ResultatRecherche_sms = new List<SmsClientData>();
                if (fields2.nom == null) { fields2.nom = ""; }
                if (fields2.prenom == null) { fields2.prenom = ""; }
                if (fields2.telephone == null) { fields2.telephone = ""; }
                try
                {

                    List<Client> resultat = ClientDAO.seekClients(fields2.nom, fields2.prenom, fields2.telephone, fields2.solde);

                    //On affiche le résultat dans le doc Panel
                    if (resultat != null)
                    {
                        foreach (Client c in resultat)
                        {
                            ResultatRecherche_sms.Add(new SmsClientData() { clt = c });
                        }
                    }
                    else
                    {
                        MessageBox.Show("Erreur : la recherche n'a renvoyée aucun résultat");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur de recherche :" + ex);
                }
                finally
                {
                    Initfields2();
                }
            }
        }
        #endregion

    }


		#region Class

    public class AutoComplete2
    {
       static fields2 fields2;
        public static fields2 getfields22()
        {

            if (fields2 == null)
            {
                fields2 = new fields2();
                return fields2;
            }
            else
            {
                return fields2;
            }

        }
	   
        public static string parseFromClassToMessage(string classeuh)
        {
            string[] salutTableau = classeuh.Split(':');
            string contenuDuChampDeText = salutTableau[salutTableau.Length - 1];
            return contenuDuChampDeText;
        }

    }

    
    public class fields2
    {
        public String nom { get; set; }
        public String prenom { get; set; }
        public String telephone { get; set; }
        public float solde { get; set; }
        public String adresse { get; set; }
        public String dateDeNaissance { get; set; }
    }
    #endregion
}