﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using App_pressing_Loreau.Model;


namespace App_pressing_Loreau.ViewModel
{
    class SmsClientData : ObservableObject
    {
        #region Variables

        private String _label_sms_Adresse;
        public int ButtonClientTag { get; set; }

        public Client clt;

        #endregion

        #region Properties 

        public String Label_sms_nom
        {
            get { return this.clt.nom; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.clt.nom = value;
                    NotifyPropertychange("Label_sms_nom");
                }
            }
        }
        public String Label_sms_telephone
        {
            get { return this.clt.telmob; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.clt.nom = value;
                    NotifyPropertychange("Label_sms_telephone");
                }
            }
        }

        public String Label_sms_prenom
        {
            get { return this.clt.prenom; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.clt.prenom = value;
                    NotifyPropertychange("Label_sms_prenom");
                }
            }
        }
        public DateTime Label_sms_DateNaissance
        {
            get { return this.clt.dateNaissance; }
            set
            {
                this.clt.dateNaissance = value;
                NotifyPropertychange("Label_identCleint_DateNaissance");
            }
        }
        
        public String Label_sms_Adresse
        {
            get { return this.clt.adresse.ToString(); }
            set
            {
                if (value != this.clt.adresse.ToString())
                {
                    _label_sms_Adresse = this.clt.adresse.ToString();
                    _label_sms_Adresse = value;
                    NotifyPropertychange("Label_sms_Adresse");
                }
            }
        }

        #endregion
    }
}

