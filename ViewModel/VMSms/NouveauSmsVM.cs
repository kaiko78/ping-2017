﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_pressing_Loreau.View;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using App_pressing_Loreau.Helper;

namespace App_pressing_Loreau.ViewModel
{
    public class Message
    {
        public String Name { get; set; }
    }
    public class NouveauSmsVM : System.ComponentModel.INotifyPropertyChanged
    {
        internal string Name;
        ObservableCollection<Message> messageList;
        Message selectedMessage;

        public NouveauSmsVM()
        {
            //populate some sample data
           
            messageList = new ObservableCollection<Message>()
        {
            new Message(){Name="Bonjour "+ClasseGlobale.Client.telmob+", merci de venir chercher votre linge au pressing."},
            new Message(){Name="Bonjour, votre linge est propre."},
            new Message(){Name="Bonjour, il y a un problème avec votre linge."},
            new Message(){Name="Bonjour, une partie de votre linge est prêt."}
        };
        }

        public static class ConfigClass
        {
            public static int MyProperty { get; set; }
            public static String testmessage { get; set; }
        }
       

        #region Properties

        public ObservableCollection<Message> MessageList
        {
            get { return messageList; }
        }

        public Message SelectedMessage
        {
            get { return selectedMessage; }
            set
            {
                selectedMessage = value;
                RaisePropertyChanged("SelectedMessage");
                NouveauSmsVM.ConfigClass.testmessage = value.Name;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
