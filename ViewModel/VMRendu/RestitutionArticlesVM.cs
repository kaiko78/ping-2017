﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows;
using Microsoft.Practices.Prism.Commands;


namespace App_pressing_Loreau.ViewModel
{
    /// <summary>
    /// ViewModel pour la vue RestitutionArticles.xaml
    /// 
    /// </summary>
    class RestitutionArticlesVM : ObservableObject, IPageViewModel
    {
        #region Attributs

        private int _txb_restitutionArticles_idFactures;
        private string _txb_restitutionArticles_choix;
        private string _txt_CommandeBip;
        private int id_com;


        ChoixBox choixbox = new ChoixBox();
        private ChoixBox _selected_restitutionClient_choix_theme;
        public List<ChoixBox> Cbb_restitutionClient_choix_theme { get; set; }

        public List<CommandeConcernantRA_DATA> _contentCommandeConcernant;
        public List<CommandeConcernantRA_DATA> _listeRechercheClient;
        private DelegateCommand<CommandeConcernantRA_DATA> commandeParIdFacture;
        private DelegateCommand<CommandeConcernantRA_DATA> _getButtonRecherche;


        private String _label_commandeSelectionner;
        #endregion

        #region Accesseurs et mutateurs des attributs qui ne sont pas propriétés
        public List<CommandeConcernantRA_DATA> ContentCommandeConcernant
        {
            get { return _contentCommandeConcernant; }
            set
            {
                if (value != _contentCommandeConcernant)
                {
                    _contentCommandeConcernant = value;
                    NotifyPropertychange("ContentCommandeConcernant");
                }
            }

        }


        public static class ConfigClass
        {
            public static string idcom { get; set; }

        }

        public List<CommandeConcernantRA_DATA> ListeRechercheClient
        {
            get { return _listeRechercheClient; }
            set
            {
                _listeRechercheClient = value;
                NotifyPropertychange("ListeRechercheClient");
            }
        }

        public String Txt_CommandeBip
        {

            get { return _txt_CommandeBip; }
            set
            {

                if (value != _txt_CommandeBip)
                {
                    _txt_CommandeBip = value;
                    NotifyPropertychange("Txt_CommandeBip");
                    if (_txt_CommandeBip == "ugviygviygviy")
                    {

                        id_com = Int32.Parse(ConfigClass.idcom);
                        //MessageBox.Show("TEST" + id_com);

                        
                        _txt_CommandeBip = "";


                    }

                }
            }
        }


        #endregion

        public String Name
        {
            get { return ""; }

        }

        #region Constructeur
        public RestitutionArticlesVM()
        {
            Cbb_restitutionClient_choix_theme = choixbox.ListeChamp();
            ClasseGlobale.Client = null;

            ClasseGlobale._renduCommande = null;

            ClasseGlobale._contentDetailCommande = null;

        }


        #endregion


        #region Propriétés
        public int Txb_restitutionArticles_idFactures
        {
            get { return _txb_restitutionArticles_idFactures; }
            set
            {

                _txb_restitutionArticles_idFactures = value;
                NotifyPropertychange("Txb_restitutionArticles_idFactures");


            }
        }

        public String Label_CommandeSelectionner
        {
            get { return _label_commandeSelectionner; }
            set
            {
                if (value != _label_commandeSelectionner)
                {
                    _label_commandeSelectionner = value;
                    NotifyPropertychange("Label_CommandeSelectionner");
                }
            }
        }
        public String Txb_restitutionArticles_choix
        {
            get { return _txb_restitutionArticles_choix; }
            set
            {
                _txb_restitutionArticles_choix = value;
                NotifyPropertychange("Txb_restitutionArticles_choix");
            }

        }


        public ChoixBox Selected_restitutionClient_choix_theme
        {
            get { return _selected_restitutionClient_choix_theme; }
            set
            {
                _selected_restitutionClient_choix_theme = value;
                RaisePropertyChanged("Selected_restitutionClient_choix_theme");
            }
        }

        #endregion


        #region Commandes

        #region Champ de recherche par id de commande

        public ICommand Btn_restitutionArticles_ok
        {
            get
            {
                return new RelayCommand(
                    p => ContenuDeLaCommande(),
                    p => Txb_restitutionArticles_idFactures > 0);
            }
        }
        public void ContenuDeLaCommande()
        {
            if (Txb_restitutionArticles_idFactures > 0)
            {
                Commande commandeRendre = (Commande)CommandeDAO.selectCommandeById(Txb_restitutionArticles_idFactures, false, true, true);
                if (commandeRendre.id != 0)
                {
                    ContentCommandeConcernant = new List<CommandeConcernantRA_DATA>();

                    foreach (Article art in commandeRendre.listArticles)
                    {
                        //Si l'un des articles n'est pas rendu, j'ajoute la commande à la liste
                        if (art.ifRendu == false)
                        {
                            String etat = null;
                            if (commandeRendre.payee == true)
                            {
                                etat = "Commande payée";
                            }
                            else
                            {
                                etat = "Commande non payée";
                            }
                            ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                            {
                                Label_restitutionArticles_Reference = commandeRendre.id,
                                Label_restitutionArticles_DateCommande = commandeRendre.date.ToString(),
                                commande = commandeRendre,
                                Label_restitutionArticles_Etat = etat,
                                Label_restitutionArticles_nomDuClient = commandeRendre.client.nom + "  " + commandeRendre.client.prenom
                            });
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Cette Commande n'existe pas");
                }
            }
            else
            {
                MessageBox.Show("Un id de commande ne peut pas être négatif ou nul");
            }
        }

        #endregion

        #region Recherche par nom ou prénom pour afficher une liste de clients


        public ICommand Btn_restitutionArticles_valider
        {
            get
            {
                return new RelayCommand(
                    p => ContenuDeLaRecherche(),
                    p => Txb_restitutionArticles_choix != null);
            }
        }
        public void ContenuDeLaRecherche()
        {
            
            ListeRechercheClient = new List<CommandeConcernantRA_DATA>();
            List<Client> resultat = null;
            if (Selected_restitutionClient_choix_theme != null)
            {


                if (Selected_restitutionClient_choix_theme.NameCbb.Equals("Nom"))
                {
                    resultat = ClientDAO.seekClients(Txb_restitutionArticles_choix, null, null, 0);
                }
                else if (Selected_restitutionClient_choix_theme.NameCbb.Equals("Prenom"))
                {
                    resultat = ClientDAO.seekClients(null, Txb_restitutionArticles_choix, null, 0);
                }
                else if (Selected_restitutionClient_choix_theme.NameCbb.Equals("Telephone"))
                {
                    resultat = ClientDAO.seekClients(null, null, Txb_restitutionArticles_choix, 0);
                }
                else resultat = null;

                if (resultat != null)
                {
                    if (resultat.Count == 2)
                    {


                        ListeRechercheClient.Add(new CommandeConcernantRA_DATA()
                        {
                            clt = resultat[0]
                        });
                        ListeRechercheClient.Add(new CommandeConcernantRA_DATA()
                        {
                            clt = resultat[1]
                        });
                        ListeRechercheClient.Add(new CommandeConcernantRA_DATA()
                        {
                            clt = resultat[1]
                        });


                    }
                    else
                    {
                        foreach (Client clt in resultat)
                        {
                            ListeRechercheClient.Add(new CommandeConcernantRA_DATA()
                            {
                                clt = clt
                            });

                        }
                    }

                }
                else
                {
                    MessageBox.Show("Pas de resultat ");
                }
            }
            else
            {
                MessageBox.Show("Choisissez un élement ");
            }
        }




        #endregion



        #region Recherche des commandes en cours d'un client
        public DelegateCommand<CommandeConcernantRA_DATA> GetButtonRecherche
        {
            get
            {
                return this._getButtonRecherche ?? (this._getButtonRecherche = new DelegateCommand<CommandeConcernantRA_DATA>(
                                                                       this.ExecuteResultatRechercheClient,
                                                                       (arg) => true));
            }
        }


        private void ExecuteResultatRechercheClient(CommandeConcernantRA_DATA obj)
        {

            ContentCommandeConcernant = new List<CommandeConcernantRA_DATA>();

            if (obj.clt.type == 0)
            {
                //List<Commande> listeCommande = (List<Commande>)CommandeDAO.selectCommandesByClient(obj.clt.id, false, true, true);
                List<Commande> listeCommandePasSms = (List<Commande>)CommandeDAO.selectCommandesByClientPasSms(obj.clt.id, false, true, true);
                List<Commande> listeCommandeSms = (List<Commande>)CommandeDAO.selectCommandesByClientSms(obj.clt.id, false, true, true);
                List<Commande> listeCommandedate = (List<Commande>)CommandeDAO.selectCommandesByDate(obj.clt.id, false, true, true);
                List<Commande> listeCommandedernier = (List<Commande>)CommandeDAO.selectCommandesDernier(obj.clt.id, false, true, true);

                if (listeCommandePasSms != null)
                {
                    if (listeCommandePasSms.Count == 2)
                    {

                        Commande com1 = listeCommandePasSms[0];
                        Commande com2 = listeCommandePasSms[1];
                        foreach (Article art in com1.listArticles)
                        {
                            //On affiche la première commande
                            if (art.ifRendu == false)
                            {
                                String etat = null;
                                if (com1.payee == true)
                                {
                                    etat = "Commande payée";
                                }
                                else
                                {
                                    etat = "Commande non payée";
                                }
                                ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                {
                                    Label_restitutionArticles_Reference = com1.id,
                                    Label_restitutionArticles_DateCommande = com1.date.ToString(),
                                    commande = com1,
                                    Label_restitutionArticles_Etat = etat,
                                    Label_restitutionArticles_nomDuClient = com1.client.nom + "  " + com1.client.prenom
                                });
                                break;
                            }
                        }

                        foreach (Article art in com2.listArticles)
                        {
                            //On affiche la 2ème commande
                            if (art.ifRendu == false)
                            {
                                String etat = null;
                                if (com2.payee == true)
                                {
                                    etat = "Commande payée";
                                }
                                else
                                {
                                    etat = "Commande non payée";
                                }
                                ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                {
                                    Label_restitutionArticles_Reference = com2.id,
                                    Label_restitutionArticles_DateCommande = com2.date.ToString(),
                                    commande = com2,
                                    Label_restitutionArticles_Etat = etat,
                                    Label_restitutionArticles_nomDuClient = com2.client.nom + "  " + com2.client.prenom
                                });
                                break;
                            }
                        }

                        foreach (Article art in com2.listArticles)
                        {
                            //On affiche la 2ème commande 2ème fois
                            if (art.ifRendu == false)
                            {
                                String etat = null;
                                if (com2.payee == true)
                                {
                                    etat = "Commande payée";
                                }
                                else
                                {
                                    etat = "Commande non payée";
                                }
                                ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                {
                                    Label_restitutionArticles_Reference = com2.id,
                                    Label_restitutionArticles_DateCommande = com2.date.ToString(),
                                    commande = com2,
                                    Label_restitutionArticles_Etat = etat,
                                    Label_restitutionArticles_nomDuClient = com2.client.nom + "  " + com2.client.prenom
                                });
                                break;
                            }
                        }

                    }
                    else
                    {
                        foreach (Commande com in listeCommandePasSms)
                        {

                            foreach (Article art in com.listArticles)
                            {

                                //Si l'un des articles n'est pas rendu, j'ajoute la commande à la liste
                                if (art.ifRendu == false)
                                {
                                    String etat = null;
                                    if (com.payee == true)
                                    {
                                        etat = "Commande payée";
                                    }
                                    else
                                    {
                                        etat = "Commande non payée";
                                    }
                                    ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                    {
                                        Label_restitutionArticles_Reference = com.id,
                                        Label_restitutionArticles_DateCommande = com.date.ToString(),
                                        commande = com,
                                        Label_restitutionArticles_Etat = etat,
                                        Label_restitutionArticles_nomDuClient = com.client.nom + "  " + com.client.prenom
                                    });
                                    break;
                                }

                            }
                        }
                    }

                }

                if (listeCommandeSms != null)
                {
                 if (listeCommandeSms.Count == 2)
                    {

                        Commande com1 = listeCommandeSms[0];
                        Commande com2 = listeCommandeSms[1];
                        foreach (Article art in com1.listArticles)
                        {
                            //On affiche la première commande
                            if (art.ifRendu == false)
                            {
                                String etat = null;
                                if (com1.payee == true)
                                {
                                    etat = "Commande payée";
                                }
                                else
                                {
                                    etat = "Commande non payée";
                                }
                                ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                {
                                    Label_restitutionArticles_Reference = com1.id,
                                    Label_restitutionArticles_DateCommandesms = com1.date.ToString(),
                                    commande = com1,
                                    Label_restitutionArticles_Etatsms = etat,
                                    Label_restitutionArticles_nomDuClientsms = com1.client.nom + "  " + com1.client.prenom
                                });
                                break;
                            }
                        }

                        foreach (Article art in com2.listArticles)
                        {
                            //On affiche la 2ème commande
                            if (art.ifRendu == false)
                            {
                                String etat = null;
                                if (com2.payee == true)
                                {
                                    etat = "Commande payée";
                                }
                                else
                                {
                                    etat = "Commande non payée";
                                }
                                ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                {
                                    Label_restitutionArticles_Reference = com2.id,
                                    Label_restitutionArticles_DateCommandesms = com2.date.ToString(),
                                    commande = com2,
                                    Label_restitutionArticles_Etatsms = etat,
                                    Label_restitutionArticles_nomDuClientsms = com2.client.nom + "  " + com2.client.prenom
                                });
                                break;
                            }
                        }

                        foreach (Article art in com2.listArticles)
                        {
                            //On affiche la 2ème commande 2ème fois
                            if (art.ifRendu == false)
                            {
                                String etat = null;
                                if (com2.payee == true)
                                {
                                    etat = "Commande payée";
                                }
                                else
                                {
                                    etat = "Commande non payée";
                                }
                                ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                {
                                    Label_restitutionArticles_Reference = com2.id,
                                    Label_restitutionArticles_DateCommandesms = com2.date.ToString(),
                                    commande = com2,
                                    Label_restitutionArticles_Etatsms = etat,
                                    Label_restitutionArticles_nomDuClientsms = com2.client.nom + "  " + com2.client.prenom
                                });
                                break;
                            }
                        }

                    }
                    else
                    {
                        foreach (Commande com in listeCommandeSms)
                        {

                            foreach (Article art in com.listArticles)
                            {

                                //Si l'un des articles n'est pas rendu, j'ajoute la commande à la liste
                                if (art.ifRendu == false)
                                {
                                    String etat = null;
                                    if (com.payee == true)
                                    {
                                        etat = "Commande payée";
                                    }
                                    else
                                    {
                                        etat = "Commande non payée";
                                    }
                                    ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                    {
                                        Label_restitutionArticles_Reference = com.id,
                                        Label_restitutionArticles_DateCommandesms = com.date.ToString(),
                                        commande = com,
                                        Label_restitutionArticles_Etatsms = etat,
                                        Label_restitutionArticles_nomDuClientsms = com.client.nom + "  " + com.client.prenom
                                    });
                                    break;
                                }

                            }
                        }
                    }

                }


                /*
                                if (listeCommandedate != null)
                                {


                                        foreach (Commande com in listeCommandedate)
                                        {

                                            foreach (Article art in com.listArticles)
                                            {

                                                //Articles rendus de moins d'un mois
                                                if (art.ifRendu == true)
                                                {
                                                    String etat = null;
                                                    if (com.payee == true)
                                                    {
                                                        etat = "Commande payée";
                                                    }
                                                    else
                                                    {
                                                        etat = "Commande non payée";
                                                    }
                                                    ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                                    {
                                                        Label_restitutionArticles_Reference = com.id,
                                                        Label_restitutionArticles_DateCommandebis = com.date.ToString(),
                                                        commande = com,
                                                        Label_restitutionArticles_Etatbis = etat,
                                                        Label_restitutionArticles_nomDuClientbis = com.client.nom + "  " + com.client.prenom
                                                    });
                                                    break;
                                                }

                                            }
                                        }
                                    }
                        */



                if (listeCommandedernier != null)
                {
                    if (listeCommandedernier.Count > 5)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            Commande com = listeCommandedernier[i];
                            foreach (Article art in com.listArticles)
                            {

                                //Articles rendus de moins d'un mois
                                if (art.ifRendu == true)
                                {
                                    String etat = null;
                                    if (com.payee == true)
                                    {
                                        etat = "Commande payée";
                                    }
                                    else
                                    {
                                        etat = "Commande non payée";
                                    }
                                    ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                    {
                                        Label_restitutionArticles_Reference = com.id,
                                        Label_restitutionArticles_DateCommandebis = com.date.ToString(),
                                        commande = com,
                                        Label_restitutionArticles_Etatbis = etat,
                                        Label_restitutionArticles_nomDuClientbis = com.client.nom + "  " + com.client.prenom
                                    });
                                    break;
                                }

                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < listeCommandedernier.Count; i++)
                        {
                            Commande com = listeCommandedernier[i];
                            foreach (Article art in com.listArticles)
                            {

                                //Articles rendus de moins d'un mois
                                if (art.ifRendu == true)
                                {
                                    String etat = null;
                                    if (com.payee == true)
                                    {
                                        etat = "Commande payée";
                                    }
                                    else
                                    {
                                        etat = "Commande non payée";
                                    }
                                    ContentCommandeConcernant.Add(new CommandeConcernantRA_DATA()
                                    {
                                        Label_restitutionArticles_Reference = com.id,
                                        Label_restitutionArticles_DateCommandebis = com.date.ToString(),
                                        commande = com,
                                        Label_restitutionArticles_Etatbis = etat,
                                        Label_restitutionArticles_nomDuClientbis = com.client.nom + "  " + com.client.prenom
                                    });
                                    break;
                                }

                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ce client n'a pas de commande");
                }
            }
        }








        #endregion


        #region Sélection d'une commande
        public DelegateCommand<CommandeConcernantRA_DATA> CommandeParIdFacture
        {
            get
            {
                return this.commandeParIdFacture ?? (this.commandeParIdFacture = new DelegateCommand<CommandeConcernantRA_DATA>(
                   this.ValiderCetteCommande,
                   (arg) => true));
            }
        }
        private void ValiderCetteCommande(CommandeConcernantRA_DATA obj)
        {
            //MessageBox.Show(obj.commande.id +"");
            ClasseGlobale._renduCommande = obj.commande;
            ClasseGlobale.Client = obj.commande.client;
            Label_CommandeSelectionner = ClasseGlobale._renduCommande.id.ToString();
            //MessageBox.Show();
        }
        #endregion

        #endregion



        #region Classe

        public class ChoixBox
        {
            public String NameCbb { get; set; }
            public int cbbId { get; set; }

            public List<ChoixBox> ListeChamp()
            {
                List<ChoixBox> lstCb = new List<ChoixBox>();

                lstCb.Add(new ChoixBox() { cbbId = 1, NameCbb = "Nom" });
                lstCb.Add(new ChoixBox() { cbbId = 2, NameCbb = "Prenom" });
                lstCb.Add(new ChoixBox() { cbbId = 3, NameCbb = "Telephone" });

                return lstCb;
            }
        }



        #endregion



    }
}