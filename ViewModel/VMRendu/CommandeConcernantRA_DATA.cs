﻿using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Model.DTO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_pressing_Loreau.ViewModel
{

    /// <summary>
    /// Listes des commandes d'un client
    /// Cette classe permettra de constituer les articles a afficher par client 
    /// </summary>


    class CommandeConcernantRA_DATA : ObservableObject
    {
        #region Attributs
        private int _label_restitutionArticles_Reference;
        private String _label_restitutionArticles_Name;
        private String _label_restitutionArticles_DateCommande;
        private String _label_restitutionArticles_Etat;

        private int _label_restitutionArticles_Referencebis;
        private String _label_restitutionArticles_Namebis;
        private String _label_restitutionArticles_DateCommandebis;
        private String _label_restitutionArticles_Etatbis;

        private String _label_restitutionArticles_Namesms;
        private String _label_restitutionArticles_DateCommandesms;
        private String _label_restitutionArticles_Etatsms;


        #endregion

        #region Constructeur

        public CommandeConcernantRA_DATA()
        {

        }
        #endregion

        #region Properties and commands
        #region recherche client
        public String ContentButtonClientRA { get; set; }
        public int TagButtonClientRA { get; set; }
        public Client clt;


        public String Label_restitutionArticles_NomClient
        {
            get { return this.clt.nom; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    this.clt.nom = value;
                    NotifyPropertychange("Label_restitutionArticles_NomClient");
                }
            }
        }


        public String Label_restitutionArticles_PrenomClient
        {
            get { return this.clt.prenom; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    this.clt.prenom = value;
                    NotifyPropertychange("Label_restitutionArticles_PrenomClient");
                }
            }
        }

        public String Label_restitutionArticles_Tel
        {
            get { return this.clt.telmob; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    this.clt.telmob = value;
                    NotifyPropertychange("Label_restitutionArticles_Tel");
                }
            }
        }

        public DateTime Label_restitutionArticles_DateNaissance
        {
            get { return this.clt.dateNaissance; }
            set
            {
                this.clt.dateNaissance = value;
                NotifyPropertychange("Label_restitutionArticles_DateNaissance");
            }
        }
        #endregion

        #region Contenu Commande du client

        public Commande commande;
        //private String nom;
        public int Label_restitutionArticles_Reference
        {
            get { return _label_restitutionArticles_Reference; }
            set
            {
                if (value != _label_restitutionArticles_Reference)
                {
                    _label_restitutionArticles_Reference = value;
                    NotifyPropertychange("Label_restitutionArticles_Reference");
                }
            }
        }

        public int Label_restitutionArticles_Referencebis
        {
            get { return _label_restitutionArticles_Referencebis; }
            set
            {
                if (value != _label_restitutionArticles_Referencebis)
                {
                    _label_restitutionArticles_Referencebis = value;
                    NotifyPropertychange("Label_restitutionArticles_Referencebis");
                }
            }
        }

        public String Label_restitutionArticles_Etat
        {
            get { return _label_restitutionArticles_Etat; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_Etat = value;
                    NotifyPropertychange("Label_restitutionArticles_Etat");
                }
            }
        }

        public String Label_restitutionArticles_Etatbis
        {
            get { return _label_restitutionArticles_Etatbis; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_Etatbis = value;
                    NotifyPropertychange("Label_restitutionArticles_Etatbis");
                }
            }
        }


        public String Label_restitutionArticles_Etatsms
        {
            get { return _label_restitutionArticles_Etatsms; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_Etatsms = value;
                    NotifyPropertychange("Label_restitutionArticles_Etatsms");
                }
            }
        }

        public String Label_restitutionArticles_nomDuClient
        {
            get { return _label_restitutionArticles_Name; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_Name = value;
                    NotifyPropertychange("Label_restitutionArticles_nomDuClient");
                }
            }
        }

        public String Label_restitutionArticles_nomDuClientbis
        {
            get { return _label_restitutionArticles_Namebis; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_Namebis = value;
                    NotifyPropertychange("Label_restitutionArticles_nomDuClientbis");
                }
            }
        }


        public String Label_restitutionArticles_nomDuClientsms
        {
            get { return _label_restitutionArticles_Namesms; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_Namesms = value;
                    NotifyPropertychange("Label_restitutionArticles_nomDuClientsms");
                }
            }
        }

        public String Label_restitutionArticles_DateCommande
        {
            get { return _label_restitutionArticles_DateCommande; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_DateCommande = value;
                    NotifyPropertychange("Label_restitutionArticles_DateCommande");
                }
            }
        }

        public String Label_restitutionArticles_DateCommandebis
        {
            get { return _label_restitutionArticles_DateCommandebis; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_DateCommandebis = value;
                    NotifyPropertychange("Label_restitutionArticles_DateCommandebis");
                }
            }
        }


        public String Label_restitutionArticles_DateCommandesms
        {
            get { return _label_restitutionArticles_DateCommandesms; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_restitutionArticles_DateCommandesms = value;
                    NotifyPropertychange("Label_restitutionArticles_DateCommandesms");
                }
            }
        }


        #endregion

        #endregion
    }
}
