﻿using App_pressing_Loreau.ViewModel;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App_pressing_Loreau.Model;
using System.Windows.Input;
using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Practices.Prism.Commands;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace App_pressing_Loreau.ViewModel
{
    public class Date_Pressing_BlanchisserieVM : ObservableObject
    {
        public static DateTime _pressing;

        public DateTime pressing
        {
            get
            {
                return _pressing;
            }
            set
            {
                _pressing = value;
                RaisePropertyChanged("pressing");
            }
        }


        public static DateTime _blanchisserie;

        public DateTime blanchisserie
        {
            get
            {
                return _blanchisserie;
            }
            set
            {
                _blanchisserie = value;
                RaisePropertyChanged("blanchisserie");
            }
        }

        public Date_Pressing_BlanchisserieVM()
        {
        }



        public static class ConfigClass
            {
                public static int MyProperty { get; set; }
                public static DateTime blanblan {get;set;}
                public static DateTime prepre { get; set; }
            }


    }
}
