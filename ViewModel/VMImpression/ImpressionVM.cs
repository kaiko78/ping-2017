﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows.Input;
using App_pressing_Loreau.Model;
using System.Windows;
using System.Windows.Forms;

using System.Collections.ObjectModel;

using App_pressing_Loreau.View;
using Microsoft.Practices.Prism.Commands;

namespace App_pressing_Loreau.ViewModel
{

    /// <summary>
    /// Classe impression
    /// @param: Pour la méthode: 0: lecture x et 1:lecture Z
    /// Impression d un ticket lecture X en cours de journée et d un ticket lecture z pour la fin de journée
    /// </summary>
    class ImpressionVM : ObservableObject, IPageViewModel
    {

        #region Properties and commands
        public String Name
        {
            get { return ""; }
        }

        public ICommand Btn_impression_imprimerTicketZ
        {
            get { return new RelayCommand(p => printTicketZ(1)); }
        }

        public ICommand Btn_impression_imprimerTicketX
        {
            get { return new RelayCommand(p => printTicketZ(0)); }
        }

        private string _txt_Imprimerfacture;


        public String Txt_Imprimerfacture
        {

            get { return _txt_Imprimerfacture; }
            set
            {

                if (value != _txt_Imprimerfacture)
                {
                    _txt_Imprimerfacture = value;
                    NotifyPropertychange("Txt_Imprimerfacture");
                }
            }
        }

        #endregion

        #region Methods
        private void printTicketZ(int var)
        {
            try
            {
                LectureExcel le = new LectureExcel(var);
                le.printLecture();
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Error" + e);
            }

        }
        #endregion
 
        

            #region Attributes
            private int _txb_factures_idCommande;
            private FactureFinaleVM _apercu_facture;

            Commande commande;
            #endregion


           


            #region Properties and commands
            public int Txb_factures_idCommande
            {
                get
                {
                    return _txb_factures_idCommande;
                }
                set
                {
                    if (_txb_factures_idCommande != value)
                    {
                        this._txb_factures_idCommande = value;
                        NotifyPropertychange("Txb_factures_idCommande");
                    }
                }
            }

            public FactureFinaleVM ApercuFacture
            {
                get
                {
                    return _apercu_facture;
                }
                set
                {
                    if (_apercu_facture != value)
                    {
                        this._apercu_facture = value;
                        NotifyPropertychange("ApercuFacture");
                    }
                }
            }


            public ICommand Btn_Factures_Recherche
            {
                get
                {
                    return new RelayCommand(
                        p => FactureApercu(),
                        p => Txb_factures_idCommande > 0);
                }
            }


            public ICommand Btn_factures_imprimer_facture
            {
                get
                {
                    return new RelayCommand(
                        p => impression());
                }
            }
            #endregion



            #region methods

            public void FactureApercu()
            {
                FactureFinaleVM ffVM = new FactureFinaleVM();
                float prixTTCTotal = 0;
                float prixHTTotal = 0;


                commande = (Commande)CommandeDAO.selectCommandeById(Txb_factures_idCommande, false, true, true);
                if (commande != null)
                {
                    //decimal tamponTTC = 0;
                    //decimal tamponHT = 0;
                    //foreach (Article art in commande.listArticles)
                    //{
                    //    //prixTTCTotal += art.TTC;
                    //    tamponTTC += (decimal)art.TTC;
                    //    tamponHT = tamponTTC * ((decimal)(1 - art.TVA / 100));
                    //}
                    //prixTTCTotal = (float)tamponTTC;
                    //prixHTTotal = (float)tamponHT;
                    ////prixHTTotal = prixTTCTotal * (1 - commande.listArticles[0].TVA / 100);

                    ////Arrondi
                    //prixHTTotal = (float)Math.Round(prixHTTotal, 2, MidpointRounding.AwayFromZero);
                    //prixTTCTotal = (float)Math.Round(prixTTCTotal, 2, MidpointRounding.AwayFromZero);
                    //prixHTTotal = (float)Math.Round(prixHTTotal, 2, MidpointRounding.AwayFromZero);

                    //ffVM.commande = commande;
                    //ffVM.LabelDetailPrixTotalTTC = prixTTCTotal;
                    //ffVM.LabelDetailMontantHT = prixHTTotal;
                    //ffVM.LabelDetailMontantTVA = (float)((decimal)prixTTCTotal - (decimal)prixHTTotal);
                    //ffVM.RemplirArticles(commande);

                    System.Windows.MessageBox.Show("Ref :" + commande.id + "\n Date recu :" + commande.date + "\n Nombre d'articles :" +
                        commande.listArticles.Count);

                }
                else
                {
                    System.Windows.MessageBox.Show("Commande non trouvée");
                }


                ApercuFacture = ffVM;

            }

            public void impression()
            {
                if (string.IsNullOrEmpty(Txt_Imprimerfacture))
                {
                    System.Windows.MessageBox.Show("Veuillez entrer un id de facture.");
                }
                else
                {
                    int idcommande = Int32.Parse(Txt_Imprimerfacture);
                    
                    int idmax = CommandeDAO.selectMaxCommande();

                    if (idcommande > idmax)
                    {
                        System.Windows.MessageBox.Show("Cette commande n'existe pas.");
                    }
                    else
                    {
                        commande = CommandeDAO.selectCommandeById(idcommande, false, true, true);
                        if (commande != null)
                        {
                            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Voulez vous imprimer cette facture?", "Impression", MessageBoxButtons.YesNo);
                            if (dialogResult == DialogResult.Yes)
                            {
                                //Imprimer
                                FactureExcel fe = new FactureExcel(commande);
                                fe.printFacture();
                            }
                            else if (dialogResult == DialogResult.No)
                            {
                                //Ne rien faire
                            }

                        }
                        else
                            System.Windows.MessageBox.Show("La commande est null");
                    }
                    
                }
                
                 
            }

            #endregion


        


    }
}
