﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows;
using Microsoft.Practices.Prism.Commands;

namespace App_pressing_Loreau.ViewModel
{

    /// <summary>  Classe Statistique
    /// Classe statistique
    /// *Chiffre d affaires total par jour, semaine , mois et année
    /// *Chiffre d'affaires par departement et par jour, semaine , mois et année
    /// *Nombre de clients ayant deposé des articles par jour, semaine , mois et année
    /// *Nombre de clients ayant recupéré des articles par  jour, semaine , mois et année
    /// *Nombre de clients ayant payé immediatement par jour, semaine , mois et année
    /// *Nombre de client ayant payé en differé par jour, semaine , mois et année
    /// *Nombre d'articles dans le pressing par jour, semaine , mois et année
    /// *Nombre d'articles dans la blanchisserie par jour, semaine , mois et année
    /// *Nombre de couettes par jour, semaine , mois et année
    /// *Nombre de chemises par jour, semaine , mois et année
    /// </summary>
    class StatistiquesVM : ObservableObject
    {
        #region Attributs
        private float _label_statistique_catotal;
        private float _label_statistique_cadep;
        private float _label_statistique_nbrClientsDepoArt;
        private float _label_statistique_nbrClientsRecupArt;
        private float _label_statistique_nbrClientspayeimediatement;

        private float _label_statistique_nbrClientspayediffere;
        private float _label_statistique_nbrArticlesPressing;
        private float _label_statistique_nbrArticlesBlanchisserie;
        private float _label_statistique_nbrCouettes;
        private float _label_statistique_nbrChemises;

        private String _label_Stat_Temps;

        private ComboDepartStat _selected_stat_ChoixDepart;
        ComboDepartStat comboDepartStat = new ComboDepartStat();

        private ComboEmployeStat _selected_stat_ChoixEmploye;
        ComboEmployeStat comboEmployeStat = new ComboEmployeStat();
        //chiffre d'affaire par département
        private List<Departement> listUsedDepartements = new List<Departement>();

        private List<Employe> listUsedEmploye = new List<Employe>();
        private List<float> caTTCDep = new List<float>();

        //aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa







        private DateTime _choix_stat_1;

        public DateTime choix_stat_1
        {
            get
            {
                return _choix_stat_1;
            }
            set
            {
                _choix_stat_1 = value;
                RaisePropertyChanged("choix_stat_1");
            }
        }

        public static DateTime _choix_stat_2;

        public DateTime choix_stat_2
        {
            get
            {
                return _choix_stat_2;
            }
            set
            {
                _choix_stat_2 = value;
                RaisePropertyChanged("choix_stat_2");
            }
        }











        //aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa




        //private DelegateCommand<StatistiquesVM> _btn_statistique_du_jour;
        #endregion

        #region Constructeur
        public StatistiquesVM()
        {

            Label_statistique_cadep = new float();
            Label_statistique_catotal = new float();
            Label_statistique_nbrArticlesBlanchisserie = new float();
            Label_statistique_nbrArticlesPressing = new float();
            Label_statistique_nbrChemises = new float();
            Label_statistique_nbrCouettes = new float();
            Label_statistique_nbrClientsDepoArt = new float();
            Label_statistique_nbrClientsRecupArt = new float();

            ListeDepartementStatChoix = comboDepartStat.ListeDep();
            ListeEmployeStatChoix = comboEmployeStat.ListeEmploye();

            Label_Stat_Temps = "";

        }

        #endregion

        #region Properties and command

        public List<ComboDepartStat> ListeDepartementStatChoix { get; set; }
        public ComboDepartStat Selected_stat_ChoixDepart
        {
            get { return _selected_stat_ChoixDepart; }
            set
            {
                if (value != _selected_stat_ChoixDepart)
                {
                    _selected_stat_ChoixDepart = value;
                    NotifyPropertychange("Selected_stat_ChoixDepart");
                }
            }
        }


        public List<ComboEmployeStat> ListeEmployeStatChoix { get; set; }
        public ComboEmployeStat Selected_stat_ChoixEmploye
        {
            get { return _selected_stat_ChoixEmploye; }
            set
            {
                if (value != _selected_stat_ChoixEmploye)
                {
                    _selected_stat_ChoixEmploye = value;
                    NotifyPropertychange("Selected_stat_ChoixEmploye");
                }
            }
        }


        public string Label_Stat_Temps
        {
            get { return _label_Stat_Temps; }
            set
            {
                if (value != _label_Stat_Temps)
                {
                    _label_Stat_Temps = value;
                    NotifyPropertychange("Label_Stat_Temps");
                }
            }
        }
        public float Label_statistique_catotal
        {
            get { return _label_statistique_catotal; }
            set
            {
                if (value != _label_statistique_catotal)
                {
                    _label_statistique_catotal = value;
                    NotifyPropertychange("Label_statistique_catotal");
                }
            }
        }
        public float Label_statistique_cadep
        {
            get { return _label_statistique_cadep; }
            set
            {
                if (value != _label_statistique_cadep)
                {
                    _label_statistique_cadep = value;
                    RaisePropertyChanged("Label_statistique_cadep");
                }
            }
        }
        public float Label_statistique_nbrClientsDepoArt
        {
            get { return _label_statistique_nbrClientsDepoArt; }
            set
            {
                if (value != _label_statistique_nbrClientsDepoArt)
                {
                    _label_statistique_nbrClientsDepoArt = value;
                    RaisePropertyChanged("Label_statistique_nbrClientsDepoArt");
                }
            }
        }
        public float Label_statistique_nbrClientsRecupArt
        {
            get { return _label_statistique_nbrClientsRecupArt; }
            set
            {
                if (value != _label_statistique_nbrClientsRecupArt)
                {
                    _label_statistique_nbrClientsRecupArt = value;
                    RaisePropertyChanged("Label_statistique_nbrClientsRecupArt");
                }
            }
        }
        public float Label_statistique_nbrClientspayeimediatement
        {
            get { return _label_statistique_nbrClientspayeimediatement; }
            set
            {
                if (value != _label_statistique_nbrClientspayeimediatement)
                {
                    _label_statistique_nbrClientspayeimediatement = value;
                    RaisePropertyChanged("Label_statistique_nbrClientspayeimediatement");
                }
            }
        }
        public float Label_statistique_nbrClientspayediffere
        {
            get { return _label_statistique_nbrClientspayediffere; }
            set
            {
                if (value != _label_statistique_nbrClientspayediffere)
                {
                    _label_statistique_nbrClientspayediffere = value;
                    RaisePropertyChanged("Label_statistique_nbrClientspayediffere");
                }
            }
        }
        public float Label_statistique_nbrArticlesPressing
        {
            get { return _label_statistique_nbrArticlesPressing; }
            set
            {
                if (value != _label_statistique_nbrArticlesPressing)
                {
                    _label_statistique_nbrArticlesPressing = value;
                    RaisePropertyChanged("Label_statistique_nbrArticlesPressing");
                }
            }
        }
        public float Label_statistique_nbrArticlesBlanchisserie
        {
            get { return _label_statistique_nbrArticlesBlanchisserie; }
            set
            {
                if (value != _label_statistique_nbrArticlesBlanchisserie)
                {
                    _label_statistique_nbrArticlesBlanchisserie = value;
                    RaisePropertyChanged("Label_statistique_nbrArticlesBlanchisserie");
                }
            }
        }
        public float Label_statistique_nbrCouettes
        {
            get { return _label_statistique_nbrCouettes; }
            set
            {
                if (value != _label_statistique_nbrCouettes)
                {
                    _label_statistique_nbrCouettes = value;
                    RaisePropertyChanged("Label_statistique_nbrCouettes");
                }
            }
        }
        public float Label_statistique_nbrChemises
        {
            get { return _label_statistique_nbrChemises; }
            set
            {
                if (value != _label_statistique_nbrChemises)
                {
                    _label_statistique_nbrChemises = value;
                    RaisePropertyChanged("Label_statistique_nbrChemises");
                }
            }
        }
        public ICommand Btn_statistique_du_jour
        {
            get
            {
                return new RelayCommand(p => statisticsByDate(1));
            }
        }
        public ICommand Btn_statistique_de_la_semaine
        {
            get
            {
                return new RelayCommand(p => statisticsByDate(2));
            }
        }
        public ICommand Btn_statistique_du_mois
        {
            get
            {
                return new RelayCommand(p => statisticsByDate(3));
            }
        }
        public ICommand Btn_statistique_de_lannee
        {
            get
            {
                return new RelayCommand(p => statisticsByDate(4));
            }
        }
        public ICommand Btn_statistique_choix
        {
            get
            {
                return new RelayCommand(p => validerComboEmploye());
                    
            }
        }

        public ICommand Btn_Statistique_validerDep
        {
            get { return new RelayCommand(p => validerComboBox()); }
        }
        #endregion

        #region methods

        /// <summary>
        /// Methodes Non complete. Aurelien, si tu peux faire kelk chose 
        /// </summary>
        public void validerComboBox()
        {
            if (Selected_stat_ChoixDepart != null)
            {
              
                for (int i = 0; i < listUsedDepartements.Count;i++ )
                {
                    if (Selected_stat_ChoixDepart.cbbDepId == listUsedDepartements[i].id)
                    {
                        Label_statistique_cadep = caTTCDep[i];
                    }
                    
                }
            }


        }

        public void validerComboEmploye()
        {
            if (Selected_stat_ChoixEmploye != null)
            {
                for (int i = 0; i < listUsedDepartements.Count; i++)
                {
                  
                        caTTCDep[i] = 0;
                    
                }
                statisticsChoixEmploye();
            }
            else
            {
                statisticsChoix();
            }

        }

        public IDictionary<Departement, float> ChiffreDaffaireByDep = new Dictionary<Departement, float>();

        public void statisticsByDate(int typeDate)
        {
            
            try
            {

                

                switch (typeDate)
                {
                    case 1: Label_Stat_Temps = "Statistique du jour";
                        break;
                    case 2: Label_Stat_Temps = "Statistique de la semaine";
                        break;
                    case 3: Label_Stat_Temps = "Statistique du mois";
                        break;
                    case 4: Label_Stat_Temps = "Statistique de l'année";
                        break;


                }

                float ChiffreAffaireDuJour = 0;
                DepartmentTTC(ArticleDAO.selectArticlePayeeByDateNosolde(typeDate));

                List<Payement> listePaiement = (List<Payement>)PayementDAO.listSommePaiementToday(typeDate);
                foreach (Payement paye in listePaiement)
                {
                    if (!paye.typePaiement.Equals("solde"))
                        ChiffreAffaireDuJour = (float)((decimal)ChiffreAffaireDuJour + (decimal)paye.montant);
                }
                //Chiffre d affaire total
                Label_statistique_catotal = ChiffreAffaireDuJour;
                //Nombre d articles dans la blanchisserie 
                Label_statistique_nbrArticlesBlanchisserie = (float)ArticleDAO.articlesInBlanchisserieByDate(typeDate);
                //Nombre d articles dans le pressing
                Label_statistique_nbrArticlesPressing = (float)ArticleDAO.articlesByDate(typeDate);
                //Nombre de chemises 
                Label_statistique_nbrChemises = (float)ArticleDAO.chemisesByDate(typeDate);
                //Nombre de couette
                Label_statistique_nbrCouettes = (float)ArticleDAO.couetteByDate(typeDate);
                //Nombre de clients ayant deposés des articles
                Label_statistique_nbrClientsDepoArt = (float)ClientDAO.nbClientDepot(typeDate);
                //Nombre de clients ayant recuperés des artilces
                Label_statistique_nbrClientsRecupArt = (float)ClientDAO.nbClientRecup(typeDate);
                //Nombre de clients ayant payé immédiatement

                //Nombre de clients ayant payé en différé
            }
            catch (Exception e)
            {
                MessageBox.Show("Erreur :" + e);
            }
        }

        public void statisticsChoix()
        {
            DateTime d1 = _choix_stat_1;
            DateTime d2 = _choix_stat_2;
            
            try
            {



                Label_Stat_Temps = "";

                float ChiffreAffaireDuJour = 0;
                DepartmentTTC(ArticleDAO.selectArticlePayeeByDateNosoldeChoix(d1, d2));

                List<Payement> listePaiement = (List<Payement>)PayementDAO.listSommePaiementTodayChoix(d1, d2);
                foreach (Payement paye in listePaiement)
                {
                    if (!paye.typePaiement.Equals("solde"))
                        ChiffreAffaireDuJour = (float)((decimal)ChiffreAffaireDuJour + (decimal)paye.montant);
                }
                //Chiffre d affaire total
                Label_statistique_catotal = ChiffreAffaireDuJour;
                //Nombre d articles dans la blanchisserie 
                Label_statistique_nbrArticlesBlanchisserie = (float)ArticleDAO.articlesInBlanchisserieByDateChoix(d1, d2);
                //Nombre d articles dans le pressing
                Label_statistique_nbrArticlesPressing = (float)ArticleDAO.articlesByDateChoix(d1, d2);
                //Nombre de chemises 
                Label_statistique_nbrChemises = (float)ArticleDAO.chemisesByDateChoix(d1, d2);
                //Nombre de couette
                Label_statistique_nbrCouettes = (float)ArticleDAO.couetteByDateChoix(d1, d2);
                //Nombre de clients ayant deposés des articles
                Label_statistique_nbrClientsDepoArt = (float)ClientDAO.nbClientDepotChoix(d1, d2);
                //Nombre de clients ayant recuperés des artilces
                Label_statistique_nbrClientsRecupArt = (float)ClientDAO.nbClientRecupChoix(d1, d2);
                //Nombre de clients ayant payé immédiatement

                //Nombre de clients ayant payé en différé
            }
            catch (Exception e)
            {
                MessageBox.Show("Erreur :" + e);
            }
        }



        public void statisticsChoixEmploye()
        {
            DateTime d1 = _choix_stat_1;
            DateTime d2 = _choix_stat_2;
            int employe_id = Selected_stat_ChoixEmploye.cbbEmployeId;

            try
            {



                Label_Stat_Temps = "";

                float ChiffreAffaireDuJour = 0;
                DepartmentTTC(ArticleDAO.selectArticlePayeeByDateNosoldeChoixEmploye(d1, d2, employe_id));

                List<Payement> listePaiement = (List<Payement>)PayementDAO.listSommePaiementTodayChoixEmploye(d1, d2, employe_id);
                foreach (Payement paye in listePaiement)
                {
                    if (!paye.typePaiement.Equals("solde"))
                        ChiffreAffaireDuJour = (float)((decimal)ChiffreAffaireDuJour + (decimal)paye.montant);
                }
                //Chiffre d affaire total
                Label_statistique_catotal = ChiffreAffaireDuJour;
                //Nombre d articles dans la blanchisserie 
                Label_statistique_nbrArticlesBlanchisserie = (float)ArticleDAO.articlesInBlanchisserieByDateChoixEmploye(d1, d2, employe_id);
                //Nombre d articles dans le pressing
                Label_statistique_nbrArticlesPressing = (float)ArticleDAO.articlesByDateChoixEmploye(d1, d2, employe_id);
                //Nombre de chemises 
                Label_statistique_nbrChemises = (float)ArticleDAO.chemisesByDateChoixEmploye(d1, d2, employe_id);
                //Nombre de couette
                Label_statistique_nbrCouettes = (float)ArticleDAO.couetteByDateChoixEmploye(d1, d2, employe_id);
                //Nombre de clients ayant deposés des articles
                Label_statistique_nbrClientsDepoArt = (float)ClientDAO.nbClientDepotChoixEmploye(d1, d2, employe_id);
                //Nombre de clients ayant recuperés des artilces
                Label_statistique_nbrClientsRecupArt = (float)ClientDAO.nbClientRecupChoixEmploye(d1, d2, employe_id);
                //Nombre de clients ayant payé immédiatement

                //Nombre de clients ayant payé en différé
            }
            catch (Exception e)
            {
                MessageBox.Show("Erreur :" + e);
            }
        }




        #endregion

        #region methodes de calcul

        public void DepartmentTTC(List<Article> articlesrendu)
        {
            Boolean ifExist;

            try
            {


                foreach (Article art in articlesrendu)
                {
                    //Vérifie que l'article n'a pas été payé en solde
                    if (!CommandeDAO.isPayedBysolde(art.fk_commande))
                    {
                        ifExist = false;
                        //Jusque là on a déroulé tout les articles
                        //recherche de départements déja entrés
                        for (int i = 0; i < listUsedDepartements.Count; i++)
                        {
                            if (listUsedDepartements[i].nom.Contains(art.type.departement.nom))
                            {
                                caTTCDep[i] =  (float)((decimal)caTTCDep[i] + (decimal) art.TTC);
                                ifExist = true;
                                break;
                            }
                        }

                        if (!ifExist)
                        {
                            listUsedDepartements.Add(art.type.departement);
                            caTTCDep.Add(art.TTC);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }

        #endregion
    }


    #region Class

    public class ComboDepartStat
    {
        public String NameDepartStat { get; set; }
        public int cbbDepId { get; set; }

        List<Departement> depart = (List<Departement>)DepartementDAO.selectDepartements();
        public List<ComboDepartStat> ListeDep()
        {
            List<ComboDepartStat> listDep = new List<ComboDepartStat>();

            foreach (Departement cc in depart)
            {
                listDep.Add(new ComboDepartStat() { NameDepartStat = cc.nom, cbbDepId = cc.id });
            }

            return listDep;
        }
    }

    public class ComboEmployeStat
    {
        public String NameEmployeStat { get; set; }
        public int cbbEmployeId { get; set; }

        List<Employe> employe = (List<Employe>)EmployeDAO.selectEmployes();
        public List<ComboEmployeStat> ListeEmploye()
        {
            List<ComboEmployeStat> listEmploye = new List<ComboEmployeStat>();

            foreach (Employe cc in employe)
            {
                listEmploye.Add(new ComboEmployeStat() { NameEmployeStat = cc.prenom, cbbEmployeId = cc.id });
            }

            return listEmploye;
        }
    }
    #endregion
}
