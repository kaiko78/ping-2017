﻿using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Model;
using App_pressing_Loreau.Model.DTO;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;


namespace App_pressing_Loreau.ViewModel
{
    class AdministrationClientVM : ObservableObject
    {
        #region Attributs

        private ComboTheme _selected_administrationClient_choix_theme;

        private DelegateCommand<AdministrationClientVM> _modifierClient;

        public List<RechercheClient> _listeRechercheClient;

        Client choixClient = new Client();
        Client clientModif = new Client();
        public List<ComboTheme> Cbb_administrationClient_choix_theme { get; set; }

        ComboTheme comboTheme = new ComboTheme();
        private String _txb_administrationClient_choix;
        private String _label_adminIdentClient_choix;

        //Modifier client
        private string _txb_adminClient_modifNumAdresse;
        private String _txb_adminClient_modifNameAdresse;
        private String _txb_adminClient_modifBP;
        private String _txb_adminClient_modifVille;

        private String _txb_adminClient_modifTypeTelephone;
        private String _txb_adminClient_modifTypeNom;
        private String _txb_adminClient_modifTypePrenom;
        private float _txb_adminClient_modifTypesolde;

        #endregion

        #region constructeur
        public AdministrationClientVM()
        {
            Cbb_AdministrationClient_choix_theme = comboTheme.ListeChamp();
            Txb_adminClient_modifTypesolde = new float();
        }
        #endregion

        #region Properties and commands

        //Modification client , parametres

        #region modifier client

        public string Txb_adminClient_modifNumAdresse
        {
            get { return _txb_adminClient_modifNumAdresse; }
            set
            {
                if (value != _txb_adminClient_modifNumAdresse)
                {
                    _txb_adminClient_modifNumAdresse = value;
                    NotifyPropertychange("Txb_adminClient_modifNumAdresse");
                }
            }

        }
        public String Txb_adminClient_modifNameAdresse
        {
            get { return _txb_adminClient_modifNameAdresse; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _txb_adminClient_modifNameAdresse = value;
                    NotifyPropertychange("Txb_adminClient_modifNameAdresse");
                }
            }
        }
        public String Txb_adminClient_modifBP
        {
            get { return _txb_adminClient_modifBP; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _txb_adminClient_modifBP = value;
                    NotifyPropertychange("Txb_adminClient_modifBP");
                }
            }
        }

        public String Txb_adminClient_modifVille
        {
            get { return _txb_adminClient_modifVille; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _txb_adminClient_modifVille = value;
                    NotifyPropertychange("Txb_adminClient_modifVille");
                }
            }
        }
        public String Txb_adminClient_modifTypeTelephone
        {
            get { return _txb_adminClient_modifTypeTelephone; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _txb_adminClient_modifTypeTelephone = value;
                    NotifyPropertychange("Txb_adminClient_modifTypeTelephone");
                }
            }
        }
        public String Txb_adminClient_modifTypeNom
        {
            get { return _txb_adminClient_modifTypeNom; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _txb_adminClient_modifTypeNom = value;
                    NotifyPropertychange("Txb_adminClient_modifTypeNom");
                }
            }
        }
        public String Txb_adminClient_modifTypePrenom
        {
            get { return _txb_adminClient_modifTypePrenom; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _txb_adminClient_modifTypePrenom = value;
                    NotifyPropertychange("Txb_adminClient_modifTypePrenom");
                }
            }
        }
        public float Txb_adminClient_modifTypesolde
        {
            get { return _txb_adminClient_modifTypesolde; }
            set
            {
                if (value != _txb_adminClient_modifTypesolde)
                {
                    _txb_adminClient_modifTypesolde = value;
                    NotifyPropertychange("Txb_adminClient_modifTypesolde");
                }
            }
        }

        public ICommand Btn_adminArt_ModifTypeArt
        {
            get { return new RelayCommand(p => EnregistrerModifClient()); }
        }
        #endregion

        public List<ComboTheme> Cbb_AdministrationClient_choix_theme { get; set; }

        public ComboTheme Selected_administrationClient_choix_theme
        {
            get { return _selected_administrationClient_choix_theme; }
            set
            {
                _selected_administrationClient_choix_theme = value;
                NotifyPropertychange("Selected_administrationClient_choix_theme");
            }
        }

        public String Txb_administrationClient_choix
        {
            get { return _txb_administrationClient_choix; }
            set
            {
                _txb_administrationClient_choix = value;
                NotifyPropertychange("Txb_administrationClient_choix");
            }
        }

        public String Label_adminIdentClient_choix
        {
            get { return _label_adminIdentClient_choix; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    _label_adminIdentClient_choix = value;
                    NotifyPropertychange("Label_adminIdentClient_choix");
                }
            }
        }
        public List<RechercheClient> ListeRechercheClient
        {
            get { return _listeRechercheClient; }
            set
            {
                _listeRechercheClient = value;
                NotifyPropertychange("ListeRechercheClient");
            }
        }

        public ICommand Btn_administrationClient_valider
        {
            get { return new RelayCommand(c => getChoix(), p => Txb_administrationClient_choix!=null); }
        }

        private DelegateCommand<RechercheClient> getButtonRecherche;
        public DelegateCommand<RechercheClient> GetButtonRecherche
        {
            get { return this.getButtonRecherche??(getButtonRecherche=new DelegateCommand<RechercheClient>(
                this.ExecuteGetClient,
                (arg)=>true)); }
        }

        public DelegateCommand<AdministrationClientVM> ModifierClient
        {
            get
            {
                return this._modifierClient ?? (this._modifierClient = new DelegateCommand<AdministrationClientVM>(
                    this.ExecuteModifClient,
                    (arg) => true));
            }
        }
        #endregion

       
        #region methods

        private void EnregistrerModifClient()
        {

            clientModif = ClientDAO.selectClientById(this.choixClient.id,false, false,false);

            Adresse ad = new Adresse();
            ad.codePostal = Txb_adminClient_modifBP;
            ad.numero = Txb_adminClient_modifNumAdresse;
            ad.rue = Txb_adminClient_modifNameAdresse;
            ad.ville = Txb_adminClient_modifVille;

            clientModif.adresse = ad;
            clientModif.telmob = Txb_adminClient_modifTypeTelephone;
            clientModif.nom = Txb_adminClient_modifTypeNom;
            clientModif.prenom = Txb_adminClient_modifTypePrenom;
            clientModif.solde = Txb_adminClient_modifTypesolde;

            int x = ClientDAO.updateClient(clientModif);
            

            if (x != 0)
            {
                initFields();
                MessageBox.Show("Modification du client \n de " + clientModif.nom + " " + clientModif.prenom + " effectué");
                
            }
           
        }



        private void initFields()
        {
            Txb_adminClient_modifNumAdresse = null;
            Txb_adminClient_modifNameAdresse = null;
            Txb_adminClient_modifBP = null;
            Txb_adminClient_modifVille = null;

            Txb_adminClient_modifTypeTelephone = null;
            Txb_adminClient_modifTypesolde = 0;

        }
        private void ExecuteModifClient(AdministrationClientVM obj)
        {
           
                Txb_adminClient_modifNumAdresse = obj.choixClient.adresse.numero;
                Txb_adminClient_modifNameAdresse = obj.choixClient.adresse.rue;
                Txb_adminClient_modifVille = obj.choixClient.adresse.ville;
                Txb_adminClient_modifBP = obj.choixClient.adresse.codePostal;

                Txb_adminClient_modifTypeTelephone = obj.choixClient.telmob;
                Txb_adminClient_modifTypeNom = obj.choixClient.nom;
                Txb_adminClient_modifTypePrenom = obj.choixClient.prenom;
                Txb_adminClient_modifTypesolde = obj.choixClient.solde;
         

        }


        private void ExecuteGetClient(RechercheClient obj)
        {
            if (obj.client != null)
            {
                choixClient = obj.client;
                Label_adminIdentClient_choix = "Choix = " + choixClient.nom + " " + choixClient.prenom;
            }

        }

        public void getChoix()
        {
            ListeRechercheClient = new List<RechercheClient>();
            List<Client> resultat = null;
            if (Selected_administrationClient_choix_theme != null)
            {


                if (Selected_administrationClient_choix_theme.NameCbb.Equals("Nom"))
                {
                    resultat = ClientDAO.seekClients(Txb_administrationClient_choix, null, null, 0);
                }
                else if (Selected_administrationClient_choix_theme.NameCbb.Equals("Prenom"))
                {
                    resultat = ClientDAO.seekClients(null, Txb_administrationClient_choix, null, 0);
                }
                else resultat = null;

                if (resultat != null)
                {
                    if (resultat.Count == 2)
                    {
                        ListeRechercheClient.Add(new RechercheClient()
                        {
                            client = resultat[0]
                        });
                        ListeRechercheClient.Add(new RechercheClient()
                        {
                            client = resultat[1]
                        });
                        ListeRechercheClient.Add(new RechercheClient()
                        {
                            client = resultat[1]
                        });
                    }
                    else
                    {
                        foreach (Client clt in resultat)
                        {
                            ListeRechercheClient.Add(new RechercheClient()
                            {
                                client = clt
                            });
                        }
                    }
                    
                }
                else
                {
                    MessageBox.Show("Pas de resultat ");
                }
            }
            else
            {
                MessageBox.Show("Choisissez un élement ");
            }
        }
        #endregion
    }

    #region Class

    public class ComboTheme
    {

        public int cbID { get; set; }
        public String NameCbb { get; set; }

        public List<ComboTheme> ListeChamp()
        {
            List<ComboTheme> lstCb = new List<ComboTheme>();

            lstCb.Add(new ComboTheme() { cbID = 1, NameCbb = "Nom" });
            lstCb.Add(new ComboTheme() { cbID = 2, NameCbb = "Prenom" });

            return lstCb;
        }
    }

    class RechercheClient : ObservableObject
    {
        public Client client = new Client();

       // public int TagButtonClientRA { get; set; }
        public String Label_administrationClient_NomClient
        {
            get { return client.nom; }
            set { client.nom = value;
            NotifyPropertychange("Label_administrationClient_NomClient");
            }
        }

        public String Label_administrationClient_PrenomClient
        {
            get { return client.prenom; }
            set
            {
                client.prenom = value;
                NotifyPropertychange("Label_administrationClient_PrenomClient");
            }
        }
    }
    #endregion
}
