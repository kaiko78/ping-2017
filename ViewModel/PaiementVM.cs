﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using App_pressing_Loreau.Model;
using App_pressing_Loreau.View;
using Microsoft.Practices.Prism.Commands;

//using System.Windows.Controls.Button;
//using System.Windows.Forms;

namespace App_pressing_Loreau.ViewModel
{
    class PaiementVM : ObservableObject
    {
        #region Attributs
        Client clientModif = new Client();
        //public static int commandePayee = 0;
        private String _label_paiement_prixHT;
        private String _label_paiement_prixTTC;
        private String _label_paiement_client;
        private String _label_paiement_tel;
        private float _label_paiement_solde;
        private float _txb_paiement_montantRemise;
        private float[] checkRemise;
        private String _label_paiement_montant;//Prix ttc - remise

        //Correspond à la valeur du testbox prix à payer (pour 1 et seulement 1 moyen de paiement)
        private float _txb_paiement_montantParMoyenPaiement;

        //Prends en param : le moyen de paiement et le montant correspondant à ce moyen de paiement

        private ListePaiement listeDeMontantParMoyenPaiement = new ListePaiement();
        private String _mode_de_paiement;

        private List<GetPaiement> _itemsMontantParMoyenPaiement;

        //private ObservableCollection<PaiementListeVM> _contenuListePaiement;

        private float _reste_a_payer;


        private DelegateCommand<PaiementListeVM> _deletePaiement;

        //private bool erreurDeManip;
        //Payement paiement;
        #endregion

        #region constructeur

        public PaiementVM()
        {
            lePaiement();
            _txb_paiement_montantParMoyenPaiement = new float();
            Label_paiement_solde = ClasseGlobale.Client.solde;
            Label_paiement_tel = ClasseGlobale.Client.telmob;
            Label_paiement_client = ClasseGlobale.Client.nom + " " + ClasseGlobale.Client.prenom;
            _itemsMontantParMoyenPaiement = new List<GetPaiement>();

            //Txb_paiement_montantParMoyenPaiement = Label_NouvelleCommande_prixTotal;
            Txb_paiement_montantParMoyenPaiement = float.Parse(Label_paiement_montant.Split(' ')[0]);
            Reste_a_payer = Txb_paiement_montantParMoyenPaiement;
            ClasseGlobale.reste_a_payer = Reste_a_payer;
            _mode_de_paiement = "";
            ClasseGlobale.initializeContenuListePaiement();
            checkRemise = new float[2] { 0F, 0F };

        }

        #endregion

        #region Accesseurs et mutateurs

        //public String Reste_a_payer_String
        //{
        //    get { return _reste_a_payer.ToString(); }
        //    set
        //    {
        //        _reste_a_payer = float.Parse(value);
        //        OnPropertyChanged("Reste_a_payer_String");
        //    }
        //}


        //public List<GetPaiement> ItemsMontantParMoyenPaiement
        //{
        //    get { return _itemsMontantParMoyenPaiement; }
        //    set
        //    {
        //        _itemsMontantParMoyenPaiement = value;
        //        OnPropertyChanged("ItemsMontantParMoyenPaiement");
        //    }
        //}



        public String Label_paiement_prixHT
        {
            get { return _label_paiement_prixHT; }
            set
            {
                if (value != _label_paiement_prixHT)
                {
                    _label_paiement_prixHT = value;
                    NotifyPropertychange("Label_paiement_prixHT");
                }
            }
        }

        public String Label_paiement_client
        {
            get { return _label_paiement_client; }
            set
            {
                if (value != _label_paiement_client)
                {
                    _label_paiement_client = value;
                    NotifyPropertychange("Label_paiement_client");
                }
            }
        }

        public String Label_paiement_tel
        {
            get { return _label_paiement_tel; }
            set
            {
                if (value != _label_paiement_tel)
                {
                    _label_paiement_tel = value;
                    NotifyPropertychange("Label_paiement_tel");
                }
            }
        }

        public String Mode_de_paiement
        {
            get { return _mode_de_paiement; }
            set
            {
                if (value != _mode_de_paiement)
                {
                    _mode_de_paiement = value;
                    NotifyPropertychange("Mode_de_paiement");
                }
            }
        }

        public float Reste_a_payer
        {
            get { return _reste_a_payer; }
            set
            {
                if (value != _reste_a_payer)
                {
                    _reste_a_payer = value;
                    NotifyPropertychange("Reste_a_payer");
                }
            }
        }

        public String Label_paiement_prixTTC
        {
            get { return _label_paiement_prixTTC; }
            set
            {
                if (value != _label_paiement_prixTTC)
                {
                    _label_paiement_prixTTC = value;
                    NotifyPropertychange("Label_paiement_prixTTC");
                }
            }
        }

        public float Label_paiement_solde
        {
            get { return _label_paiement_solde; }
            set
            {
                if (value != _label_paiement_solde)
                {
                    _label_paiement_solde = value;
                    NotifyPropertychange("Label_paiement_solde");
                }
            }
        }

        public String Label_paiement_montant
        {
            get { return _label_paiement_montant; }
            set
            {
                if (value != _label_paiement_montant)
                {
                    _label_paiement_montant = value;
                    NotifyPropertychange("Label_paiement_montant");
                }
            }
        }



        public float Txb_paiement_montantRemise
        {
            get { return _txb_paiement_montantRemise; }
            set
            {
                if (value != _txb_paiement_montantRemise)
                {
                    _txb_paiement_montantRemise = value;
                    NotifyPropertychange("Txb_paiement_montantRemise");
                    //OnPropertyChanged("Txb_paiement_montantParMoyenPaiement");
                }
            }
        }

        public float Txb_paiement_montantParMoyenPaiement
        {
            get { return _txb_paiement_montantParMoyenPaiement; }
            set
            {
                if (value != _txb_paiement_montantParMoyenPaiement)
                {
                    _txb_paiement_montantParMoyenPaiement = value;
                    NotifyPropertychange("Txb_paiement_montantParMoyenPaiement");
                }
            }
        }

        //public ListePaiement MontantParMoyenPaiement
        //{
        // get { return listeDeMontantParMoyenPaiement; }
        // set
        // {
        // if (value != listeDeMontantParMoyenPaiement)
        // {
        // listeDeMontantParMoyenPaiement = value;
        // OnPropertyChanged("MontantParMoyenPaiement");
        // }
        // }
        //}


        #endregion

        #region Commandes

        #region Bouton valider commande
        public ICommand Btn_valider_paiement_commande
        {
            get
            {
                return new RelayCommand(
                    p => CartePrepayee(),
                    p => ClasseGlobale.Client != null
                    );
            }
        }


        public ICommand Btn_paiement_tel
        {
            get { return new RelayCommand(p => EnregistrerModifClient()); }
        }

        #endregion


        private void EnregistrerModifClient()
        {

            clientModif = ClasseGlobale.Client;



            clientModif.telmob = Label_paiement_tel;
            clientModif.nom = ClasseGlobale.Client.nom;
            clientModif.prenom = ClasseGlobale.Client.prenom;


            int x = ClientDAO.updateClient(clientModif);


            if (x != 0)
            {

                MessageBox.Show("Modification du client \n de " + clientModif.nom + " " + clientModif.prenom + " effectué");

            }

        }





        #region Bouton mode de paiement

        //Gère les boutons de mode de paiement
        ICommand btn_paiement;
        public ICommand Btn_paiement
        {
            get
            {
                //if (ClasseGlobale.Client.type == 1)
                return btn_paiement ?? (btn_paiement = new RelayCommand(getModeDePaiement));
                //return null;
            }
        }
        private void getModeDePaiement(object button)
        {
            System.Windows.Controls.Button clickedbutton = button as System.Windows.Controls.Button;
            //Si je ne suis pas un client pro je ne peux pas cliquer sur virement
            if (ClasseGlobale.Client.type == 0 && clickedbutton.Tag.ToString() == "VirementBancaire")
            {
                MessageBox.Show("Les virements bancaires sont résevés aux clients professionnels");
            }
            else
            {
                _mode_de_paiement = clickedbutton.Tag.ToString();
            }
            
        }
        #endregion

        #region Bouton valider le montant par mode de paiement ou valider la remise
        //Gère les boutons de mode de paiement
        ICommand btn_paiement_valider;
        public ICommand Btn_paiement_valider
        {
            get { return btn_paiement_valider ?? (btn_paiement_valider = new RelayCommand(completeLaListeDesModesDePaiement)); }
        }
        private void completeLaListeDesModesDePaiement(object button)
        {
            Button clickedbutton = button as Button;
            if (clickedbutton != null)
            {
                //Si j'ai sélectionné un mode de paiement et que la remise n'a pas bougée
                if (Mode_de_paiement != "" && checkRemise[0] == Txb_paiement_montantRemise)
                {
                    applyModeDePaiement();
                }

                //Si je n'ai pas sélectionné de mode de paiement mais que la remise à changée, j'applique la remise
                else if (Mode_de_paiement == "" && checkRemise[0] != Txb_paiement_montantRemise)
                {
                    applyRemise();
                }
                else
                {
                    //Récupération de la valeur saisie pour ce paiement
                    float valeur_desiree = Txb_paiement_montantParMoyenPaiement;
                    //MessageBox.Show("Veuillez d'abord appliquer votre remise, puis le paiement.\nInfo : ");
                    applyRemise();

                    Txb_paiement_montantParMoyenPaiement = valeur_desiree;

                    applyModeDePaiement();

                }

            }


        }

        #endregion

        #region
        public DelegateCommand<PaiementListeVM> DeletePaiement
        {
            get
            {
                return this._deletePaiement ?? (this._deletePaiement = new DelegateCommand<PaiementListeVM>(
                                                                       this.ExecuteDeletePaiement,
                                                                       (arg) => true));
            }
        }

        private void ExecuteDeletePaiement(PaiementListeVM obj)
        {
            //MessageBox.Show("mode de paiement : "+obj.ModeDePaiement+"\nMontant : "+obj.Montant);

            //listeDeMontantParMoyenPaiement[Mode_de_paiement] = Txb_paiement_montantParMoyenPaiement;

            //Actualisation des champs de texte
            Reste_a_payer += float.Parse(obj.Montant);

            //Suppression des champs de la liste
            listeDeMontantParMoyenPaiement.dico.Remove(obj.ModeDePaiement);

            //Attributio de la nouvelle valeur au champ de texte
            Txb_paiement_montantParMoyenPaiement = Reste_a_payer;

            //Initialisation -> Permet que la valeur bindée (en classe globale, soit modifiée aussi)
            InitialiseLaListeDePaiement();
        }

        #endregion


        #region ObservableCollection
        public ObservableCollection<PaiementListeVM> ContenuListePaiement
        {
            get
            {
                if (ClasseGlobale._contenuListePaiement == null)//ClasseGlobale.
                {
                    ClasseGlobale.initializeContenuListePaiement();
                    //_contenuListePaiement = new ObservableCollection<PaiementListeVM>();
                }
                return ClasseGlobale._contenuListePaiement;
            }

            set
            {
                if (value != null)
                {
                    ClasseGlobale._contenuListePaiement = value;
                    //_contenuListePaiement = value;
                    NotifyPropertychange("ContenuListePaiement");
                }
            }
        }
        #endregion

        #endregion

        #region Methods
        //initialisation de la page de paiement
        public void lePaiement()
        {
            //On récupère la classe globale contenant les articles et on calcul le prix
            ObservableCollection<ArticlesVM> cmdDetail = ClasseGlobale._contentDetailCommande;


            float prixHT = 0;
            float prixTTC = 0;
            float prixTTCrendu = 0;
            float prixHTrendu = 0;
            //Si je viens de la page de création de la commande
            if (cmdDetail != null)
            {
                try
                {
                    foreach (ArticlesVM art in cmdDetail)
                    {
                        //prixTTC += art.typeArticle.TTC;
                        //prixHT += art.typeArticle.TTC * (1 - art.typeArticle.TVA / 100);

                        prixTTC = (float)((decimal)prixTTC + (decimal)art.typeArticle.TTC);
                        prixHT = (float)((decimal)prixHT + (decimal)art.typeArticle.TTC * (1 - (decimal)art.typeArticle.TVA / 100));
                        prixHT = (float)Math.Round((decimal)prixHT + (decimal)art.typeArticle.TTC * (1 - (decimal)art.typeArticle.TVA / 100), 2, MidpointRounding.AwayFromZero);
                        
                    }
                    Label_paiement_prixHT = prixHT + " €";
                    Label_paiement_prixTTC = prixTTC + " €";
                    Label_paiement_montant = prixTTC - Txb_paiement_montantRemise + " €";
                }
                catch (Exception )
                {
                    //Inscription en log
                }
            }
            //Si je viens de la page de sélection des articles à payer
            else if (ClasseGlobale._rendreArticlesSelectionnes != null)
            {
                try
                {
                    foreach (Article artic in ClasseGlobale._rendreArticlesSelectionnes)
                    {
                        prixTTCrendu = (float)((decimal)prixTTCrendu + (decimal)artic.TTC);
                        prixHTrendu = (float)((decimal)prixHTrendu + (decimal)artic.TTC * (1 - (decimal)artic.TVA / 100));
                    }
                    Label_paiement_prixHT = prixHTrendu - prixHT + " €";
                    Label_paiement_prixTTC = prixTTCrendu - prixTTC + " €";
                    Label_paiement_montant = prixTTCrendu - prixTTC - Txb_paiement_montantRemise + " €";
                }
                catch (Exception e)
                {
                    //Inscription en log
                }
            }
            


        }


        private void CartePrepayee()
        {
            int fid = 0;
            int fid50 = 0;
            int fid80 = 0;
            int fid100 = 0;
            int autre = 0;
            ObservableCollection<ArticlesVM> cmdDetails = ClasseGlobale._contentDetailCommande;
            List<Article> ListeSelectArt = ClasseGlobale._rendreArticlesSelectionnes;

            if (cmdDetails != null)//si nouvelle commande
            {

                // On regarde le contenu de la commande
                foreach (ArticlesVM arts in cmdDetails)
                {
                    if (arts.ArticlesName == "Fidelite 50 euros" || arts.ArticlesName == "Fidelité 80 euros" || arts.ArticlesName == "Fidélité 100 euros")
                    {
                        fid = fid + 1;
                        if(arts.ArticlesName == "Fidelite 50 euros")
                        { fid50 = fid50 + 60; }
                        else if (arts.ArticlesName == "Fidelité 80 euros")
                        { fid80 = fid80 + 100; }
                        else if (arts.ArticlesName == "Fidélité 100 euros")
                        { fid100 = fid100 + 150; }
                    }
                    else
                    {
                        autre = autre + 1;
                    }

                }
                if (fid > 0 && autre > 0)
                {
                    MessageBox.Show("Veuillez faire deux commandes différentes pour la fidélité et le reste.");
                }
                if (fid > 0 && autre == 0) // Uniquement de la fidélité
                {
                    Payement paiementbis;
                    Client clientbis = ClasseGlobale.Client;
                    ICollection<String> liste_des_moyens_de_paiementbis = listeDeMontantParMoyenPaiement.dico.Keys;
                    Commande cmdbis = new Commande(DateTime.Now, true, Txb_paiement_montantRemise, clientbis, ClasseGlobale.employeeEnCours.id);
                    
                    cmdbis = CommandeDAO.selectCommandeById(CommandeDAO.lastCommande().id, false, true, false);
                    int cb = 0;

                    foreach (String monMoyenDePaiementbis in liste_des_moyens_de_paiementbis)
                    {
                        paiementbis = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiementbis], monMoyenDePaiementbis, cmdbis.id);

                        if (monMoyenDePaiementbis == "solde")
                        {
                            cb = cb + 1;
                        }
                        else
                        {

                        }
                    }
                    if (cb != 0)
                    {
                        MessageBox.Show("Veuillez choisir un autre moyen de paiement.");
                    }
                    else // Si on a de la fidélité et que le mode de paiement est tout sauf solde
                    {
                        Payement paiementfid;
                        Client clientfid = ClasseGlobale.Client;
                        ICollection<String> liste_des_moyens_de_paiementfid = listeDeMontantParMoyenPaiement.dico.Keys;
                        Commande cmdfid = new Commande(DateTime.Now, true, Txb_paiement_montantRemise, clientfid, ClasseGlobale.employeeEnCours.id);
                        CommandeDAO.insertCommande(cmdfid);
                        cmdfid = CommandeDAO.selectCommandeById(CommandeDAO.lastCommande().id, false, true, false);

                        float soldetotal = 0;
                        foreach (String monMoyenDePaiementfid in liste_des_moyens_de_paiementbis)
                        {
                            Client clientModif = new Client();
                            clientModif = ClientDAO.selectClientById(clientfid.id, false, false, false);
                            paiementfid = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiementfid], monMoyenDePaiementfid, cmdfid.id);
                            clientModif.solde = clientfid.solde + fid50 + fid80 + fid100;
                            int x = ClientDAO.updateClient(clientModif);
                            PayementDAO.insertPaiement(paiementfid);
                            
                            soldetotal = paiementfid.montant;
                        }
                        MessageBox.Show("Solde ajouté au compte client " + clientbis.nom + " " + clientbis.prenom);

                        
                        CommandeDAO.updateCommandeSolde(cmdfid.id);

                        Commande cmdTota = CommandeDAO.selectCommandeById(cmdfid.id, true, true, true);

                        ObservableCollection<ArticlesVM> cmdDetail = ClasseGlobale._contentDetailCommande;

                        //Enregistrement des articles
                        foreach (ArticlesVM artVM in cmdDetail)
                        {
                            Article art = artVM.getArticle(cmdTota.id);
                            art.date_payee = DateTime.Now;
                            ArticleDAO.insertArticle(art);
                        }
                        try
                        {
                            RecuPaiement rp = new RecuPaiement(cmdTota);
                            rp.printRecuSolde(soldetotal);
                            

                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Impression refusée");
                        }
                        finally
                        {
                            //initialise tout
                            ClasseGlobale.SET_ALL_NULL();
                        }
                    }


                }
                if (fid == 0 && autre >= 0)
                {
                    ValidationPaiement();
                    //MessageBox.Show("validation paiement");
                }
            }




            //Si je viens de l'écran de rendu des articles
            else if (ListeSelectArt != null)
            {
                Client client = ClasseGlobale.Client;
                Commande comdRendu = ClasseGlobale._renduCommande;

                foreach (Article art in ListeSelectArt)
                {
                    //Mise à jour de la place convoyeur
                    //1 - dans la table convoyeur : on soustrait l'encombrement
                    //2 - dans la table article : id convoyeur devient nul

                    art.convoyeur.encombrement = (float)((decimal)art.convoyeur.encombrement - (decimal)art.type.encombrement);
                    //Si un article est à la même place, il faut modifier sa place convoyeur pour qu'elle corresponde au changement appliqué
                    //Permet la mise à jour correcte de la table convoyeur
                    foreach (Article art2 in ListeSelectArt)
                    {
                        //Si j'ai un autre article au même emplacement convoyeur
                        if (art2.convoyeur.id == art.convoyeur.id && art2.id != art.id)
                        {
                            //Je lui attribut le bon encombrement
                            art2.convoyeur.encombrement = art.convoyeur.encombrement;
                        }
                    }

                    PlaceConvoyeurDAO.updatePlaceConvoyeur(art.convoyeur);

                    Article artAdd = new Article(art.id, art.photo, art.commentaire, true, art.TVA, art.TTC, art.type, null, comdRendu.id);
                    artAdd.date_rendu = DateTime.Now;
                    artAdd.date_payee = DateTime.Now;

                    ArticleDAO.updateArticle(artAdd);


                }


                //Enregistrement du/des paiement(s)
                Payement paiement;
                ICollection<String> liste_des_moyens_de_paiement = listeDeMontantParMoyenPaiement.dico.Keys;
                foreach (String monMoyenDePaiement in liste_des_moyens_de_paiement)
                {
                    paiement = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiement], monMoyenDePaiement, comdRendu.id);

                    if (monMoyenDePaiement == "solde")
                    {
                        Client clientModif = new Client();
                        clientModif = ClientDAO.selectClientById(client.id, false, false, false);
                        if (clientModif.solde < paiement.montant)
                        {
                            MessageBox.Show("Solde insuffisant.");
                        }
                        else
                        {
                            clientModif.solde = client.solde - paiement.montant;
                            int x = ClientDAO.updateClient(clientModif);
                            PayementDAO.insertPaiement(paiement);
                        }

                    }
                    else
                    {
                        PayementDAO.insertPaiement(paiement);
                    }
                }



                //Mise à jour de la commande
                comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);

                //Vérification du paiement
                //1 - Je calcule le montant total de la commande
                //2 - Je calcule le montant payé total
                decimal prixTotalDeLaCommande = 0;
                foreach (Article article in comdRendu.listArticles)
                {
                    prixTotalDeLaCommande += (decimal)article.TTC;
                }


                decimal prixPayeTotal = 0;
                if (comdRendu.listPayements.Count > 0)
                {
                    foreach (Payement paiementEffectue in comdRendu.listPayements)
                    {
                        prixPayeTotal += (decimal)paiementEffectue.montant;
                    }
                }


                decimal resteAPayer = prixTotalDeLaCommande - prixPayeTotal - (decimal)Txb_paiement_montantRemise;
                if (resteAPayer == 0)
                {
                    //Mise à jour de la commande, le champ cmd_payee passe à 1
                    comdRendu.payee = true;
                    comdRendu.date_rendu = DateTime.Now;

                }
                else
                {
                    MessageBox.Show("Un reste à payer de " + resteAPayer);
                }
                if (Txb_paiement_montantRemise != 0)
                {
                    comdRendu.remise = Txb_paiement_montantRemise;
                }
                if (Txb_paiement_montantRemise != 0 || resteAPayer == 0)
                {
                    CommandeDAO.updateCommande(comdRendu);
                }



                Commande cmdTota = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);

                try
                {
                    RecuPaiement rp = new RecuPaiement(cmdTota);
                    rp.printRecu(true);
                    rp.printRecu(false);

                }
                catch (Exception)
                {
                    MessageBox.Show("Impression refusée");
                }
                finally
                {
                    //initialise tout
                    ClasseGlobale.SET_ALL_NULL();
                }
            }








        }

        public void ValidationPaiement()
        {
            Payement paiementbis;
            Client clientbis = ClasseGlobale.Client;
            ICollection<String> liste_des_moyens_de_paiementbis = listeDeMontantParMoyenPaiement.dico.Keys;
            Commande cmdbis = new Commande(DateTime.Now, true, Txb_paiement_montantRemise, clientbis, ClasseGlobale.employeeEnCours.id);
            cmdbis = CommandeDAO.selectCommandeById(CommandeDAO.lastCommande().id, false, true, false);
            
            foreach (String monMoyenDePaiementbis in liste_des_moyens_de_paiementbis)
            {
                paiementbis = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiementbis], monMoyenDePaiementbis, cmdbis.id);

                if (monMoyenDePaiementbis == "solde")
                {
                    
                    Client clientModifbis = new Client();
                    clientModifbis = ClientDAO.selectClientById(clientbis.id, false, false, false);
                    if (clientModifbis.solde < paiementbis.montant)
                    {
                        MessageBox.Show("Solde insuffisant.");
                    }
                    else
                    {
                        if (ClasseGlobale.Client != null)
                        {
                            //Récupération des articles de la commande, du client, et du paiement et enregistrement en bdd

                            Client client = ClasseGlobale.Client;

                            //Validation du paiement

                            if (Reste_a_payer == 0)
                            {
                                //Récupère la liste des articles, il y'en a qu'une seule qui soit initialisée. L'autre est nulle
                                ObservableCollection<ArticlesVM> cmdDetail = ClasseGlobale._contentDetailCommande;
                                List<Article> ListeSelectArt = ClasseGlobale._rendreArticlesSelectionnes;

                                //Si je viens de l'écran de nouvelle commande
                                if (cmdDetail != null)
                                {
                                    Commande cmd = new Commande(DateTime.Now, true, Txb_paiement_montantRemise, client, ClasseGlobale.employeeEnCours.id);
                                    CommandeDAO.insertCommande(cmd);
                                    cmd = CommandeDAO.selectCommandeById(CommandeDAO.lastCommande().id, false, true, false);

                                    //Enregistrement des articles
                                    foreach (ArticlesVM artVM in cmdDetail)
                                    {
                                        Article art = artVM.getArticle(cmd.id);
                                        art.date_payee = DateTime.Now;
                                        ArticleDAO.insertArticle(art);
                                        
                                    }

                                    //Enregistrement du/des paiement(s)
                                    Payement paiement;
                                    ICollection<String> liste_des_moyens_de_paiement = listeDeMontantParMoyenPaiement.dico.Keys;
                                    foreach (String monMoyenDePaiement in liste_des_moyens_de_paiement)
                                    {
                                        paiement = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiement], monMoyenDePaiement, cmd.id);

                                        if (monMoyenDePaiement == "solde")
                                        {
                                            Client clientModif = new Client();
                                            clientModif = ClientDAO.selectClientById(client.id, false, false, false);
                                            if (clientModif.solde < paiement.montant)
                                            {
                                                MessageBox.Show("Solde insuffisant.");
                                            }
                                            else
                                            {
                                                clientModif.solde = client.solde - paiement.montant;
                                                int x = ClientDAO.updateClient(clientModif);
                                                PayementDAO.insertPaiement(paiement);
                                            }

                                        }
                                        else
                                        {
                                            PayementDAO.insertPaiement(paiement);
                                        }
                                    }

                                    //Mise à jour de la table convoyeur

                                    foreach (PlaceConvoyeur place in ClasseGlobale.PlacesLibres.getList())
                                    {
                                        PlaceConvoyeurDAO.updatePlaceConvoyeur(place);
                                    }



                                    Commande cmdTota = CommandeDAO.selectCommandeById(cmd.id, true, true, true);

                                    //MessageBox.Show("La commande " + cmdTota.id + " à été enregistrée avec succès");
                                    try
                                    {
                                        RecuPaiement rp = new RecuPaiement(cmdTota);
                                        rp.printRecu(true);
                                        rp.printRecu(false);


                                        if (cmdTota.listArticles != null)
                                        {
                                            TicketVetement ticketVetement = new TicketVetement(cmdTota);
                                            ticketVetement.printAllArticleCmd();
                                        }
                                        else
                                            MessageBox.Show("La commande ne contient pas d'articles");

                                    }
                                    catch (Exception)
                                    {
                                        MessageBox.Show("Impression refusée");
                                    }
                                    finally
                                    {
                                        //initialise tout
                                        ClasseGlobale.SET_ALL_NULL();
                                    }


                                    //FactureExcel fe = new FactureExcel(CommandeDAO.selectCommandeById(cmd.id, true, true, true));
                                    //fe.printFacture();
                                }
                                //Si je viens de l'écran de rendu des articles
                                else if (ListeSelectArt != null)
                                {
                                    Commande comdRendu = ClasseGlobale._renduCommande;

                                    foreach (Article art in ListeSelectArt)
                                    {
                                        //Mise à jour de la place convoyeur
                                        //1 - dans la table convoyeur : on soustrait l'encombrement
                                        //2 - dans la table article : id convoyeur devient nul

                                        art.convoyeur.encombrement = (float)((decimal)art.convoyeur.encombrement - (decimal)art.type.encombrement);
                                        //Si un article est à la même place, il faut modifier sa place convoyeur pour qu'elle corresponde au changement appliqué
                                        //Permet la mise à jour correcte de la table convoyeur
                                        foreach (Article art2 in ListeSelectArt)
                                        {
                                            //Si j'ai un autre article au même emplacement convoyeur
                                            if (art2.convoyeur.id == art.convoyeur.id && art2.id != art.id)
                                            {
                                                //Je lui attribut le bon encombrement
                                                art2.convoyeur.encombrement = art.convoyeur.encombrement;
                                            }
                                        }

                                        PlaceConvoyeurDAO.updatePlaceConvoyeur(art.convoyeur);

                                        Article artAdd = new Article(art.id, art.photo, art.commentaire, true, art.TVA, art.TTC, art.type, null, comdRendu.id);
                                        artAdd.date_rendu = DateTime.Now;
                                        artAdd.date_payee = DateTime.Now;

                                        ArticleDAO.updateArticle(artAdd);


                                    }


                                    //Enregistrement du/des paiement(s)
                                    Payement paiement;
                                    ICollection<String> liste_des_moyens_de_paiement = listeDeMontantParMoyenPaiement.dico.Keys;
                                    foreach (String monMoyenDePaiement in liste_des_moyens_de_paiement)
                                    {
                                        paiement = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiement], monMoyenDePaiement, comdRendu.id);

                                        if (monMoyenDePaiement == "solde")
                                        {
                                            Client clientModif = new Client();
                                            clientModif = ClientDAO.selectClientById(client.id, false, false, false);
                                            if (clientModif.solde < paiement.montant)
                                            {
                                                MessageBox.Show("Solde insuffisant.");
                                            }
                                            else
                                            {
                                                clientModif.solde = client.solde - paiement.montant;
                                                int x = ClientDAO.updateClient(clientModif);
                                                PayementDAO.insertPaiement(paiement);
                                            }

                                        }
                                        else
                                        {
                                            PayementDAO.insertPaiement(paiement);
                                        }
                                    }



                                    //Mise à jour de la commande
                                    comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);

                                    //Vérification du paiement
                                    //1 - Je calcule le montant total de la commande
                                    //2 - Je calcule le montant payé total
                                    decimal prixTotalDeLaCommande = 0;
                                    foreach (Article article in comdRendu.listArticles)
                                    {
                                        prixTotalDeLaCommande += (decimal)article.TTC;
                                    }


                                    decimal prixPayeTotal = 0;
                                    if (comdRendu.listPayements.Count > 0)
                                    {
                                        foreach (Payement paiementEffectue in comdRendu.listPayements)
                                        {
                                            prixPayeTotal += (decimal)paiementEffectue.montant;
                                        }
                                    }


                                    decimal resteAPayer = prixTotalDeLaCommande - prixPayeTotal - (decimal)Txb_paiement_montantRemise;
                                    if (resteAPayer == 0)
                                    {
                                        //Mise à jour de la commande, le champ cmd_payee passe à 1
                                        comdRendu.payee = true;
                                        comdRendu.date_rendu = DateTime.Now;

                                    }
                                    else
                                    {
                                        MessageBox.Show("Un reste à payer de " + resteAPayer);
                                    }
                                    if (Txb_paiement_montantRemise != 0)
                                    {
                                        comdRendu.remise = Txb_paiement_montantRemise;
                                    }
                                    if (Txb_paiement_montantRemise != 0 || resteAPayer == 0)
                                    {
                                        CommandeDAO.updateCommande(comdRendu);
                                    }



                                    Commande cmdTota = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);

                                    try
                                    {
                                        RecuPaiement rp = new RecuPaiement(cmdTota);
                                        rp.printRecu(true);
                                        rp.printRecu(false);

                                    }
                                    catch (Exception)
                                    {
                                        MessageBox.Show("Impression refusée");
                                    }
                                    finally
                                    {
                                        //initialise tout
                                        ClasseGlobale.SET_ALL_NULL();
                                    }
                                }
                                //Si je ne  viens pas des pages précedentes
                                //Mes liste sont vides => paiement déjà effectué

                                else
                                {
                                    MessageBox.Show("La commande à été correctement enregistrée, cliquez sur le bouton home pour retourner à l'accueil");
                                }
                                //Accueil page2Obj = new Accueil(); //Create object of Page2
                                //page2Obj.Show(); //Show page2
                                //this.Close();

                            }
                            else
                            {
                                MessageBox.Show("Toute la commande n'a pas été payée. Veuillez s'il vous plait compléter l'intégralité du paiement.");
                            }

                        }
                        else
                        {
                            MessageBox.Show("La commande à été correctement enregistrée, cliquez sur le bouton home pour retourner à l'accueil");
                        }
                    }

                }
                else
                {
                    if (ClasseGlobale.Client != null)
                    {
                        //Récupération des articles de la commande, du client, et du paiement et enregistrement en bdd

                        Client client = ClasseGlobale.Client;

                        //Validation du paiement

                        if (Reste_a_payer == 0)
                        {
                            //Récupère la liste des articles, il y'en a qu'une seule qui soit initialisée. L'autre est nulle
                            ObservableCollection<ArticlesVM> cmdDetail = ClasseGlobale._contentDetailCommande;
                            List<Article> ListeSelectArt = ClasseGlobale._rendreArticlesSelectionnes;

                            //Si je viens de l'écran de nouvelle commande
                            if (cmdDetail != null)
                            {
                                Commande cmd = new Commande(DateTime.Now, true, Txb_paiement_montantRemise, client, ClasseGlobale.employeeEnCours.id);
                                CommandeDAO.insertCommande(cmd);
                                cmd = CommandeDAO.selectCommandeById(CommandeDAO.lastCommande().id, false, true, false);
                                float cmpencombrement = 0;
                                //On compte l'encombrement total des articles
                                foreach (ArticlesVM artencom in cmdDetail)
                                {
                                    //MessageBox.Show("place de articlevm ? : " + artencom.PlaceConvoyeur.emplacement);
                                    Article arte = artencom.getArticle(cmd.id);
                                    if (arte.convoyeur != null)
                                    {
                                        //MessageBox.Show("place de article ? : " + arte.convoyeur.emplacement);

                                        cmpencombrement = cmpencombrement + arte.type.encombrement;
                                    }

                                    
                                }
                                //MessageBox.Show("encombrement total : " + cmpencombrement);


                                //Enregistrement des articles
                                foreach (ArticlesVM artVM in cmdDetail)
                                {
                                    
                                    Article art = artVM.getArticle(cmd.id);
                                    //if (art.convoyeur != null)
                                    //{
                                    //    MessageBox.Show("article place : " + art.convoyeur.emplacement);
                                    //}
                                    
                                    art.date_payee = DateTime.Now;
                                    ArticleDAO.insertArticle(art);
                                    //MessageBox.Show("conv au foreach : " + art.convoyeur.id);
                                }

                                //Enregistrement du/des paiement(s)
                                Payement paiement;
                                ICollection<String> liste_des_moyens_de_paiement = listeDeMontantParMoyenPaiement.dico.Keys;
                                foreach (String monMoyenDePaiement in liste_des_moyens_de_paiement)
                                {
                                    paiement = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiement], monMoyenDePaiement, cmd.id);

                                    if (monMoyenDePaiement == "solde")
                                    {
                                        Client clientModif = new Client();
                                        clientModif = ClientDAO.selectClientById(client.id, false, false, false);
                                        if (clientModif.solde < paiement.montant)
                                        {
                                            MessageBox.Show("Solde insuffisant.");
                                        }
                                        else
                                        {
                                            clientModif.solde = client.solde - paiement.montant;
                                            int x = ClientDAO.updateClient(clientModif);
                                            PayementDAO.insertPaiement(paiement);
                                        }

                                    }
                                    else
                                    {
                                        PayementDAO.insertPaiement(paiement);
                                    }
                                }

                                //Mise à jour de la table convoyeur
                                //foreach (PlaceConvoyeur placex in ClasseGlobale.PlacesLibres.getList())
                                //{
                                //    MessageBox.Show("placex : " + placex.emplacement);
                                //}

                                foreach (PlaceConvoyeur place in ClasseGlobale.PlacesLibres.getList())
                                {
                                    PlaceConvoyeurDAO.updatePlaceConvoyeur(place);
                                    //MessageBox.Show("place updated : " + place.emplacement + " et encomb : " + place.encombrement);
                                }



                                Commande cmdTota = CommandeDAO.selectCommandeById(cmd.id, true, true, true);

                                //MessageBox.Show("La commande " + cmdTota.id + " à été enregistrée avec succès");
                                try
                                {
                                    RecuPaiement rp = new RecuPaiement(cmdTota);
                                    rp.printRecu(true);
                                    rp.printRecu(false);


                                    if (cmdTota.listArticles != null)
                                    {
                                        TicketVetement ticketVetement = new TicketVetement(cmdTota);
                                        ticketVetement.printAllArticleCmd();
                                    }
                                    else
                                        MessageBox.Show("La commande ne contient pas d'articles");

                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Impression refusée");
                                }
                                finally
                                {
                                    //initialise tout
                                    ClasseGlobale.SET_ALL_NULL();
                                }


                                //FactureExcel fe = new FactureExcel(CommandeDAO.selectCommandeById(cmd.id, true, true, true));
                                //fe.printFacture();
                            }
                            //Si je viens de l'écran de rendu des articles
                            else if (ListeSelectArt != null)
                            {
                                Commande comdRendu = ClasseGlobale._renduCommande;

                                foreach (Article art in ListeSelectArt)
                                {
                                    //Mise à jour de la place convoyeur
                                    //1 - dans la table convoyeur : on soustrait l'encombrement
                                    //2 - dans la table article : id convoyeur devient nul

                                    art.convoyeur.encombrement = (float)((decimal)art.convoyeur.encombrement - (decimal)art.type.encombrement);
                                    //Si un article est à la même place, il faut modifier sa place convoyeur pour qu'elle corresponde au changement appliqué
                                    //Permet la mise à jour correcte de la table convoyeur
                                    foreach (Article art2 in ListeSelectArt)
                                    {
                                        //Si j'ai un autre article au même emplacement convoyeur
                                        if (art2.convoyeur.id == art.convoyeur.id && art2.id != art.id)
                                        {
                                            //Je lui attribut le bon encombrement
                                            art2.convoyeur.encombrement = art.convoyeur.encombrement;
                                        }
                                    }

                                    PlaceConvoyeurDAO.updatePlaceConvoyeur(art.convoyeur);

                                    Article artAdd = new Article(art.id, art.photo, art.commentaire, true, art.TVA, art.TTC, art.type, null, comdRendu.id);
                                    artAdd.date_rendu = DateTime.Now;
                                    artAdd.date_payee = DateTime.Now;

                                    ArticleDAO.updateArticle(artAdd);


                                }


                                //Enregistrement du/des paiement(s)
                                Payement paiement;
                                ICollection<String> liste_des_moyens_de_paiement = listeDeMontantParMoyenPaiement.dico.Keys;
                                foreach (String monMoyenDePaiement in liste_des_moyens_de_paiement)
                                {
                                    paiement = new Payement(DateTime.Now, listeDeMontantParMoyenPaiement[monMoyenDePaiement], monMoyenDePaiement, comdRendu.id);

                                    if (monMoyenDePaiement == "solde")
                                    {
                                        Client clientModif = new Client();
                                        clientModif = ClientDAO.selectClientById(client.id, false, false, false);
                                        if (clientModif.solde < paiement.montant)
                                        {
                                            MessageBox.Show("Solde insuffisant.");
                                        }
                                        else
                                        {
                                            clientModif.solde = client.solde - paiement.montant;
                                            int x = ClientDAO.updateClient(clientModif);
                                            PayementDAO.insertPaiement(paiement);
                                        }

                                    }
                                    else
                                    {
                                        PayementDAO.insertPaiement(paiement);
                                    }
                                }



                                //Mise à jour de la commande
                                comdRendu = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);

                                //Vérification du paiement
                                //1 - Je calcule le montant total de la commande
                                //2 - Je calcule le montant payé total
                                decimal prixTotalDeLaCommande = 0;
                                foreach (Article article in comdRendu.listArticles)
                                {
                                    prixTotalDeLaCommande += (decimal)article.TTC;
                                }


                                decimal prixPayeTotal = 0;
                                if (comdRendu.listPayements.Count > 0)
                                {
                                    foreach (Payement paiementEffectue in comdRendu.listPayements)
                                    {
                                        prixPayeTotal += (decimal)paiementEffectue.montant;
                                    }
                                }


                                decimal resteAPayer = prixTotalDeLaCommande - prixPayeTotal - (decimal)Txb_paiement_montantRemise;
                                if (resteAPayer == 0)
                                {
                                    //Mise à jour de la commande, le champ cmd_payee passe à 1
                                    comdRendu.payee = true;
                                    comdRendu.date_rendu = DateTime.Now;

                                }
                                else
                                {
                                    MessageBox.Show("Un reste à payer de " + resteAPayer);
                                }
                                if (Txb_paiement_montantRemise != 0)
                                {
                                    comdRendu.remise = Txb_paiement_montantRemise;
                                }
                                if (Txb_paiement_montantRemise != 0 || resteAPayer == 0)
                                {
                                    CommandeDAO.updateCommande(comdRendu);
                                }



                                Commande cmdTota = CommandeDAO.selectCommandeById(comdRendu.id, true, true, true);

                                try
                                {
                                    RecuPaiement rp = new RecuPaiement(cmdTota);
                                    rp.printRecu(true);
                                    rp.printRecu(false);

                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Impression refusée");
                                }
                                finally
                                {
                                    //initialise tout
                                    ClasseGlobale.SET_ALL_NULL();
                                }
                            }
                            //Si je ne  viens pas des pages précedentes
                            //Mes liste sont vides => paiement déjà effectué

                            else
                            {
                                MessageBox.Show("La commande à été correctement enregistrée, cliquez sur le bouton home pour retourner à l'accueil");
                            }
                            //Accueil page2Obj = new Accueil(); //Create object of Page2
                            //page2Obj.Show(); //Show page2
                            //this.Close();

                        }
                        else
                        {
                            MessageBox.Show("Toute la commande n'a pas été payée. Veuillez s'il vous plait compléter l'intégralité du paiement.");
                        }

                    }
                    else
                    {
                        MessageBox.Show("La commande à été correctement enregistrée, cliquez sur le bouton home pour retourner à l'accueil");
                    }
                }
            }









            

        }

        private void applyModeDePaiement()
        {
            //On vérifie qu'un mode de paiement a bien été sélectionné
            if (Mode_de_paiement != "")
            {
                //On vérifie que le montant est positif
                if (Txb_paiement_montantParMoyenPaiement >= 0)
                {
                    //S'il n'a rien mis dans le text box, on ne fait rien et on le signale à l'utilisateur
                    if (Txb_paiement_montantParMoyenPaiement != 0)
                    {
                        //On vérifie que le montant désiré est inférieur ou égal au reste à payer
                        if (Txb_paiement_montantParMoyenPaiement <= Reste_a_payer)
                        {
                            //Vérifie s'il n'y a pas de problème de paiement solde
                            //Lorsqu'une commande est payée en solde, elle n'est payée qu'en solde
                            if (soldeOK())
                            {
                                //Ajout de mon montant et du mode de paiement dans le dico
                                listeDeMontantParMoyenPaiement[Mode_de_paiement] = Txb_paiement_montantParMoyenPaiement;

                                InitialiseLaListeDePaiement();

                                //Je met dans le text box le nouveau reste à payer. Le mélange decimal/float permet de ne pas avoir de problème de précision
                                //comme 15.56668879 ou autre
                                Txb_paiement_montantParMoyenPaiement = (float)((decimal)Reste_a_payer - (decimal)Txb_paiement_montantParMoyenPaiement);

                                //Redéfinition du reste à payer
                                Reste_a_payer = Txb_paiement_montantParMoyenPaiement;
                                ClasseGlobale.reste_a_payer = Reste_a_payer;
                                //MessageBox.Show("ma remise vaut : " + Txb_paiement_montantRemise + "\nprix du paiement : " + Txb_paiement_montantParMoyenPaiement + "\n Reste_a_payer : " + Reste_a_payer);
                                Mode_de_paiement = "";
                            }
                        }
                        else
                        {
                            //Redéfinition du contenu du text box
                            Txb_paiement_montantParMoyenPaiement = Reste_a_payer;
                            MessageBox.Show("Problème lors de la validation de ce paiement.\nLa somme désirée est supérieure à la somme due.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Paiement de 0 € refusé. Veuillez renseigner la valeur désirée du paiement.");
                    }
                }
                else
                {
                    //Redéfinition du contenu du text box
                    Txb_paiement_montantParMoyenPaiement = Reste_a_payer;
                    MessageBox.Show("Vous ne pouvez pas saisir des montants négatifs");
                }
            }
            else
            {
                MessageBox.Show("Vous devez sélectionner un mode de paiement");
            }



        }

        private bool soldeOK()
        {
            //Si je viens 
            //de l'écran rendu et que 
            //tout les articles sont sélectionnés alors je peux payer en clean way
            List<Article> ListeSelectArt = ClasseGlobale._rendreArticlesSelectionnes;

            if (ListeSelectArt != null)//Je viens de l'écran de rendu donc toute la commande doit être sélectionnée
            {
                if (ClasseGlobale._renduCommande != null)
                {
                    if (ClasseGlobale._renduCommande.listArticles.Count == ListeSelectArt.Count || Mode_de_paiement != "solde")
                    {
                        //Si tous les articles sont sélectionnés je vérifie la possibilité solde
                        return checkUnicitePaiementsolde();
                    }

                    //Sinon le paiemnt par solde n'est pas possible
                    MessageBox.Show("Possibilité de payer par solde annulée, il faut sélectionner tous les articles de la commande");
                    return false;
                }
                else
                {
                    MessageBox.Show("La commande à rendre n'est pas retrouvée.\n Problème logiciel grave, contactez l'administrateur");
                    return false;
                }

            }
            return checkUnicitePaiementsolde();



        }

        private bool checkUnicitePaiementsolde()
        {
            //Si je désire payer en solde, je vérifie que ma liste de paiement ne contient pas de paiement autre que solde
            //Sinon, on vérifie qu'aucun paiement solde n'a été effectué
            if (Mode_de_paiement == "solde")
            {
                //Si mon seul paiement est en solde ou si j'en ai pas return true
                if (listeDeMontantParMoyenPaiement.dico.Count == 1 && listeDeMontantParMoyenPaiement.dico.Keys.Contains("solde")
                    ||
                    listeDeMontantParMoyenPaiement.dico.Count == 0)
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("Vous ne pouvez pas ajouter de paiement par solde, supprimez d'abord les autres paiements");
                    return false;
                }
            }
            else
            {
                if (listeDeMontantParMoyenPaiement.dico.Keys.Contains("solde"))
                {
                    MessageBox.Show("Vous ne pouvez pas ajouter de paiement autre que solde.");
                    return false;
                }
                else
                {
                    //Aucun paiement solde => roulez jeunesse
                    return true;
                }


            }
        }

        private void InitialiseLaListeDePaiement()
        {
            //On récupère tous les modes de paiements
            System.Collections.Generic.ICollection<String> liste_des_moyens_de_paiement = listeDeMontantParMoyenPaiement.dico.Keys;

            //On crèe la liste que l'on référencera plus tard dans la classe globale
            ObservableCollection<PaiementListeVM> contenuListePaiementTampon = new ObservableCollection<PaiementListeVM>();

            //initialisation de la liste de paiement en globale
            ClasseGlobale.initializeContenuListePaiement();

            //Reconstruction à partir du dico mis à jour
            foreach (String monMoyenDePaiement in liste_des_moyens_de_paiement)
            {
                contenuListePaiementTampon.Add(new PaiementListeVM()
                {
                    ModeDePaiement = monMoyenDePaiement,
                    Montant = listeDeMontantParMoyenPaiement[monMoyenDePaiement].ToString()
                });
            }

            //On donne une nouvelle référence à notre liste globale de client checkRemise
            ContenuListePaiement = contenuListePaiementTampon;
            ClasseGlobale.listeDeMontantParMoyenPaiement = listeDeMontantParMoyenPaiement;
        }

        private void applyRemise()
        {
            //enlève la remise précédente et applique la nouvelle remise
            checkRemise[1] = checkRemise[0];//met le montant de l'ancienne à la place de la nouvelle
            checkRemise[0] = Txb_paiement_montantRemise;
            Txb_paiement_montantParMoyenPaiement = Reste_a_payer - checkRemise[0] + checkRemise[1];
            Reste_a_payer = Txb_paiement_montantParMoyenPaiement;

            //Réinitialise l'ancienne valeur
            checkRemise[1] = 0;
            ClasseGlobale.remise = checkRemise[0];
        }

        #endregion

    }

    #region classes internes
    public class GetPaiement
    {
        /// <summary>
        /// 
        /// </summary>
        public String Label_nomModePaiement { get; set; }

        public float Label_MontantPayerParMode { get; set; }
    }

    class ListePaiement
    {

        private readonly IDictionary<string, float> maListeDePaiement = new Dictionary<string, float>();

        public IDictionary<string, float> dico
        {
            get
            {
                return maListeDePaiement;
            }
        }

        public float this[string key]
        {
            get
            {
                return maListeDePaiement[key];
            }

            // Je mets la nouvelle valeur pour ce mode de paiement, attention le calcul (- et + une certaine somme) se fait à l'extérieur
            set
            {
                //maListeDePaiement[key] = value;
                if (maListeDePaiement.ContainsKey(key))
                {
                    maListeDePaiement[key] += value;
                }
                else
                {
                    maListeDePaiement.Add(key, value);
                }

            }
        }


    }

    #endregion
}