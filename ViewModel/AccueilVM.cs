﻿using App_pressing_Loreau.Helper;
using App_pressing_Loreau.ViewModel;
using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Model.DTO;
using App_pressing_Loreau.View;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.IO;
using Microsoft.Practices.Prism.Commands;
using System.Windows.Media.Animation;




namespace App_pressing_Loreau
{
    /// <summary>
    /// ViewModel pour la classe Accueil.xaml
    /// Quelque soit le bouton de l'accueil qui est cliqué (header) le set est appelé puis le getter deux fois
    /// </summary>
    public class AccueilVM : ObservableObject
    {
        #region Attributs

        private IPageViewModel _accessUserControl;
        private List<CategoryItem> _listeUser;
        private String _label_Accueil_NomUser;
    

        private Brush _btn_receptionColor;
        private Brush _btn_renduColor;
        private Brush _Btn_emballageColor;
        private Brush _btn_impressionColor;
        private Brush _btn_smsColor;
   
        private Brush _btn_administrateurColor;
        private Brush _btn_convoyeurColor;

        private Brush _colorUserConnect;

        ICommand lesUtilisateurs;
        private DelegateCommand<AccueilVM> _btn_accueil_image;

        #endregion

        #region Constructeur
        public AccueilVM()
        {

            UtilisateurListe();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
            
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_smsColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

            ColorUserConnect = Brushes.Linen;

            Label_Accueil_NomUser = "Choisissez un employé";
        }

        #endregion

        #region Propriétés et Commandes
       
        public String Label_Accueil_NomUser
        {
            get { return _label_Accueil_NomUser; }
            set
            {

                _label_Accueil_NomUser = value;
                NotifyPropertychange("Label_Accueil_NomUser");

            }
        }
        public IPageViewModel accessUserControl
        {
            get { return _accessUserControl; }
            set
            {
                if (_accessUserControl != value)
                {
                    _accessUserControl = value;
                    NotifyPropertychange("accessUserControl");
                }
            }
        }

        public List<CategoryItem> ListeUser
        {
            get { return this._listeUser; }
            set
            {
                this._listeUser = value;
                NotifyPropertychange("ListeUser");
            }
        }

        public ICommand LesUtilisateurs
        {
            get
            {
                return lesUtilisateurs ?? (lesUtilisateurs = new RelayCommand(ClickSurUtilisateur));
            }

        }

        public Brush Btn_receptionColor
        {
            get
            {
                return _btn_receptionColor;
            }
            set
            {
                _btn_receptionColor = value;
                ClasseGlobale.SET_ALL_NULL();
                RaisePropertyChanged("Btn_receptionColor");

            }
        }
        public Brush Btn_renduColor
        {
            get { return _btn_renduColor; }
            set
            {
                ClasseGlobale.SET_ALL_NULL();
                _btn_renduColor = value;
                RaisePropertyChanged("Btn_renduColor");
            }
        }
        public Brush Btn_emballageColor
        {
            get { return _Btn_emballageColor; }
            set
            {
                ClasseGlobale.SET_ALL_NULL();
                _Btn_emballageColor = value;
                RaisePropertyChanged("Btn_emballageColor");
            }
        }
        public Brush Btn_convoyeurColor
        {
            get { return _btn_convoyeurColor; }
            set
            {
                ClasseGlobale.SET_ALL_NULL();
                _btn_convoyeurColor = value;
                RaisePropertyChanged("Btn_convoyeurColor");
            }
        }
        
        public Brush Btn_smsColor
        {
            get { return _btn_smsColor; }
            set
            {
                ClasseGlobale.SET_ALL_NULL();
                _btn_smsColor = value;
                RaisePropertyChanged("Btn_smsColor");
            }
        }
        public Brush Btn_impressionColor
        {
            get { return _btn_impressionColor; }
            set
            {
                ClasseGlobale.SET_ALL_NULL();
                _btn_impressionColor = value;
                RaisePropertyChanged("Btn_impressionColor");
            }
        }
        public Brush Btn_administrateurColor
        {
            get { return _btn_administrateurColor; }
            set
            {
                ClasseGlobale.SET_ALL_NULL();
                _btn_administrateurColor = value;
                RaisePropertyChanged("Btn_administrateurColor");
            }
        }

        public Brush ColorUserConnect
        {
            get { return _colorUserConnect; }
            set
            {
                _colorUserConnect = value;
                NotifyPropertychange("ColorUserConnect");
            }
        }




        #region Command bouton menu
        // Button permetTant la Linenirection vers la page Identification Client 
        public ICommand Btn_accueil_receptionClient
        {
            get
            {
                return new RelayCommand(p => identificationClientVM(),
                    p => ClasseGlobale.employeeEnCours != null);
            }
        }

        // Button permetTant la Linenirection vers la page Restitution client 
        public ICommand Btn_accueil_renduArticles
        {
            get
            {
                return new RelayCommand(p => restitutionArticleVM(),
                    p => ClasseGlobale.employeeEnCours != null);
            }
        }



        // Button permetTant la Linenirection vers la page emballage
           public ICommand Btn_accueil_emballage
         {
           get
            {
             return new RelayCommand(p => emballageVM(),
             p => ClasseGlobale.employeeEnCours != null);
        }
         }




        // Button permetTant la Linenirection vers la page sms
        public ICommand Btn_accueil_sms
        {
            get
            {
                return new RelayCommand(p => smsVM(),
                    p => ClasseGlobale.employeeEnCours != null);
            }
        }



        // Button permetTant la Linenirection vers la page impression 
        public ICommand Btn_accueil_impression
        {
            get
            {
                return new RelayCommand(p => impressionVM(),
                    p => ClasseGlobale.employeeEnCours != null);
            }
        }



        // Button permetTant la Linenirection vers la page Connexion administateur
        public ICommand Btn_accueil_administrateur
        {
            get
            {
                return new RelayCommand(p => administateurVM(),
                    p => ClasseGlobale.employeeEnCours != null );
            }
        }


        // Button permetTant la Linenirection vers la page convoyeur
        public ICommand Btn_accueil_convoyeur
        {
            get
            {
                return new RelayCommand(p => convoyeurVM(),
                    p => ClasseGlobale.employeeEnCours != null);
            }
        }

        #endregion

        // Button permetTant de revenir a la page preincipale 
        public DelegateCommand<AccueilVM> Btn_accueil_image
        {
            get
            {
                return this._btn_accueil_image ?? (this._btn_accueil_image = new DelegateCommand<AccueilVM>(accueilVM,
                    (arg) => true));
            }
        }


        #endregion

        #region Méthodes


        #region Methodes Button menu
        //Methodes des Linenirection vers le ViewModel de l'Identification client
        public void identificationClientVM()
        {
            Btn_receptionColor = Brushes.Linen;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
      
            Btn_smsColor = Brushes.DimGray;
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

            accessUserControl = new IdentificationClientVM();//Problème avec le datacontext... la vm est liée via data context => ajouter view à dockpanel

        }
        //Methodes des Linenirection vers le ViewModel de l restitution client
        public void restitutionArticleVM()
        {
            accessUserControl = new RestitutionArticlesVM();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.Linen;
            Btn_emballageColor = Brushes.DimGray;
 
            Btn_smsColor = Brushes.DimGray;
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

        }

        public void emballageVM()
        {
            accessUserControl = new EmballageVM();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.Linen;

            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_smsColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

        }


        public void smsVM()
        {
            accessUserControl = new SmsVM();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
           
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_smsColor = Brushes.Linen;
            Btn_administrateurColor = Brushes.DimGray;

        }
        public void impressionVM()
        {
            accessUserControl = new ImpressionVM();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
            
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.Linen;
            Btn_smsColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

        }

        public void administateurVM()
        {
            accessUserControl = new PageAdministrateurVM();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
            
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_smsColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.Linen;

        }

        public void convoyeurVM()
        {
            accessUserControl = new ConvoyeurVM();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
            
            Btn_convoyeurColor = Brushes.Linen;
            Btn_impressionColor = Brushes.DimGray;
            Btn_smsColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

        }

        #endregion

        // Action sur button utilisateur
        public void ClickSurUtilisateur(object User)
        {
            Button clickedbutton = User as Button;

            //Action qui suit si le button utilisateur est actionné
            if (clickedbutton != null)
            {
                //Si un utilisateur est en cours 
                if (ClasseGlobale.employeeEnCours != null)
                {
                    ColorUserConnect = Brushes.Orange;

                    //Un processus est en cours 
                    if (accessUserControl != null)
                    {
                        
                        MessageBox.Show("Un utilisateur est en cours d'utilisation \n Un processus est en cours");

                    }
                    else
                    {
                        MessageBox.Show("Un utilisateur est en cours d'utilisation");

                    }
                }
                else if (accessUserControl != null)
                {
                    MessageBox.Show("Un processus est en cours");

                }
                    //Changement d utilisateur
                else
                {
                    try
                    {
                        ClasseGlobale.employeeEnCours = EmployeDAO.selectEmployeById(Int32.Parse(clickedbutton.Tag.ToString()));
                        Label_Accueil_NomUser = "Utilisateur : " + ClasseGlobale.employeeEnCours.nom + " " + ClasseGlobale.employeeEnCours.prenom;
                        ColorUserConnect = Brushes.DimGray;
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("" + e);

                    }
                    

                }

            }
        }
        public void UtilisateurListe()
        {
            try
            {

          

            ListeUser = new List<CategoryItem>();

            List<Employe> emp = (List<Employe>)EmployeDAO.selectEmployes();
            if (emp != null)
            {
                foreach (Employe em in emp)
                {
                    ListeUser.Add(new CategoryItem() { ButtonUserContent = em.prenom, ButtonUserTag = em.id, ButtonUserBackground = Brushes.DimGray});
                }
            }
            else
            {
                ListeUser.Add(new CategoryItem() { ButtonUserContent = "Erreur", ButtonUserBackground = Brushes.DimGray });
                   
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Erreur chargement utilisateur" + e);
            }
        }




        public void accueilVM(AccueilVM obj)
        {

            obj.UtilisateurListe();

            Btn_receptionColor = Brushes.DimGray;
            Btn_renduColor = Brushes.DimGray;
            Btn_emballageColor = Brushes.DimGray;
            
            Btn_convoyeurColor = Brushes.DimGray;
            Btn_impressionColor = Brushes.DimGray;
            Btn_smsColor = Brushes.DimGray;
            Btn_administrateurColor = Brushes.DimGray;

            ClasseGlobale.SET_ALL_NULL();


            obj.accessUserControl = null;
            ClasseGlobale.employeeEnCours = null;
            Label_Accueil_NomUser = "Choisissez un employé";
            ColorUserConnect = Brushes.Linen;
        }
        #endregion
    }
    #region Class
    public class CategoryItem
    {
        public string ButtonUserContent { get; set; }

        public int ButtonUserTag { get; set; }

        public Brush ButtonUserBackground { get; set; }

    }
    #endregion

}
