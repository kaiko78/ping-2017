﻿using App_pressing_Loreau.Data.DAO;
using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Model.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using ZXing.Common;
using ZXing;
using ZXing.QrCode;

using BarcodeReader = ZXing.Presentation.BarcodeReader;
using BarcodeWriter = ZXing.Presentation.BarcodeWriter;
using BarcodeWriterGeometry = ZXing.Presentation.BarcodeWriterGeometry;
using App_pressing_Loreau.ViewModel;



namespace App_pressing_Loreau.Model
{
    class RecuPaiement
    {

        private static String printerName;
        //= "EPSON TM-U220 Receipt";

        //"EPSON TM-T20II Receipt5";
        public Commande commande { get; set; }
        public static String printName = "TM-T20";
      
        //public static String pattern_path = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.Length - 10) + "Resources\\PatternFile\\RecuPaiement";
        //public static String copy_path = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.Length - 10)+"Resources\\Temp\\RecuPaiement";
       

        //public static String pattern_path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName)+ "Resources\\PatternFile\\RecuPaiement";
        //public static String copy_path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName)+ "Resources\\Temp\\RecuPaiement";


        public static String pattern_path = "C:\\Resources\\PatternFile\\RecuPaiement";
        public static String copy_path = "C:\\Resources\\Temp\\RecuPaiement";

        //je crée une variable qui prendra le numéro de la commande pour transformer en qrcode
        public static string codeid="0000"; 
       // public static String pattern_path = "C:\\Users\\Maxime\\Documents\\PING LOREAU\\ping1-pressing-loreau-master\\App_pressing_Loreau\\Resources\\PatternFile\\RecuPaiement";
        //public static String copy_path = "C:\\Users\\Maxime\\Documents\\PING LOREAU\\ping1-pressing-loreau-master\\App_pressing_Loreau\\Resources\\Temp\\RecuPaiement";




        Font verdana10Font;
        StreamReader reader;

        public RecuPaiement(Commande commande)
        {
            this.commande = commande;
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                if (PrinterSettings.InstalledPrinters[i].Contains(printName))
                {
                    printerName = PrinterSettings.InstalledPrinters[i];
                }

            }

            if (printerName == null)
            {
                printerName = "";
                MessageBox.Show("Imprimante non trouvée");
            }
        }

        public void printRecu(Boolean testonce = true)
        {
            try
            {
                //creation du fichier temporaire dans resource/temp
                if (System.IO.File.Exists(copy_path + ".txt"))
                    System.IO.File.Delete(copy_path + ".txt");
                System.IO.File.Copy(pattern_path + ".txt", copy_path + ".txt");

                //calcul des totals
                float total_TTC = 0;
                float total_payee = 0;
                foreach (Article art in commande.listArticles)
                {
                    total_TTC = total_TTC + art.TTC;
                }
                foreach (Payement paie in commande.listPayements)
                {
                    total_payee = total_payee + paie.montant;
                }

                //Ajout du contenue du ticket
                File.AppendAllText(copy_path + ".txt", commande.client.nom + " " + commande.client.prenom + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);

                File.AppendAllText(copy_path + ".txt", DateTime.Now.ToString() + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "N° de commande : " + commande.id + Environment.NewLine);
                if (testonce == true)
                {
                    //je passe le int en string pour qu'il passe dans la génération du qrcode 
                    codeid = commande.id.ToString();
                    QrcodeBitmap = GenerateQR(200, 200, codeid);
                    //le tempoqr.bmp contient le qr code à afficher il faudra peut etre changer l'emplacement du bmp pour le metre dans ressource
                    //et le modifier aussi au niveau de l'impression voir bas de page: g.DrawImage(Image.FromFile("tempoqr.bmp"), new System.Drawing.Point(10, (int)yPos));
                    QrcodeBitmap.Save(copy_path + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                   
                }
                double tempsTraitement = 3;
                foreach (Article art in commande.listArticles)
                {
                    if (art.type.departement.id == 14)
                    {
                        tempsTraitement = 7;
                        break;
                    }
                }
                //On récupère les dates choisies, on les passe en string
                DateTime blanblan = Date_Pressing_BlanchisserieVM.ConfigClass.blanblan;
                String aaaaa = blanblan.ToString("dd/MM/yyyy");
                DateTime prepre = Date_Pressing_BlanchisserieVM.ConfigClass.prepre;
                String bbbbbbb = prepre.ToString("dd/MM/yyyy");
                String ccccccc = bbbbbbb;

                if (aaaaa == "01/01/0001" || bbbbbbb == "01/01/0001")
                {
                    File.AppendAllText(copy_path + ".txt", "Remise prévue le " + commande.date.AddDays(tempsTraitement).ToString("dd/MM/yyyy") + Environment.NewLine);
                }
                else
                {
                    foreach (Article art in commande.listArticles)
                    {
                        if (art.type.departement.id == 14)
                        {
                            ccccccc = aaaaa;
                            break;
                        }
                    }
                    File.AppendAllText(copy_path + ".txt", "Remise prévue le " + ccccccc + Environment.NewLine);

                }
                File.AppendAllText(copy_path + ".txt", "Réceptionniste : " + ClasseGlobale.employeeEnCours.nom + " " + ClasseGlobale.employeeEnCours.prenom + Environment.NewLine);
                if (commande.payee)
                    File.AppendAllText(copy_path + ".txt", "Commande réglée" + Environment.NewLine);
                else
                    File.AppendAllText(copy_path + ".txt", "NON REGLEE" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "_________________________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "Commande : " + commande.listArticles.Count + " article(s)" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);

                //ajout des articles
                float totalTVA = 0;
                float totalTTC = 0;
                float totalHT = 0;

                foreach (Article arti in commande.listArticles)
                {
                    //ajout du nom de l'article avec son nombre d'espaces
                    if (arti.ifRendu)
                        File.AppendAllText(copy_path + ".txt", "r ");
                    else
                        File.AppendAllText(copy_path + ".txt", "~ ");

                    //dans ou hors convoyeur
                    String conv = "";
                    if (arti.convoyeur != null && arti.convoyeur.emplacement != 0)
                        conv = "" + arti.convoyeur.emplacement;
                    else
                        conv = "HC";

                    //test la taille du nom de l'article
                    int nbespaces = 25 - arti.type.nom.Length;
                    if (arti.type.nom.Length < 18)
                    {

                        File.AppendAllText(copy_path + ".txt", String.Format("{0}{1}{2}", arti.type.nom.PadRight(nbespaces), String.Format("{0}€", " "+(decimal)arti.TTC).PadRight(6), String.Format("{0}", conv)) + Environment.NewLine);
                    }
                    else
                    {
                        File.AppendAllText(copy_path + ".txt", arti.type.nom.PadRight(20) + Environment.NewLine);
                        File.AppendAllText(copy_path + ".txt", "".PadRight(nbespaces) + (" "+ ((decimal)arti.TTC) + "€").PadRight(6) + "" + conv + Environment.NewLine);
                    }

                    //si commentaire
                    if (arti.commentaire != null)
                        if (!arti.commentaire.Equals(""))
                            File.AppendAllText(copy_path + ".txt", "    + " + arti.commentaire + Environment.NewLine);

                    totalTTC = totalTTC + arti.TTC;
                    totalHT = totalHT + (arti.TTC / (1 + arti.TVA / 100));
                }
                //ajout des totals
                totalTVA = totalTTC - totalHT;
                File.AppendAllText(copy_path + ".txt", "             ______________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "TTC                     " + (decimal)totalTTC + "€" + Environment.NewLine);
                if (commande.remise != 0)
                    File.AppendAllText(copy_path + ".txt", "Remise                 " + (decimal)commande.remise + "€" + Environment.NewLine);

                double TVATotale = Math.Round((totalTTC - commande.remise) - (totalTTC - commande.remise) / ((100 + (TypeArticleDAO.selectTypesById(2).TVA)) / 100), 2, MidpointRounding.AwayFromZero);
                File.AppendAllText(copy_path + ".txt", "Total TTC              " + ((decimal)totalTTC - (decimal)commande.remise) + "€" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "Dont TVA               " + (decimal)TVATotale + "€" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", " " + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "Solde compte               " + commande.client.solde + "€" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "_________________________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", " " + Environment.NewLine);

                //ajout des paiements
                foreach (Payement paie in commande.listPayements)
                {
                    int nbespace = 18;
                    File.AppendAllText(copy_path + ".txt", "    " + paie.typePaiement);
                    for (int i = 0; i < (nbespace - paie.typePaiement.Length); i++)
                        File.AppendAllText(copy_path + ".txt", "-");
                    File.AppendAllText(copy_path + ".txt", (decimal)paie.montant + "€" + Environment.NewLine);
                }
                File.AppendAllText(copy_path + ".txt", "_________________________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);

                //code pour générer un qrcode 
                
              
                //string path = "tempoqr.bmp";
                //fin génération bmp qrcode
                PrintOff();
                //System.IO.File.Delete(copy_path + ".txt");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Impr : " + e);
            }

        }


        public void printRecuSolde(float soldestotal)
        {
            try
            {
                //creation du fichier temporaire dans resource/temp
                if (System.IO.File.Exists(copy_path + ".txt"))
                    System.IO.File.Delete(copy_path + ".txt");
                System.IO.File.Copy(pattern_path + ".txt", copy_path + ".txt");

                //calcul des totals
                float total_TTC = 0;
                float total_payee = 0;
                foreach (Article art in commande.listArticles)
                {
                    total_TTC = total_TTC + art.TTC;
                }
                foreach (Payement paie in commande.listPayements)
                {
                    total_payee = total_payee + paie.montant;
                }

                //Ajout du contenue du ticket
                File.AppendAllText(copy_path + ".txt", commande.client.nom + " " + commande.client.prenom + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);

                File.AppendAllText(copy_path + ".txt", DateTime.Now.ToString() + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "N° de commande : " + commande.id + Environment.NewLine);
                
                double tempsTraitement = 3;
                foreach (Article art in commande.listArticles)
                {
                    if (art.type.departement.id == 14)
                    {
                        tempsTraitement = 7;
                        break;
                    }
                }
                DateTime blanblan = Date_Pressing_BlanchisserieVM.ConfigClass.blanblan;
                String aaaaa = blanblan.ToString("dd/MM/yyyy");
                DateTime prepre = Date_Pressing_BlanchisserieVM.ConfigClass.prepre;
                String bbbbbbb = prepre.ToString("dd/MM/yyyy");
                String ccccccc = bbbbbbb;

                if (aaaaa == "01/01/0001" || bbbbbbb == "01/01/0001")
                {
                    //File.AppendAllText(copy_path + ".txt", "Remise prévue le " + commande.date.AddDays(tempsTraitement).ToString("dd/MM/yyyy") + Environment.NewLine);
                }
                else
                {
                    foreach (Article art in commande.listArticles)
                    {
                        if (art.type.departement.id == 14)
                        {
                            ccccccc = aaaaa;
                            break;
                        }
                    }
                    //File.AppendAllText(copy_path + ".txt", "Remise prévue le " + ccccccc + Environment.NewLine);

                }
                File.AppendAllText(copy_path + ".txt", "Réceptionniste : " + ClasseGlobale.employeeEnCours.nom + " " + ClasseGlobale.employeeEnCours.prenom + Environment.NewLine);
                if (commande.payee)
                    File.AppendAllText(copy_path + ".txt", "Commande réglée" + Environment.NewLine);
                else
                    File.AppendAllText(copy_path + ".txt", "NON REGLEE" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "_________________________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "Commande : Achat de solde" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);

                //ajout des articles
                float totalTVA = 0;
                float totalTTC = 0;
                float totalHT = 0;

                foreach (Article arti in commande.listArticles)
                {
                    //ajout du nom de l'article avec son nombre d'espaces
                    if (arti.ifRendu)
                        File.AppendAllText(copy_path + ".txt", "r ");
                    else
                        File.AppendAllText(copy_path + ".txt", "~ ");

                    //dans ou hors convoyeur
                    String conv = "";
                    if (arti.convoyeur != null && arti.convoyeur.emplacement != 0)
                        conv = "" + arti.convoyeur.emplacement;
                    else
                        conv = "HC";

                    //test la taille du nom de l'article
                    int nbespaces = 25 - arti.type.nom.Length;
                    if (arti.type.nom.Length < 18)
                    {

                        File.AppendAllText(copy_path + ".txt", String.Format("{0}{1}{2}", arti.type.nom.PadRight(nbespaces), String.Format("{0}€", " " + (decimal)arti.TTC).PadRight(6), String.Format("{0}", conv)) + Environment.NewLine);
                    }
                    else
                    {
                        File.AppendAllText(copy_path + ".txt", arti.type.nom.PadRight(20) + Environment.NewLine);
                        File.AppendAllText(copy_path + ".txt", "".PadRight(nbespaces) + (" " + ((decimal)arti.TTC) + "€").PadRight(6) + "" + conv + Environment.NewLine);
                    }

                    //si commentaire
                    if (arti.commentaire != null)
                        if (!arti.commentaire.Equals(""))
                            File.AppendAllText(copy_path + ".txt", "    + " + arti.commentaire + Environment.NewLine);

                    totalTTC = totalTTC + arti.TTC;
                    totalHT = totalHT + (arti.TTC / (1 + arti.TVA / 100));
                }
                //ajout des totals
                totalTVA = totalTTC - totalHT;
                File.AppendAllText(copy_path + ".txt", "             ______________" + Environment.NewLine);
                //File.AppendAllText(copy_path + ".txt", "TTC                     " + (decimal)totalTTC + "€" + Environment.NewLine);
                if (commande.remise != 0)
                    File.AppendAllText(copy_path + ".txt", "Remise                 " + (decimal)commande.remise + "€" + Environment.NewLine);

                double TVATotale = Math.Round((soldestotal - commande.remise) - (soldestotal - commande.remise) / ((100 + (TypeArticleDAO.selectTypesById(2).TVA)) / 100), 2, MidpointRounding.AwayFromZero);
                File.AppendAllText(copy_path + ".txt", "Total TTC              " + (decimal)soldestotal + "€" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "Dont TVA               " + (decimal)TVATotale + "€" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", " " + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "Solde compte               " + commande.client.solde + "€" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", "_________________________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", " " + Environment.NewLine);

                //ajout des paiements
                foreach (Payement paie in commande.listPayements)
                {
                    int nbespace = 18;
                    File.AppendAllText(copy_path + ".txt", "    " + paie.typePaiement);
                    for (int i = 0; i < (nbespace - paie.typePaiement.Length); i++)
                        File.AppendAllText(copy_path + ".txt", "-");
                    File.AppendAllText(copy_path + ".txt", (decimal)paie.montant + "€" + Environment.NewLine);
                }
                File.AppendAllText(copy_path + ".txt", "_________________________" + Environment.NewLine);
                File.AppendAllText(copy_path + ".txt", Environment.NewLine);

                //code pour générer un qrcode 


                //string path = "tempoqr.bmp";
                //fin génération bmp qrcode
                PrintOffSolde();
                //System.IO.File.Delete(copy_path + ".txt");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Impr : " + e);
            }

        }
        //ca devrait générer un bmp avec un qrcode dedans 
        public Bitmap QrcodeBitmap { get; private set; }

        public Bitmap GenerateQR(int width, int height, string text)
        {
            var bw = new ZXing.BarcodeWriter();
            var encOptions = new ZXing.Common.EncodingOptions() { Width = width, Height = height, Margin = 0 };
            bw.Options = encOptions;
            bw.Format = ZXing.BarcodeFormat.QR_CODE;
            var result = new Bitmap(bw.Write(text));

            return result;
        }
      
        //Print the document
        //source : http://www.c-sharpcorner.com/UploadFile/mahesh/printfile06062007133250PM/printfile.aspx
        public void PrintOff()
        {
            string filename = copy_path + ".txt";
            //Create a StreamReader object
            reader = new StreamReader(filename);
            //Create a Verdana font with size 10
            verdana10Font = new Font("Verdana", 10);
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //gestion des marges
            pd.OriginAtMargins = true;
            pd.DefaultPageSettings.Margins.Top = 0;
            pd.DefaultPageSettings.Margins.Left = 21;
            pd.DefaultPageSettings.Margins.Right = 0;
            pd.DefaultPageSettings.Margins.Bottom = 0;
            //Add PrintPage event handler
            pd.PrintPage += new PrintPageEventHandler(this.PrintTextFileHandler);
            //Call Print Method
            pd.PrinterSettings.PrinterName = printerName;
            pd.Print();
            //Close the reader
            if (reader != null)
                reader.Close();
        }

        public void PrintOffSolde()
        {
            string filename = copy_path + ".txt";
            //Create a StreamReader object
            reader = new StreamReader(filename);
            //Create a Verdana font with size 10
            verdana10Font = new Font("Verdana", 10);
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //gestion des marges
            pd.OriginAtMargins = true;
            pd.DefaultPageSettings.Margins.Top = 0;
            pd.DefaultPageSettings.Margins.Left = 21;
            pd.DefaultPageSettings.Margins.Right = 0;
            pd.DefaultPageSettings.Margins.Bottom = 0;
            //Add PrintPage event handler
            pd.PrintPage += new PrintPageEventHandler(this.PrintTextFileHandlerSolde);
            //Call Print Method
            pd.PrinterSettings.PrinterName = printerName;
            pd.Print();
            //Close the reader
            if (reader != null)
                reader.Close();
        }

        private void PrintTextFileHandler(object sender, PrintPageEventArgs ppeArgs)
        {

            //Get the Graphics object
            Graphics g = ppeArgs.Graphics;
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            //Read margins from PrintPageEventArgs
            float leftMargin = ppeArgs.MarginBounds.Left;
            float topMargin = ppeArgs.MarginBounds.Top;
            string line = null;
            //Calculate the lines per page on the basis of the height of the page and the height of the font
            linesPerPage = ppeArgs.MarginBounds.Height /
            verdana10Font.GetHeight(g);
            //Now read lines one by one, using StreamReader
            while (count < linesPerPage &&
            ((line = reader.ReadLine()) != null))
            {
                //Calculate the starting position
                yPos = topMargin + (count *
                verdana10Font.GetHeight(g));
                //Draw text
                g.DrawString(line, verdana10Font, Brushes.Black,
                leftMargin, yPos, new StringFormat());
                //Move to next line
                count++;
            }
            //ajout du qrcode à la fin pour ca il faut jouer sur le Point(10,(int)yPos) 
            //et peut etre metre le chemin complet du bmp

            g.DrawImage(Image.FromFile(copy_path+".bmp"), new System.Drawing.Point(31, (int)yPos));
           
            //If PrintPageEventArgs has more pages to print
            if (line != null)
            {
                ppeArgs.HasMorePages = true;
            }
            else
            {
                ppeArgs.HasMorePages = false;
            }
        }

        private void PrintTextFileHandlerSolde(object sender, PrintPageEventArgs ppeArgs)
        {

            //Get the Graphics object
            Graphics g = ppeArgs.Graphics;
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            //Read margins from PrintPageEventArgs
            float leftMargin = ppeArgs.MarginBounds.Left;
            float topMargin = ppeArgs.MarginBounds.Top;
            string line = null;
            //Calculate the lines per page on the basis of the height of the page and the height of the font
            linesPerPage = ppeArgs.MarginBounds.Height /
            verdana10Font.GetHeight(g);
            //Now read lines one by one, using StreamReader
            while (count < linesPerPage &&
            ((line = reader.ReadLine()) != null))
            {
                //Calculate the starting position
                yPos = topMargin + (count *
                verdana10Font.GetHeight(g));
                //Draw text
                g.DrawString(line, verdana10Font, Brushes.Black,
                leftMargin, yPos, new StringFormat());
                //Move to next line
                count++;
            }
            //ajout du qrcode à la fin pour ca il faut jouer sur le Point(10,(int)yPos) 
            //et peut etre metre le chemin complet du bmp

            //g.DrawImage(Image.FromFile(copy_path + ".bmp"), new System.Drawing.Point(10, (int)yPos));

            //If PrintPageEventArgs has more pages to print
            if (line != null)
            {
                ppeArgs.HasMorePages = true;
            }
            else
            {
                ppeArgs.HasMorePages = false;
            }
        }
    }
}
