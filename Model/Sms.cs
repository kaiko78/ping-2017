﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace App_pressing_Loreau.Model
{


    public class Sms 
    {
        private string _string1;
        private string _string2;
        private int _voteValue;
        //calcul du SHA1
        
        
        public string String1
        {
            get { return _string1; }
            set
            {
                _string1 = value;
              
            }
        }

        public string String2
        {
            get { return _string2; }
            set
            {
                _string2 = value;
               }
        }
        public static string HashSHA1(string sInputString)
        {

            var sha = SHA1.Create();
            byte[] bHash = sha.ComputeHash(Encoding.UTF8.GetBytes(sInputString));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < bHash.Length; i++)
            {
                sBuilder.Append(bHash[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
        public static void sendSms(string num_tel, string message)
        {
            Hashtable section = (Hashtable)ConfigurationManager.GetSection("userSettings/SMS");
            Console.WriteLine(section["AK"]);
            String AK = section["AK"].ToString();
            String AS = section["AS"].ToString();
            String CK = section["CK"].ToString();
            Console.WriteLine("ak " + AK);
            //Paramètres de la méthode appellée
            String ServiceName = "sms-ld166878-1";
            String METHOD = "POST";
            String QUERY = "https://eu.api.ovh.com/1.0/sms/" + ServiceName + "/jobs/";
            // If num_tel.StartsWith("06") Or num_tel.StartsWith("07") Or num_tel.StartsWith("+336") Or num_tel.StartsWith("+337") Then
            const string quote = "\"";
            String BODY = "{ " + quote + "charset" + quote + ": " + quote + "UTF-8" + quote + ", " + quote + "receivers" + quote + ": [ " + quote + num_tel + quote + " ], " + quote + "message" + quote + ": " + quote + message + quote + ", " + quote + "priority" + quote + ": " + quote + "high" + quote + ",  " + quote + "senderForResponse" + quote + ": true}";
            //String BODY2 = @"{ ""charset"": ""UTF-8"", ""receivers"": [ ""+33671481102"" ], ""message"": ""Test SMS OVH"", ""priority"": ""high"",  ""senderForResponse"": true}";

            //Console.WriteLine("BODY "+BODY);
            //Console.WriteLine("BODY2 " + BODY);
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            String TSTAMP = (unixTimestamp).ToString();


            String signature = "$1$" + HashSHA1(AS + "+" + CK + "+" + METHOD + "+" + QUERY + "+" + BODY + "+" + TSTAMP);
            Console.WriteLine(signature);

            //Creation de la requete
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(QUERY);
            req.Method = METHOD;
            req.ContentType = "application/json";
            req.Headers.Add("X-Ovh-Application:" + AK);
            req.Headers.Add("X-Ovh-Consumer:" + CK);
            req.Headers.Add("X-Ovh-Signature:" + signature);
            req.Headers.Add("X-Ovh-Timestamp:" + TSTAMP);

            //Ecriture des paramètres BODY
            using (System.IO.Stream s = req.GetRequestStream())
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                    sw.Write(BODY);
            }

            try
            {
                //Récupération du résultat de l'appel
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)req.GetResponse();
                String[] l = null;
                using (var respStream = myHttpWebResponse.GetResponseStream())
                {
                    var reader = new StreamReader(respStream);
                    String result = reader.ReadToEnd().Trim();
                    Console.WriteLine(result);

                }
                myHttpWebResponse.Close();

            }
            catch (WebException e)
            {
                Console.WriteLine("Error : ");
                Console.WriteLine("Error : ");
                using (WebResponse response = e.Response)
                using (Stream data = ((HttpWebResponse)response).GetResponseStream())
                using (var reader = new StreamReader(data))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }
        public int VoteValue
        {
            get { return _voteValue; }
            set { _voteValue = value;  }
        }
        
    }
}
      
