

ALTER TABLE `client` ADD `clt_solde` float DEFAULT '0' ;

ALTER TABLE `commande` ADD `cmd_emp_id` int(11) NOT NULL DEFAULT '13' ;
ALTER TABLE `commande` ADD `cmd_date_sms` timestamp NULL DEFAULT NULL ;
ALTER TABLE `commande` ADD `cmd_solde` int(11) NOT NULL DEFAULT '0' ;

UPDATE commande SET cmd_solde=1;

UPDATE departement SET dep_nom="Carte prepayee" WHERE dep_id=16;

INSERT INTO `departement` (`dep_id`, `dep_nom`) VALUES
(19, 'Retouche'),
(20, 'Divers');

UPDATE type SET typ_nom='Fidelite 50 euros' WHERE typ_id=52;
UPDATE type SET typ_nom='Fidelité 80 euros' WHERE typ_id=53;
UPDATE type SET typ_nom='Fidélité 100 euros' WHERE typ_id=54;

UPDATE typepaiement SET tpp_nom="solde" WHERE tpp_id=1;

CREATE TABLE IF NOT EXISTS `popup` (
  `id` int(11) NOT NULL,
  `today` timestamp NULL DEFAULT NULL,
  `pressing` timestamp NULL DEFAULT NULL,
  `blanchisserie` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `popup`
--

INSERT INTO `popup` (`id`, `today`, `pressing`, `blanchisserie`) VALUES
(0, '2017-01-16 15:49:54', '2017-01-27 23:00:00', '2017-01-19 23:00:00');

ALTER TABLE `commande`
  ADD CONSTRAINT `fk_commande_employe1` FOREIGN KEY (`cmd_emp_id`) REFERENCES `employe` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;