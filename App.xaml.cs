﻿using App_pressing_Loreau.View;
using App_pressing_Loreau.ViewModel;
using System.Windows;
using System.Windows.Controls;
using App_pressing_Loreau.Data.DAO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;

using App_pressing_Loreau.Helper;
using App_pressing_Loreau.Model.DTO;
using Microsoft.Practices.Prism.Commands;

namespace App_pressing_Loreau
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// Lancement de l'application avec le chargement du VM
    /// </summary>
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

        }
        
        protected override void OnStartup(StartupEventArgs e)
        {
           
            base.OnStartup(e);
           
        // chargement de la page accueil

        Accueil acc = new Accueil();
            // liaison avec la page AccueilVM
            AccueilVM context = new AccueilVM();


            var ajd = DateTime.Now;
            var dateajd = ajd.Date;
            DateTime popup = CommandeDAO.selectToday();
            var datepopup = popup.Date;
            int today = 3;

            if(dateajd == datepopup)
            { today = 1; }
            else
            {
                today = 0;
            }
           

            
          //  Dates_Pressing_blanchisserieVM datVM = new Dates_Pressing_blanchisserieVM();
            acc.DataContext = context;
            Dates_Pressing_blanchisserie dat = new Dates_Pressing_blanchisserie();

           
            acc.Show();
            //affiche la fenetre de choix de date blanchisserie et pressing

            if(today==1)
            {
				dat.Close();
            }
            else if(today==0)
            {

                dat.ShowDialog();
            }
            

           // DatePicker blanchisserie = datVM.Blanchisserie;
   //         DatePicker pressing = datVM.Pressing;

            //PageDemarrage acc = new PageDemarrage();

            //PageDemarrageVM pVM = new PageDemarrageVM();
            //acc.DataContext = pVM;
            //acc.Show();
        }
        
    }
}
