-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 12 Janvier 2017 à 10:44
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bddping1`
--
CREATE DATABASE IF NOT EXISTS `bddping1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bddping1`;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_photo` varchar(150) DEFAULT NULL,
  `art_commentaire` text,
  `art_rendu` tinyint(1) NOT NULL,
  `art_date_rendu` timestamp NULL DEFAULT NULL,
  `art_TVA` float NOT NULL,
  `art_TTC` float NOT NULL,
  `art_conv_id` int(11) DEFAULT NULL,
  `art_cmd_id` int(11) NOT NULL,
  `art_typ_id` int(11) NOT NULL,
  `art_date_payee` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`art_id`),
  KEY `fk_article_convoyeur1_idx` (`art_conv_id`),
  KEY `fk_article_commande1_idx` (`art_cmd_id`),
  KEY `fk_article_type1_idx` (`art_typ_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `clt_id` int(11) NOT NULL AUTO_INCREMENT,
  `clt_type` tinyint(1) NOT NULL,
  `clt_nom` varchar(45) NOT NULL,
  `clt_prenom` varchar(45) NOT NULL,
  `clt_dateInscription` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clt_contactmail` tinyint(1) NOT NULL,
  `clt_contactsms` tinyint(1) NOT NULL,
  `clt_fix` varchar(45) DEFAULT NULL,
  `clt_mob` varchar(45) DEFAULT NULL,
  `clt_adresse` text,
  `clt_dateNaissance` date DEFAULT NULL,
  `clt_email` varchar(50) DEFAULT NULL,
  `clt_idCleanway` int(11) DEFAULT NULL,
  `clt_solde` float DEFAULT NULL,
  PRIMARY KEY (`clt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

INSERT INTO `client` (`clt_id`, `clt_type`, `clt_nom`, `clt_prenom`, `clt_dateInscription`, `clt_contactmail`, `clt_contactsms`, `clt_fix`, `clt_mob`, `clt_adresse`, `clt_dateNaissance`, `clt_email`, `clt_idCleanway`, `clt_solde`) VALUES
(2, 0, 'LEFEVRE', 'David', '2015-03-02 19:02:56', 0, 0, '', '0684932718', '\\\\\\\\', '1982-10-21', '', 0, 0);


-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `cmd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cmd_date_rendu` timestamp NULL DEFAULT NULL,
  `cmd_payee` tinyint(1) NOT NULL,
  `cmd_clt_id` int(11) NOT NULL,
  `cmd_remise` float DEFAULT NULL,
  `cmd_emp_id` int(11) NOT NULL DEFAULT '13',
  `cmd_date_sms` timestamp NULL DEFAULT NULL,
  `cmd_solde` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cmd_id`),
  KEY `fk_commande_client1_idx` (`cmd_clt_id`),
  KEY `fk_commande_employe1` (`cmd_emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `commande` (`cmd_id`, `cmd_date`, `cmd_date_rendu`, `cmd_payee`, `cmd_clt_id`, `cmd_remise`, `cmd_emp_id`, `cmd_date_sms`, `cmd_solde`) VALUES
(1, '2015-03-02 18:03:04', '2015-03-02 18:42:01', 1, 2, 0, 13, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `com_com` varchar(45) NOT NULL,
  PRIMARY KEY (`com_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

INSERT INTO `commentaire` (`com_id`, `com_com`) VALUES
(1, 'Sang'),
(2, 'Chocolat'),
(3, 'Urine'),
(4, 'Décolaration'),
(5, 'Gras'),
(6, 'Troué'),
(7, 'Bouton manquant'),
(8, 'Ceinture'),
(9, 'Boucle dans poche'),
(10, 'Fruit'),
(11, 'Vin'),
(12, 'Herbe'),
(13, 'transpiration'),
(14, 'Lait'),
(15, 'Stylo'),
(16, 'Autres'),
(17, 'SANS GARANTIE');

-- --------------------------------------------------------

--
-- Structure de la table `convoyeur`
--

CREATE TABLE IF NOT EXISTS `convoyeur` (
  `conv_id` int(11) NOT NULL AUTO_INCREMENT,
  `conv_emplacement` int(11) NOT NULL,
  `conv_encombrement` float NOT NULL,
  PRIMARY KEY (`conv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE IF NOT EXISTS `departement` (
  `dep_id` int(11) NOT NULL AUTO_INCREMENT,
  `dep_nom` varchar(45) NOT NULL,
  PRIMARY KEY (`dep_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

INSERT INTO `departement` (`dep_id`, `dep_nom`) VALUES
(10, 'Classique'),
(11, 'Manteaux'),
(12, 'Literie'),
(13, 'Ameublement'),
(14, 'Blanchisserie'),
(15, 'Reppassage'),
(16, 'Carte prépayée'),
(17, 'sous traitance'),
(18, 'Vente additionnelle');

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE IF NOT EXISTS `employe` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_nom` varchar(45) NOT NULL,
  `emp_prenom` varchar(45) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

INSERT INTO `employe` (`emp_id`, `emp_nom`, `emp_prenom`) VALUES
(13, 'L', 'David'),
(15, 'B', 'Hajar'),
(17, 'B', 'Michelle'),
(19, 'B', 'Marion'),
(20, 'DR', 'Sarah'),
(21, 'M', 'Laurence');

-- --------------------------------------------------------

--
-- Structure de la table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_message` varchar(45) NOT NULL,
  `log_emp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `fk_log_employe1_idx` (`log_emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `paiement`
--

CREATE TABLE IF NOT EXISTS `paiement` (
  `pai_id` int(11) NOT NULL AUTO_INCREMENT,
  `pai_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pai_montant` float NOT NULL,
  `pai_cmd_id` int(11) NOT NULL,
  `pai_type` varchar(45) NOT NULL,
  PRIMARY KEY (`pai_id`),
  KEY `fk_paiement_commande1_idx` (`pai_cmd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `typ_id` int(11) NOT NULL AUTO_INCREMENT,
  `typ_nom` varchar(45) CHARACTER SET utf8 NOT NULL,
  `typ_encombrement` float NOT NULL,
  `typ_TVA` float NOT NULL,
  `typ_TTC` float NOT NULL,
  `typ_dep_id` int(11) NOT NULL,
  PRIMARY KEY (`typ_id`),
  KEY `fk_typ_departement_idx` (`typ_dep_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

INSERT INTO `type` (`typ_id`, `typ_nom`, `typ_encombrement`, `typ_TVA`, `typ_TTC`, `typ_dep_id`) VALUES
(1, 'Pantalon', 1, 20, 5.6, 10),
(2, 'Pantalon Clair', 1, 20, 6.5, 10),
(3, 'Tee Shirt', 1, 20, 4.6, 10),
(4, 'Veste', 1.5, 20, 5.6, 10),
(5, 'Veste Claire', 1.5, 20, 6.5, 10),
(6, 'Polo', 1, 20, 4.6, 10),
(7, 'Costume 2 pièces', 1.5, 20, 11.2, 10),
(8, 'Costume Clair', 1.5, 20, 13, 10),
(9, 'Gilet Costume', 1, 20, 4, 10),
(13, 'Chemise', 1, 20, 3.7, 10),
(14, 'Chemise pliée', 0, 20, 4.2, 10),
(15, 'Chemisier', 1, 20, 5.2, 10),
(16, 'Robe', 3, 20, 8.7, 10),
(17, 'Robe de soirée', 3, 20, 12, 10),
(18, 'Blouson', 2, 20, 8.7, 11),
(19, 'Manteau', 3, 20, 11, 11),
(20, 'Imperméable', 3, 20, 16, 11),
(21, 'Doudoune ', 3, 20, 16, 11),
(22, 'Supp imperméabilisation', 0, 20, 4, 11),
(24, 'Couette Blanche', 0, 20, 17.5, 12),
(25, 'Couette plume', 0, 20, 24, 12),
(26, 'Couette Pure Laine', 0, 20, 25, 12),
(28, 'Couverture', 0, 20, 15, 12),
(30, 'Traversin', 0, 20, 12, 12),
(31, 'Traversin plume', 0, 20, 19, 12),
(32, 'Oreiller', 0, 20, 8, 12),
(33, 'Oreiller plume ', 0, 20, 19, 12),
(35, 'Rideau simple', 0, 20, 6, 13),
(36, 'Double rideau', 0, 20, 8, 13),
(37, 'Drap plat', 10, 20, 3, 14),
(38, 'Drap Housse', 10, 20, 3.7, 14),
(39, 'Housse de Couette', 10, 20, 6.4, 14),
(40, 'Taie', 10, 20, 2, 14),
(41, 'Finition main', 10, 20, 2.5, 14),
(42, 'Supp amidon', 10, 20, 2.5, 14),
(43, 'Torchon', 10, 20, 1.2, 14),
(44, 'Serviette de table ', 10, 20, 1.2, 14),
(45, 'Serviette Brodée', 10, 20, 1.5, 14),
(46, 'Nappe', 10, 20, 8, 14),
(47, 'Nappe longue', 10, 20, 12, 14),
(48, 'Chemises', 1, 20, 3.7, 15),
(49, 'Repassage Pantalon', 1, 20, 4.2, 15),
(50, 'Repassage kilo', 0, 20, 7, 15),
(52, 'Fidelite 50 euros', 0, 20, 50, 16),
(53, 'Fidelité 80 euros', 0, 20, 80, 16),
(54, 'Fidélité 100 euros', 0, 20, 100, 16),
(55, 'Carte chemise', 0, 20, 25, 16),
(56, 'Tapis', 0, 20, 16, 17),
(57, 'Manteau peau', 0, 20, 65, 17),
(58, 'Pantalon', 0, 20, 55, 17),
(59, 'Jupe', 0, 20, 45, 17),
(60, 'Gilet de Chasse', 0, 20, 50, 17),
(61, 'Gants', 0, 20, 40, 17),
(62, 'Anti Mite', 0, 20, 3, 18),
(63, 'Anti mite x2', 0, 20, 5, 18),
(64, 'Article 9.90€', 0, 20, 9.9, 18),
(65, 'Rouleau avec manche', 0, 20, 4.9, 18),
(66, 'Petite Recharge', 0, 20, 2.9, 18),
(68, 'Housse de Canapé', 0, 20, 40, 13),
(69, 'Housse de Fauteuil', 0, 20, 30, 13),
(70, 'Assise / Dossier', 0, 20, 7, 13),
(71, 'Clic Clac', 0, 20, 30, 13),
(72, 'Nappe Brodée', 10, 20, 16, 14),
(73, 'Peignoir', 0, 20, 7.5, 14),
(74, 'Serviette éponge', 10, 20, 3, 14),
(75, 'Robe de mariée', 0, 20, 65, 10),
(76, 'Pull', 3, 20, 5.5, 10),
(77, 'Gilet', 3, 20, 5.5, 10),
(78, 'Cravate', 1, 20, 6.2, 10),
(79, 'Jupe', 1.5, 20, 6, 10),
(80, 'Voilage', 0, 20, 5, 13),
(81, 'Dessus de Lit', 0, 20, 17.5, 12),
(82, 'Plaid', 0, 20, 8, 12),
(83, 'Sac de Couchage', 0, 20, 12, 12),
(84, 'Sac Plume', 0, 20, 20, 12),
(85, 'Nappe Ronde', 0, 20, 11, 14),
(86, 'Lavage / Repassage kilo', 0, 20, 8.9, 15),
(87, 'Séchage / Pliage kilo', 0, 20, 5, 15),
(88, 'Lavage / Pliage Kilo', 0, 20, 6.9, 15),
(89, 'Veste Huilée', 0, 20, 65, 17),
(90, 'Manteau Huilé', 0, 20, 95, 17),
(91, 'Peau de Mouton', 0, 20, 35, 17),
(92, 'Peau de Vache m²', 0, 20, 30, 17),
(93, 'Autres sur devis', 0, 20, 100, 17),
(94, 'Ourlet simple', 0, 20, 11, 17),
(95, 'Ourlet revers', 0, 20, 14, 17),
(96, 'ourlet Manteau/veste', 0, 20, 30, 17),
(97, 'Ourlet Rideau simple', 0, 20, 15, 17),
(98, 'Ourlet Rideau Doublé', 0, 20, 25, 17),
(99, 'Ourlet Robe/jupe', 0, 20, 13, 17),
(100, 'Ourlet Robe/jupe doublée', 0, 20, 18, 17),
(101, 'Bas de Manche', 0, 20, 28, 17),
(102, 'Fermeture pantalon', 0, 20, 19, 17),
(103, 'Reprise taille jean', 0, 20, 17, 17),
(104, 'Grande Recharge', 0, 20, 8.9, 18),
(105, 'Pierre Carbonne', 0, 20, 4.9, 18),
(106, 'Gants', 0, 20, 7, 10),
(107, 'Casquette', 0, 20, 7, 10),
(108, 'Echarpe', 1, 20, 7, 10),
(109, 'Foulard', 1, 20, 7, 10),
(110, 'Chapeau', 0, 20, 7, 10),
(111, 'Couette Couleur', 0, 20, 17.5, 12),
(112, 'Couette motif', 0, 20, 17.5, 12),
(114, 'Divers HC', 0, 20, 5, 10),
(115, 'Petite réparation', 0, 20, 7, 17),
(116, 'Location machine moquette', 0, 20, 40, 18),
(117, 'Boite ', 0, 20, 5.9, 18),
(118, 'Soupline', 0, 20, 6.9, 18),
(119, 'Produit Moquette', 0, 20, 19, 18),
(120, 'Rode de Chambre', 3, 20, 7.5, 10),
(121, 'URGENT', 0, 20, 1, 10),
(122, 'Exo Tache', 0, 20, 7.9, 18),
(123, 'Teinture', 0, 20, 11.9, 18),
(124, 'Vetement de travail', 0, 20, 5, 10),
(125, 'Côte de travail', 0, 20, 8, 10),
(126, 'Ceinture', 0, 20, 0, 11),
(127, 'Entre Jambe', 0, 20, 7, 17),
(128, 'Petite reparation doublée', 0, 20, 15, 17),
(129, 'SKI', 0, 20, 10, 11);

-- --------------------------------------------------------

--
-- Structure de la table `typepaiement`
--

CREATE TABLE IF NOT EXISTS `typepaiement` (
  `tpp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpp_nom` varchar(45) NOT NULL,
  PRIMARY KEY (`tpp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Structure de la table `popup`
--

CREATE TABLE IF NOT EXISTS `popup` (
  `id` int(11) NOT NULL,
  `today` timestamp NULL DEFAULT NULL,
  `pressing` timestamp NULL DEFAULT NULL,
  `blanchisserie` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `popup`
--

INSERT INTO `popup` (`id`, `today`, `pressing`, `blanchisserie`) VALUES
(0, '2017-01-16 15:49:54', '2017-01-27 23:00:00', '2017-01-19 23:00:00');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_commande1` FOREIGN KEY (`art_cmd_id`) REFERENCES `commande` (`cmd_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_article_convoyeur1` FOREIGN KEY (`art_conv_id`) REFERENCES `convoyeur` (`conv_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_article_type1` FOREIGN KEY (`art_typ_id`) REFERENCES `type` (`typ_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_commande_client1` FOREIGN KEY (`cmd_clt_id`) REFERENCES `client` (`clt_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_commande_employe1` FOREIGN KEY (`cmd_emp_id`) REFERENCES `employe` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `fk_log_employe1` FOREIGN KEY (`log_emp_id`) REFERENCES `employe` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD CONSTRAINT `fk_paiement_commande1` FOREIGN KEY (`pai_cmd_id`) REFERENCES `commande` (`cmd_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `type`
--
ALTER TABLE `type`
  ADD CONSTRAINT `fk_typ_departement_idx` FOREIGN KEY (`typ_dep_id`) REFERENCES `departement` (`dep_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
